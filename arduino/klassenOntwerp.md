Ontwerpen van klassen voor de arduino
======================================
Eerst wordt een korte inleiding gegeven over de soorten arduinos en hun 
eigenschappen in het kort. Daarna is er een motivatie voor de keuze van het
objectgericht programmeren van de arduinos. Vervolgens bespreken we de 
vereisten van de arduinos zodat we daaruit de klassen kunnen ontwerpen in een 
volgende sectie. Ten slotte een besluit om alles samen te vatten.

Inleiding
----------
Er zijn twee type arduinos

+ arduino-huisje
+ arduino-CCU

Hoewel er overlap van functionaliteit is, zal een verschillend programma draaien 
op elke arduino waarbij de individuele eisen voor elke arduino in rekening wordt
gebracht.

Motivatie
----------
Ons doel is nu om beide type arduinos op een efficiënte manier te programmeren.
Om dit te bereiken zullen we objectgericht programmeren. Zo kunnen we ons 
focussen op de verschillende onderdelen van het systeem en vermijden we 
repititieve code. En omdat er overlap van functionaliteit is zullen sommige 
klassen op beide arduinos kunnen worden gebruikt. Een bijkomend voordeel is de 
mogelijkheid tot abstractie. Dit vergemakkelijkt het ontwerpproces van de code
door in eerste instantie na te denken over het probleem in termen van het 
probleem zelf. Dit in tegenstelling met proceduraal programmeren waarbij je
gedwongen bent om in termen van de computer te denken (if/while statements, 
loops, lijsten, ...). Bij objectgericht programmeren zijn de bouwstenen waaruit
een programma is opgebouwd klassen en objecten die reële objecten voorstellen.
Bij proceduraal programmeren zijn deze bouwstenen procedures die uit een serie
statements bestaan. Deze procedures zijn functies die argumenten afbeelden op
return waarden en staan niet direct in relatie met de reële objecten.

Eisen voor de arduinos
-----------------------
### Wat moet de arduino van een huisje kunnen doen? ###
+ De arduino moet data kunnen inlezen van de sensors. Deze moet de arduino 
  kunnen doorsturen. De doogestuurde informatie bestaat uit de sensorwaarde,
  de id van het object waar de sensor toe behoort, een timestamp, ...
+ De arduino moet toestellen kunnen in- en uitschakelen. Hierbij moet het 
  mogelijk zijn om, indien van toepassing, de verschillende standen van een 
  toestel in te stellen.
+ ~~Op elk ogenblik moet de arduino de toestand van een toestel kunnen 
  opvragen.~~ --> niet nodig! Toestand van een toestel wordt vastgehouden in het
  programma van de CCU. De voorstelling van die toestellen in het huisje zijn 
  geen exacte representatie van de toestand van een object maar slecht een 
  voorstelling die enkele aspecten bij benadering visualiseert.
  **Belangrijk!!** --> klopt dit? (tomg)
+ De arduino moet instructies  kunnen ontvangen van de CCU arduino. Deze 
  instructies bestaan uit het instellen van de toestand van toestellen en het
  opvragen van sensorwaarden.
+ **Opmerking:** De arduino controlleert enkel de actuatoren en sensoren van de 
  toestellen. Dit is slechts een voorstelling van de eigenlijke toestand van een 
  toestel dat compleet wordt voorgesteld door het programma van de CCU.
  **Belangrijk!!** --> klopt dit? (tomg)

### Wat moet de arduino van de CCU kunnen doen? ###
+ De arduino moet data kunnen ontvangen van de arduino van een huisje en 
  doorsturen naar de verwerkinsgeenheid van de database.
+ De arduino moet data kunnen ontvangen van verwerkingseenheid van de database
  en doorsturen naar de arduino van het huisje.

Klassenontwerp versie 1
------------------------
Zie ook UML diagram.
### Arduino van een huisje ###
De klasse **Controller** stelt de arduino microconroller voor. Een controller 
heeft een huisnummer dat verwijst naar het huis waar de controller toe behoort. 
Daarnaast heeft een controller actuatoren, sensoren en animaties. De controller 
heeft een methode sendDataToCCU() om gevraagde data te sturen naar de CCU. Deze 
data wordt eerst gecodeerd alvorens verstuurd te worden. Op analoge wijze kan de
controller instructies ontvangen in gecodeerde vorm. Een controller 
interpreteert deze instructies door de ontvangen data eerst te decoderen en 
vervolgens zijn actuators, sensors en animaties aan te sturen.

De klasse **Actuator** stelt de actuatoren voor die in het modelhuisje aanwezig
zijn. Voorbeelden zijn LED's en elektromtoren. Elke actuator behoort tot een 
controller.

De klasse **Animation** stelt animaties voor die de status van een toestel
kunnen visualiseren. Een animatie heeft één of meerdere actuatoren, een aantal
animation states, een stop state, een current state en een vlag *running* die 
aangeeft of de animatie aan het afspelen is of niet. Een animatie die aan het
afspelen is, loopt doorheen de animation states tot de current state en begint 
zo terug vanaf de eerste state. Een animatie kan gestopt worden waarop de 
current animation state continu wordt getoond. Een animation state die de 
stop state bereikt wordt automatisch onderbroken. Het is mogelijk de animatie 
terug te starten met de methode start().

Besluit
--------
