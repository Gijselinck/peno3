#include "Arduino.h" //access to standard Arduino functions
#include "Controller.h" //access to definitions in header file

class Actuator;

/**
 * A class of controllers involving a house number, a shift register,
 *   a latch pin, a clock pin and a data pin.
 *
 * @version 1.0
 * @author	Tom Gijselinck <tomgijselinck@gmail.com>
 */


Controller::Controller(int houseNumber, int numberOfActuators) {
 	_houseNumber = houseNumber;
 	_numberOfActuators = numberOfActuators;
 	_actuators[numberOfActuators];
 	_dataPin = 11; //blue
 	_latchPin = 12; //green
 	_clockPin = 13; //yellow
 	for (int i = 0; i < 8; i++){
          _shiftRegister[i] = 0;
   	}
 	pinMode(_latchPin, OUTPUT);
 	pinMode(_clockPin, OUTPUT);
 	pinMode(_dataPin, OUTPUT);
 }

int Controller::getHouseNumber() {
	return _houseNumber;
}

bool Controller::getShiftRegister() {
 	return _shiftRegister;
 }

bool Controller::getValueAtRegisterPin(int pin) {
	return _shiftRegister[pin];
}

void Controller::writeToShiftRegister(int pin, bool value) {
	_shiftRegister[pin] = value;
	//convert binary to decimal
	int decimal = 0;
	if (_shiftRegister[0] == 1) {decimal += 1;} //2^0
	for (int i = 1; i < 8; i++) {
		int pow2 = 0;
		if (_shiftRegister[i] == 1) {
			pow2 = 1;
			for (int j = 0; j < i; j++) {
				pow2 *= 2;
			}
		}
		decimal += pow2;
	}
	//write register values to register
	digitalWrite(_latchPin, LOW);
	shiftOut(_dataPin, _clockPin, MSBFIRST, decimal);
	digitalWrite(_latchPin, HIGH);
}

void Controller::shiftRegisterSetAllTrue() {
	for (int i = 0; i < 8; i++) {
		_shiftRegister[i] = 1;
	}
	digitalWrite(_latchPin, LOW);
	shiftOut(_dataPin, _clockPin, MSBFIRST, 255);
	digitalWrite(_latchPin, HIGH);
}

void Controller::shiftRegisterSetAllFalse() {
	for (int i = 0; i < 8; i++) {
		_shiftRegister[i] = 0;
	}
	digitalWrite(_latchPin, LOW);
	shiftOut(_dataPin, _clockPin, MSBFIRST, 0);
	digitalWrite(_latchPin, HIGH);
}

void Controller::senDataToCCU(int data) {}

void Controller::interpretInstruction(int instruction) {}

/**
 * Uitleg gebruik array indices:
 *	Voor n elementen in een array geldt dat het i-de element zicht bevindt op 
 *	adres i-n. Proefondervindelijk bepaald dat dit (voorlopig als enigste 
 *	manier) werkt.
 */
Actuator Controller::getActuatorAt(int actuatorNumber) {
	//Serial.print("i - n = "); Serial.print(actuatorNumber - _numberOfActuators); Serial.print(" // ");
	//Serial.print("i: "); Serial.println(actuatorNumber);
	//Actuator tmp3 = _actuators[actuatorNumber - _numberOfActuators];
	//int pin = _actuators[actuatorNumber - _numberOfActuators].getPin();
	//Serial.print("pin: "); Serial.println(pin);
	return _actuators[actuatorNumber - _numberOfActuators];
}

/**
 * Uitleg gebruik array indices:
 *	Voor n elementen in een array geldt dat het i-de element zicht bevindt op 
 *	adres i-n. Proefondervindelijk bepaald dat dit (voorlopig als enigste 
 *	manier) werkt.
 */
void Controller::addAsActuator(Actuator *actuator, int actuatorNumber) {
	_actuators[actuatorNumber - _numberOfActuators] = *actuator;
}




/**
 * A class of actuators involving a status and an arduino pin.
 *
 * @version 1.0
 * @author	Tom Gijselinck
 */

Actuator::Actuator(char pin, Controller *controller) {
	_controller = controller;
	pinMode(pin, OUTPUT);
	_pin = pin; //immutable
	setStatus(false);
}

bool Actuator::getStatus() {
	return _status;
}

void Actuator::setStatus(bool status) {
	if (getPin() > 10) {
		_controller->writeToShiftRegister(getPin()-11, status);
	} else {
		//not on shift register
		if (status == 1) {
			digitalWrite(getPin(), HIGH);
		} else {
			digitalWrite(getPin(), LOW);
		}
	}
	_status = status;

}

int Actuator::getPin() {
	return _pin;
}



/**
 * A class of animations involving a lot of things.
 *
 * @version 1.0
 * @author	Robbert Camps <me@robbertc5.com>
 */

Animation::Animation(int numberOfActuators, int numberOfAnimationStates) {
	_numberOfActuators = numberOfActuators;
	_numberOfAnimationStates = numberOfAnimationStates;
	_actuators[_numberOfActuators];
	_animationStates[_numberOfAnimationStates][_numberOfActuators];
	_currentState = 0;
	_stopState = _numberOfAnimationStates-1;
	_startState = 0;
	_running = false;
	setActuators();
}

void Animation::addAsActuator(Actuator *actuator, int actuatorNumber) {
	//Serial.println(actuatorNumber);
	_actuators[actuatorNumber - _numberOfActuators] = *actuator;
}

void Animation::addState(int state[], int stateNumber) {
	for(int i = 0; i < _numberOfActuators; i++){
		Serial.println(i);
  		_animationStates[stateNumber][i] = state[i];
	}
	Serial.println(_animationStates[stateNumber][0]);
	Serial.println(_animationStates[stateNumber][1]);
	Serial.println(_animationStates[stateNumber][2]);
}

void Animation::setStartState(int state) {
  	_startState = state;
}


void Animation::setStopState(int state) {
  	_stopState = state;
}

void Animation::nextState() {
	if(_running==true){
		_currentState = _currentState + 1;
		if(_currentState > _stopState){
			_currentState = _startState;
		}
		// Serial.println("Nextie");
		setActuators();
	}
}

void Animation::setActuators() {
	for (int i = 0; i < _numberOfActuators; ++i)
		{
			//_actuators[i].setStatus(*_animationStates[_currentState][i]);
			_actuators[i - _numberOfActuators].setStatus(_animationStates[_currentState][i]);
			// Serial.print("Zet actuator om");
			/*Serial.print(_actuators[i - _numberOfActuators].getPin());
			Serial.print("   ");
			Serial.println(_animationStates[_currentState][i]);*/
		}
}

void Animation::start() {
	_running = true;
}

void Animation::stop(){
	_running = false;
}

void Animation::setState(int state){
	_currentState = state;
	setActuators();
}


/**
 * A class of sensors involving an arduino pin.
 *
 * @version 1.0
 * @author	Tom Gijselinck <tomgijselinck@gmail.com>
 */
Sensor::Sensor(int pin, Controller *controller) {
	_controller = controller;
	pinMode(pin, INPUT);
	_pin = pin;
}

int Sensor::getPin() {
	return _pin;
}

int Sensor::getReading() {
	return analogRead(_pin);
}