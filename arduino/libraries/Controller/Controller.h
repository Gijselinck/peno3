#include "Arduino.h"

class Actuator;

/**
 * A class of controllers involving a house number and a shift register.
 *
 * @version 1.0
 * @author	Tom Gijselinck <tomgijselinck@gmail.com>
 */
class Controller
{
  public:

 	/**
	 * Initialize this new controller with given house number.
	 *
	 * @param	houseNumber
	 *			The house number for this new controller.
	 */
 	Controller(int houseNumber, int numberOfActuators);

	/**
	 * Return the house number of this controller.
	 */
 	int getHouseNumber();

 	/**
	 * Return the current contents of the shift register of this 
	 * controller.
	 */
 	bool getShiftRegister();

 	bool getValueAtRegisterPin(int pin);

 	/**
	 * Set value of the given register pin of the register of this
	 * controller to the given value.
	 *
	 * @param	pin
	 *			The register pin to set the new value of the register of
	 *			this controller.
	 * @param	value
	 *			The new value to set at the given register pin of the 
	 *			register of this controller.
	 */
 	void writeToShiftRegister(int pin, bool value);

 	/**
	 * Set all register pins of the register of this controller to 1.
	 */
 	void shiftRegisterSetAllTrue();

 	/**
	 * Set all register pins of the register of this controller to 0.
	 */
 	void shiftRegisterSetAllFalse();

 	/**
	 * Send data to CCU.
	 *
	 * @param	data
	 *			The data to be send.
	 */
 	void senDataToCCU(int data);

 	/**
	 * Interpret the given instruction and execute it's commands.
	 *
	 * @param	instruction
	 *			The instruction to interpret and execute.
	 */
 	void interpretInstruction(int instruction);

 	/**
 	 * Return the actuator of this controller connected at the given pin.
 	 *
 	 * @param	pin
 	 *			The pin of the actuator to be returned.
 	 */
 	Actuator getActuatorAt(int actuatorNumber);

 	/**
 	 * Add the given actuator to the list of actuators attached to this 
 	 * controller.
 	 *
 	 * @param	actuator
 	 *			The adress of the actuator to be added (use &).
 	 */
 	void addAsActuator(Actuator *actuator, int actuatorNumber);



  private:

 	/**
 	 * Variable referencing the house number of this controller.
 	 */
 	int _houseNumber;

 	/**
 	 * Variable referencing the contents of the shift register of
 	 * this controller.
 	 */
 	bool _shiftRegister[8];

 	/**
 	 * Variable referencing the latch pin of this controller.
 	 */
 	int _latchPin;

 	/**
 	 * Variable referencing the clock pin of this controller.
 	 */
 	int _clockPin;

 	/**
 	 * Variable referencing the data pin of this controller.
 	 */
 	int _dataPin;

 	/**
 	 * Variable referencing the number of actuators attached to this controller.
 	 */
 	int _numberOfActuators;

 	/**
 	 * Variable referencing the actuators connected to this controller.
 	 */
 	Actuator *_actuators;
};


/**
 * A class of actuators involving a status and an arduino pin.
 *
 * @version 1.0
 * @author	Tom Gijselinck <tomgijselinck@gmail.com>
 */
class Actuator
{
  public:

  	/**
 	   * Initialize this new actuator with given pin.
 	   *
 	   * @param	pin
 	   *		The arduino pin for this new actuator.
 	   * @param controller
 	   *		The adress of the controller for this new actuator (use &).
 	   */
  	Actuator(char pin, Controller *controller);

  	/**
	   * Return the status of this actuator.
	   */
  	bool getStatus();

    /**
  	 * Set the status of this actuator to the given status.
  	 *
  	 * @param	status
  	 * 			The new status for this actuator.
  	 */
  	void setStatus(bool status);

	  /**
	   * Return the arduino pin of this actuator.
	   */
  	int getPin();



  private:

  	/**
  	 * Variable referencing the status of this actuator.
  	 */
  	bool _status;

  	/**
  	 * Variable referencing the pin number of the arduino board where
  	 * this actuator is connected to.
  	 */
  	int _pin;

    /**
     * Variable (pointer) referencing the controller of this actuator.
     */
    Controller * _controller;
};


/**
 * A class of animations involving a lot of things.
 *
 * @version 1.0
 * @author	Robbert Camps <robbert_camps@hotmail.com>
 */
class Animation
{
  public:

  	/**
 	 * Initialize this new animation with given actuators and animation states.
 	 *
 	 * @param	actuators
 	 *			An array with the actuators that will perform the animation.
	 * @param	animationstates
	 *			An array with the states of the animation. Every state is represented by
	 *			an array of actuator states.
 	 */
  	Animation(int numberOfActuators, int numberOfAnimationStates);

  	/**
	 * Add a state to the animation
	 *
	 * @param	stateNumber
	 * 			The number of the state in the animation.
	 * @param	state
	 * 			A list of states of actuators.
	 */
  	void addState(int state[], int stateNumber);

 	/**
 	 * Add the given actuator to the list of actuators attached to this 
 	 * animation.
 	 *
 	 * @param	actuator
 	 *			The adress of the actuator to be added (use &).
 	 */
 	void addAsActuator(Actuator *actuator, int actuatorNumber);

  	/**
	 * Set the state after wich the animation has to restart from the startstate.
	 *
	 * @param	state
	 * 			The state after wich the animation restarts.
	 */
  	void setStopState(int state);

  	/**
	 * Set the state wich the animation has to restart to.
	 *
	 * @param	state
	 * 			The state after wich the animation restarts.
	 */
  	void setStartState(int state);
	
	/**
	 * Update all the actuators to the next state.
	 */
  	void nextState();
	
	/**
	 * Actualise all the actuators.
	 */
  	void setActuators();
	
	/**
	 * Start the animation.
	 */
  	void start();
	
	/**
	 * Stop the animation.
	 */
  	void stop();

  	/**
	 * Set the state.
	 *
	 * @param	state
	 * 			The index of the state that needs to be actualized.
	 */
  	void setState(int state);


  private:

  	/**
 	 * Variable referencing the number of actuators controlled by this animation..
 	 */
 	int _numberOfActuators;
	
  	/**
  	 * Variable referencing a list of lists of the states of each actuator in each animation state.
  	 */
  	int _animationStates[6][6];

  	/*
  	 * Variable referencing the number of animationStates.
  	 */
  	int _numberOfAnimationStates;
	
	/**
  	 * Variable referencing the status to wich the animation will restart.
  	 */
  	int _startState;

  	/**
  	 * Variable referencing the status after wich the animation will restart.
  	 */
  	int _stopState;
	
  	/**
  	 * Variable referencing the current status of this animation.
  	 */
  	int _currentState;

  	/**
  	 * Variable referencing whether the animation is running or not.
  	 */
  	bool _running;


  	/**
  	 * Variable referencing a list the different actuators.
  	 */
  	Actuator *_actuators;

};


/**
 * A class of sensors involving an arduino pin.
 *
 * @version 1.0
 * @author	Tom Gijselinck <tomgijselinck@gmail.com>
 */
class Sensor
{
  public:
	/**
 	 * Initialize this new sensor with given pin.
 	 *
 	 * @param	pin
 	 *		The arduino pin for this new sensor.
 	 * @param controller
 	 *		The adress of the controller for this new sensor (use &).
 	 */
  	Sensor(int pin, Controller *controller);

  	/**
  	 * Return the arduino pin of this sensor.
  	 */
  	int getPin();

  	/**
  	 * Return the reading of this sensors.
  	 */
  	int getReading();


  private:
  	/**
  	 * Variable referencing the arduino pin of this sensor.
  	 */
  	int _pin;

  	/**
  	 * Variable referencing the controller of this actuator.
  	 */
  	Controller * _controller;
};