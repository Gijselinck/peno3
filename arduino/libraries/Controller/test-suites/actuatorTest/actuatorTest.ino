#include "Controller.h"

/**
 * A test program collecting tests for the class of actuators.
 *
 * @version  1.1
 * @author   Tom Gijselinck
 */

Controller controller(1, 3);
//actuator with ordinary pin
Actuator actuator2(2, &controller);
//actuator with extended pin number (using shift register)
Actuator actuator16(16, &controller);  
//actuator using extended shift register, border value
Actuator actuator11(11, &controller);

//test variables
int error = 0;
int noTest = 0;

void setup() {
  Serial.begin(9600);
}

void loop() {
  Serial.println("=================");
  Serial.println("===== START =====");
  Serial.println("=================");
  Serial.println("Test1: getStatus");
  assertTrue(equals(actuator2.getStatus(), false));
  Serial.println("Test2: getPin");
  assertTrue(equals(actuator2.getPin(), 2));
  Serial.println("Test3: setStatus");
  actuator2.setStatus(true);
  assertTrue(equals(actuator2.getStatus(), true));
  Serial.println("-----------------------");
  Serial.println("Number of tests: ");
  Serial.println(noTest);
  Serial.println("Number of errors: ");
  Serial.println(error);
  
  //VISUAL TESTS
  Serial.println("-----------------------");
  Serial.println("Running visual tests now ...");
  // visual test: off-on-off-on (led pin 16)
  actuator2.setStatus(false);
  delay(1000);
  actuator2.setStatus(true);
  delay(1000);
  actuator2.setStatus(false);
  delay(1000);
  actuator2.setStatus(true);
  delay(1000);
  // visual test: on-off-on (led pin 16)
  actuator16.setStatus(true);
  delay(1000);
  actuator16.setStatus(false);
  delay(1000);
  actuator16.setStatus(true);
  delay(1000);
  // visual test: on-off-on (led pin 10)
  actuator11.setStatus(true);
  delay(1000);
  actuator11.setStatus(false);
  delay(1000);
  actuator11.setStatus(true);
  delay(1000);
  controller.shiftRegisterSetAllFalse();
  
  delay(4000);
}


//TEST FUNCTIONS
boolean equals(boolean A, boolean B) {
  noTest += 1;
  return A == B;
}


boolean equals(int A, int B) {
  noTest += 1;
  return A == B;
}

boolean assertTrue(boolean test) {
  if (test == false) {
    error += 1;
    Serial.println("0 --> test failed");
  } else {
    Serial.println(1);
  }
  return test;
}

boolean assertFalse(boolean test) {
  if (test == true) {
    error += 1;
    Serial.println("0 --> test failed");
  } else {
    Serial.println(1);
  }
  return !test;
}
