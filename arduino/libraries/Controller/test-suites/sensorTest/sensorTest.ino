#include "Controller.h"

/**
 * A test program collecting tests for the class of actuators.
 *
 * @version  1.1
 * @author   Tom Gijselinck
 */

Controller controller(1, 3);
//actuator with ordinary pin
Actuator actuator2(2, &controller);
//actuator with extended pin number (using shift register)
Actuator actuator16(16, &controller);  
//actuator using extended shift register, border value
Actuator actuator11(11, &controller);
Sensor sensor0(0, &controller);

//test variables
int error = 0;
int noTest = 0;

void setup() {
  Serial.begin(9600);
}

void loop() {
  assertTrue(greaterThan(sensor0.getReading(), 0));
  
  //VISUAL TESTS
  Serial.println(sensor0.getReading());
  
  delay(200);
}


//TEST FUNCTIONS
boolean equals(boolean A, boolean B) {
  noTest += 1;
  return A == B;
}


boolean equals(int A, int B) {
  noTest += 1;
  return A == B;
}

boolean lessThan(int A, int B) {
  noTest += 1;
  return A < B;
}

boolean greaterThan(int A, int B) {
  noTest += 1;
  return A > B;
}

boolean assertTrue(boolean test) {
  if (test == false) {
    error += 1;
    Serial.println("0 --> test failed");
  } else {
    Serial.println(1);
  }
  return test;
}

boolean assertFalse(boolean test) {
  if (test == true) {
    error += 1;
    Serial.println("0 --> test failed");
  } else {
    Serial.println(1);
  }
  return !test;
}
