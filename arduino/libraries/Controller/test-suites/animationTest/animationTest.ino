#include "Controller.h"

/**
 * A test suite collecting tests for the class of controllers.
 *
 * @version  1.0
 * @author   Tom Gijselinck <tomgijselinck@gmail.com>
 */

Controller controller(1, 3);
//actuator with ordinary pin
Actuator actuator11(11, &controller);
//actuator with extended pin number (using shift register)
Actuator actuator12(12, &controller);  
//actuator using extended shift register, border value
Actuator actuator13(13, &controller);

// Make animation1 with 3 LED's and 5 states
Animation animation1(3,5);




//test variables
int error = 0;
int noTest = 0;

void setup() {
  Serial.begin(9600);
  
  // Add the actuators
  animation1.addAsActuator(&actuator11, 0);
  animation1.addAsActuator(&actuator12, 1);
  animation1.addAsActuator(&actuator13, 2);
  // The states
  int states[][3] = {{0,0,0},{0,0,1},{0,1,1},{1,1,1},{1,0,1}};
  for(int i=0; i<5; i++){
    animation1.addState(states[i],i);
  }
  // Make it running
  animation1.start();
}

void loop() {
  Serial.println("=================");
  Serial.println("===== START =====");
  Serial.println("=================");
  Serial.println("Looping...");
  for(int i = 0; i<10; i++){
    animation1.nextState();
    delay(500);
  }
  Serial.println("Stop.");
  animation1.stop();
  for(int i = 0; i<10; i++){
    animation1.nextState(); // this should do nothing now
    delay(500);
  }
  Serial.println("Everything out");
  animation1.setState(0);
  delay(2000);
  Serial.println("The last (101) state");
  animation1.setState(4);
  delay(2000);
  Serial.println("Everything on");
  animation1.setState(3);
  delay(2000);
  Serial.println("Starting again");
  animation1.start();
  for(int i = 0; i<10; i++){
    animation1.nextState(); // this should do nothing now
    delay(500);
  }
  Serial.println("Cutting off the last state");
  animation1.setStopState(3);
  for(int i = 0; i<20; i++){
    animation1.nextState(); // this should do nothing now
    delay(500);
  }
  Serial.println("First stay on");
  animation1.setStartState(1);
  for(int i = 0; i<20; i++){
    animation1.nextState(); // this should do nothing now
    delay(500);
  }
  Serial.println("Second stay on");
  animation1.setStartState(2);
  for(int i = 0; i<20; i++){
    animation1.nextState(); // this should do nothing now
    delay(500);
  }
  Serial.println("Restoring to normal");
  animation1.setStartState(0);
  animation1.setStopState(4);
  for(int i = 0; i<20; i++){
    animation1.nextState(); // this should do nothing now
    delay(500);
  }
  /*error = 0;
  noTest = 0;
  Serial.println("=================");
  Serial.println("===== START =====");
  Serial.println("=================");
  Serial.println("Test1: getHouseNumber");
  assertTrue(equals(controller.getHouseNumber(), 1));
  Serial.println("Test2: writeToShiftRegister");
  controller.writeToShiftRegister(0, 1);
  assertTrue(equals(controller.getValueAtRegisterPin(0), 1));
  assertFalse(equals(controller.getValueAtRegisterPin(1), 1));
  assertTrue(equals(controller.getValueAtRegisterPin(7), 0));
  Serial.println("Test3: addAsActuator");
  controller.addAsActuator(&actuator2, 0);
  controller.addAsActuator(&actuator11, 1);
  controller.addAsActuator(&actuator16,2);
  assertTrue(equals(controller.getActuatorAt(0).getPin(), actuator2.getPin()));
  assertTrue(equals(controller.getActuatorAt(1).getPin(), actuator11.getPin()));
  assertTrue(equals(controller.getActuatorAt(2).getPin(), actuator16.getPin()));
  Serial.println("-----------------------");
  Serial.println("Number of tests: ");
  Serial.println(noTest);
  Serial.println("Number of errors: ");
  Serial.println(error);
  
  // visual test
  Serial.println("-----------------------");
  Serial.println("Running visual tests now ...");
  controller.shiftRegisterSetAllTrue();
  delay(1000);
  controller.shiftRegisterSetAllFalse();
  delay(1000);
  controller.writeToShiftRegister(2, 1);
  delay(1000);
  controller.writeToShiftRegister(5, 1);
  delay(1000);
  controller.writeToShiftRegister(6, 1);
  delay(1000);
  controller.writeToShiftRegister(5, 0);
  delay(2000);
  controller.shiftRegisterSetAllTrue();
  delay(1000);
  controller.shiftRegisterSetAllFalse();
  delay(1000);
  controller.shiftRegisterSetAllTrue();
  delay(250);  
  controller.shiftRegisterSetAllFalse();*/
  
  delay(500);
}


//test functions
boolean equals(boolean A, boolean B) {
  noTest += 1;
  return A == B;
}


boolean equals(int A, int B) {
  noTest += 1;
  return A == B;
}

boolean assertTrue(boolean test) {
  if (test == false) {
    error += 1;
    Serial.println("0 --> test failed");
  } else {
    Serial.println(1);
  }
  return test;
}

boolean assertFalse(boolean test) {
  if (test == true) {
    error += 1;
    Serial.println("0 --> test failed");
  } else {
    Serial.println(1);
  }
  return !test;
}
