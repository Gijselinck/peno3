#include <Controller.h>

/**
 * A test suite collecting tests for the class of controllers.
 *
 * @version  1.0
 * @author   Tom Gijselinck <tomgijselinck@gmail.com>
 */

Controller controller(1, 3);
//actuator with ordinary pin
Actuator actuator2(2, &controller);
//actuator with extended pin number (using shift register)
Actuator actuator16(16, &controller);  
//actuator using extended shift register, border value
Actuator actuator11(11, &controller);

//test variables
int error = 0;
int noTest = 0;

void setup() {
  Serial.begin(9600);
}

void loop() {
  error = 0;
  noTest = 0;
  Serial.println("=================");
  Serial.println("===== START =====");
  Serial.println("=================");
  Serial.println("Test1: getHouseNumber");
  assertTrue(equals(controller.getHouseNumber(), 1));
  Serial.println("Test2: writeToShiftRegister");
  controller.writeToShiftRegister(0, 1);
  assertTrue(equals(controller.getValueAtRegisterPin(0), 1));
  assertFalse(equals(controller.getValueAtRegisterPin(1), 1));
  assertTrue(equals(controller.getValueAtRegisterPin(7), 0));
  Serial.println("Test3: addAsActuator");
  controller.addAsActuator(&actuator2, 0);
  controller.addAsActuator(&actuator11, 1);
  controller.addAsActuator(&actuator16,2);
  assertTrue(equals(controller.getActuatorAt(0).getPin(), actuator2.getPin()));
  assertTrue(equals(controller.getActuatorAt(1).getPin(), actuator11.getPin()));
  assertTrue(equals(controller.getActuatorAt(2).getPin(), actuator16.getPin()));
  Serial.println("-----------------------");
  Serial.println("Number of tests: ");
  Serial.println(noTest);
  Serial.println("Number of errors: ");
  Serial.println(error);
  
  // visual test
  Serial.println("-----------------------");
  Serial.println("Running visual tests now ...");
  controller.shiftRegisterSetAllTrue();
  delay(1000);
  controller.shiftRegisterSetAllFalse();
  delay(1000);
  controller.writeToShiftRegister(2, 1);
  delay(1000);
  controller.writeToShiftRegister(5, 1);
  delay(1000);
  controller.writeToShiftRegister(6, 1);
  delay(1000);
  controller.writeToShiftRegister(5, 0);
  delay(2000);
  controller.shiftRegisterSetAllTrue();
  delay(1000);
  controller.shiftRegisterSetAllFalse();
  delay(1000);
  controller.shiftRegisterSetAllTrue();
  delay(250);  
  controller.shiftRegisterSetAllFalse();
  
  delay(4000);
}


//test functions
boolean equals(boolean A, boolean B) {
  noTest += 1;
  return A == B;
}


boolean equals(int A, int B) {
  noTest += 1;
  return A == B;
}

boolean assertTrue(boolean test) {
  if (test == false) {
    error += 1;
    Serial.println("0 --> test failed");
  } else {
    Serial.println(1);
  }
  return test;
}

boolean assertFalse(boolean test) {
  if (test == true) {
    error += 1;
    Serial.println("0 --> test failed");
  } else {
    Serial.println(1);
  }
  return !test;
}
