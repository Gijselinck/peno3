#include <Actuator.h>

/**
 * A test program collecting tests for the class of actuators.
 *
 * @version  1.0
 * @author   Tom Gijselinck
 */

Actuator actuator(13);
int error = 0;
int noTest = 0;

void setup() {
  Serial.begin(9600);
}

void loop() {
  Serial.println("Test1: getStatus");
  testBoolEqual(actuator.getStatus(), false);
  Serial.println("Test2: getPin");
  testIntEqual(actuator.getPin(), 13);
  Serial.println("Test3: setStatus");
  actuator.setStatus(true);
  testBoolEqual(actuator.getStatus(), true);
  String message;
  Serial.println("==========================");
  Serial.println("Number of tests: ");
  Serial.println(noTest);
  Serial.println("Number of errors: ");
  Serial.println(error);
  
  // visual test: off-on-off-on (led pin 13)
  actuator.setStatus(false);
  delay(500);
  actuator.setStatus(true);
  delay(500);
  actuator.setStatus(false);
  delay(500);
  actuator.setStatus(true);
  delay(500);
  
  delay(999000);
}

void testBoolEqual(boolean A, boolean B) {
  noTest += 1;
  boolean result = A == B;
  if (result == false) {
    error += 1;
  }
  Serial.println(result);
}


void testIntEqual(int A, int B) {
  noTest += 1;
  boolean result = A == B;
  if (result == false) {
    error += 1;
  }
  Serial.println(result);
}
  
