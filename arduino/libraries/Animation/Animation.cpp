#include "Arduino.h" //access to standard Arduino functions
#include "Animation.h" //access to definitions in header file
// @todo include standard cplusplus libraries !

/**
 * A class of animations involving a lot of things.
 *
 * @version 1.0
 * @author	Robbert Camps <robbert_camps@hotmail.com>
 */

Animation::Animation(Actuator& actuators[], int animationStates[][]) 
	_actuators(actuators)
{
	_animationStates = animationStates;
	_currentState = 0;
	_stopState = animationStates.end();
	_running = false;
}

void setStopState(int state) {
  	_stopState = state;
}

void nextState() {
	if(_running==true){
		_currentState = (_currentState + 1)%_stopState;
		setActuators();
	}
}

void setActuators() {
	for (int i = 0; i < _actuators; ++i)
		{
			_actuators[i].setStatus(animationStates[_currentState][i]);
		}
}

void start() {
	_running = true;
}

void stop(){
	_running = false;
}

void setState(int state){
	_currentState = state;
	setActuators();
}