#include "Arduino.h"
#include "Actuator.h"

/**
 * A class of animations involving a lot of things.
 *
 * @version 1.0
 * @author	Robbert Camps <robbert_camps@hotmail.com>
 */
class Animation
{
  public:

  	/**
 	 * Initialize this new animation with given actuators and animation states.
 	 *
 	 * @param	actuators
 	 *			An array with the actuators that will perform the animation.
	 * @param	animationstates
	 *			An array with the states of the animation. Every state is represented by
	 *			an array of actuator states.
 	 */
  	Animation(Actuator& actuators[], array animationStates[]);

  	/**
	 * Set the state after which the animation has to restart from state zero.
	 *
	 * @param	state
	 * 			The state after which the animation restarts.
	 */
  	void setStopState(int state);
	
	/**
	 * Update all the actuators to the next state.
	 */
  	void nextState();
	
	/**
	 * Actualise all the actuators.
	 */
  	void setActuators();
	
	/**
	 * Start the animation.
	 */
  	void start();
	
	/**
	 * Stop the animation.
	 */
  	void stop();

  	/**
	 * Set the state.
	 *
	 * @param	state
	 * 			The index of the state that needs to be actualized.
	 */
  	void setState(int state);



  private:

  	/**
  	 * Variable referencing a list the different actuators.
  	 */
  	Actuator _actuators;
	
  	/**
  	 * Variable referencing a list of lists of the states of each actuator in each animation state.
  	 */
  	array _animationStates[];
	
  	/**
  	 * Variable referencing the status after which the animation will restart.
  	 */
  	int _stopState;
	
  	/**
  	 * Variable referencing the current status of this animation.
  	 */
  	int _currentState;

  	/**
  	 * Variable referencing whether the animation is running or not.
  	 */
  	bool _running;
};