Opmerkingen bij het gebruik van de RF-communicatie wrapper
===========================================================

Gebruik
---------

Om succesvol te compilen moet de VirtualWire-library ge�nstalleerd zijn in de libraries-map van de Arduino IDE. Wegens beperkingen van de IDE is er hier geen ontkomen aan. De VirutalWire-library kan gedownload worden op [de projectpagina](http://www.airspayce.com/mikem/arduino/VirtualWire/index.html).
Instructies voor het installeren van libraries in het algemeen zijn te vinden op [http://arduino.cc/en/Reference/Libraries](http://arduino.cc/en/Reference/Libraries).

Als VirtualWire ge�nstalleerd is, moet je om de wrapper te gebruiken de bestanden `rfwrap.h` en `rfwrap.cpp` in de map plaatsen van de sketch waarin die gebruikt zullen worden. Voeg ook de code `#include "rfwrap.h"` toe aan het begin van de sketch. Indien de sketch nog open was in de Arduino IDE moet je deze eerst opnieuw opstarten alvorens te compilen. 

Beperkingen opgelegd aan de Arduino door VirutalWire
-----------------------------------------------------

VirtualWire gebruikt timer interrupts om communicatiemogelijkheden te voorzien zonder de code te onderbreken totdat de transmissie is voltooid. In het kort komt het erop neer dat om de x klok-cycli van de processor (dus om een vast aantal milliseconden) de code waaraan de Arduino bezig is, wordt onderbroken om wat verzend- en ontvang-bewerkingen te doen. Dit is heel kort en daardoor lijkt de code in de `loop()`-functie, waar geen expliciet verzend/ontvang werk wordt gedaan, zonder onderbreking verder te gaan. Het probleem is dat de Arduino Uno slechts 3 timers heeft, die ook voor enkele "natuurlijke" Arduino functies worden gebruikt. Door `timer1` te gebruiken op de Arduino, breekt VirutualWire dus de volgende functionaliteiten:

* de servo-library is incompatibel
* PWM op pins 9 and 10: `AnalogWrite()` werkt niet meer op deze pinnen

VirtualWire kan ook niet zomaar een andere timer gebruiken omdat `timer1` anders telt dan 0 en 2 (andere counter-waarde), en nul ook intensief wordt gebruikt door de functie `delay()` en dergelijke tijdgerelateerde zaken.

Beperkingen
------------

Todo
