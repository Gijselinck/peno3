//#define RAW_PRINT
//enum met types van data die verzonden kan worden
enum rf_data_type { nul, text, u_char, integer, u_integer, floating}; 
enum command_type { GET, SET, CONFIRM, RESPONSE};

//Const
const int MAX_MESSAGE_LEN = 77; // vw_max_len = 80, min 3 controle bits

// Prototypes voor functies in rfwrap.cpp hier

// Arduino doet dit eigenlijk standaard, byte is eig geen standaard c++: voor de volledigheid toegevoegd.
typedef unsigned char byte;
typedef unsigned char uint8_t;
typedef signed char int8_t;

void rf_setup(const char device_id, uint8_t rx_pin = 11, uint8_t tx_pin = 12, int8_t ppt_pin = -1, unsigned int speed = 2000);


// bij oproepen van deze functies, zorg dat de namen null-terminated strings zijn! 
// dus eindigen met '\0'. C++ doet dit automatisch
// als je een string schrijft in code "op deze manier".

bool rf_set( const char* var_name, byte* data, char target_id, 
		uint8_t length = 1, bool wait_on_response = false, rf_data_type data_type = u_char);
bool rf_set( const char* var_name, const char* data, char target_id, 
		uint8_t length = 1, bool wait_on_response = false);
bool rf_set( const char* var_name, const int* data, char target_id, 
		uint8_t length = 1, bool wait_on_response = false);
bool rf_set( const char* var_name, const unsigned int* data, char target_id, 
		uint8_t length = 1, bool wait_on_response = false);
bool rf_set( const char* var_name, const float* data, char target_id, 
		uint8_t length = 1, bool wait_on_response = false);

// klasse voor het ontbinden en bijhouden van de inhoud van een ontvangen bericht
// Opgepast: methodes van deze klasse werken met poniters , ook in de klasse worden
// pointers naar arrays opgelsagen ipv kopieën van die arrays. Dit bespaart geheugen,
// MAAR zorg dat de array waarnaar het verwijst dan in scope blijft en dus niet  als lokale variabele vernieteigd
// wordt aan het einde van een routine.
class rf_msg
{
private:
  void pretty_print_header();
  void swap( rf_msg &other);
  void shallow_copy(rf_msg &target, rf_msg &source);
 
protected:
	bool _is_valid;
	
public:
	// voor de eenvoudigheid, geen setters en getters
	byte	_sender_id;
	byte	_receiver_id;
	char*	_var_name;
	uint8_t	_var_name_len;
	byte*	_data_array;
	uint8_t	_data_len;
	rf_data_type 	_type;
	command_type 	_cmd_type;
	bool 	_wait_on_response;

	
	//default contstructor, initialiseerd _is_valid op false en laat de rest open
	rf_msg();
	
	// Copy Constructor & Ansignment operator overload
	// Nodig omdat we met dynamisch gegeheugen werken en je dus niet wil dat letterlijk
	// de pointers naar dit dynamisch gegheugen worden gekopieerd (bv het teruggeven
	// van een instantie door een functie), omdat dan het vernietigen van het ene
	// impliceert dat het geuheugen waar beide naar verwijzen wordt vrijgegeven
	rf_msg( const rf_msg &msg_to_copy);
	rf_msg& operator= (const rf_msg &other);

	//Cosntructor for received data
	// als dump_for_other waar is, zal er onderbroken worden met het bericht
	// te lezen als geconstateert wordç dat het bericht niet aan deze arduino gericht is
	// (id wordt met rf_setup ingesteld). Dit betekent dat het in dat geval ook niet volledig 
	// verwerkt en te leze is (geheugenbesparing)
	rf_msg( const byte* data_buf , uint8_t data_len, bool dump_for_other = true);

	//constructor to create message to get data
	// als er geen lengte van de var_name wordt opgegeven gaat het uit van een null-terminated cstring
	rf_msg( const char* var_name_to_get, uint8_t target_id, int var_name_len = -1);

	//constructor to create message to set data
	// als er geen lengte van de var_name wordt opgegeven gaat het uit van een null-terminated cstring
	rf_msg( const char* var_name_to_set, const byte* data, uint8_t data_len, rf_data_type type, uint8_t target_id, bool wait_on_response = true, int var_name_len = -1);
	
	//Destructor: geeft dynamisch geheugen weer vrij
	 ~rf_msg();
	 
	// Operator overload voor expliciete conversie naar bool
	// nuttig in de contex van een if-clause
	operator bool() const;
	
	// Atlijd eerst deze waarde checken voordat je waarden hierboven raadpleegt
	 bool is_valid();

	 // Controleert type
	 // controleert tevens of het wel een geldig bericht is
	 bool is_of_cmd_type(command_type type);
	 bool is_of_type(rf_data_type type);

	 // print bericht in vorm zoals het doorgezonden wordt naar buf_out
	 void print_to(byte* buf_out);
	 
	 // geeft lengte van wat print_to zou wegschrijven naar de buffer als argument
	 int print_len();
	 
	 // print bericht naar seriele console:leesbare commando's voor mensen
	 void pretty_print();
	
	 // verzend het bericht.
	 // In geval van een GET wacht deze fucntie op een antwoord (via ATTEMPT_WAIT en ATTEMPT_NB geregeld)
	 // en returnt true indien er een RESPONSE is ontvangen. De response wordt dan ook opgeslagen in de rf_msg
	 // waarnaar p_response_msg verwijst
	 //
	 // In geval van een SET met wait_on_response, wacht op een CONFIRM en return true indine confirm ontvangen is
	 bool send(rf_msg *p_response_msg = 0);

};

class rf_confirm : public rf_msg
{
public:
	rf_confirm(uint8_t &target_id);

};

class rf_response : public rf_msg
{
	
public:
	rf_response(byte* response_data, int data_len, uint8_t &target_id);

};

rf_msg rf_get(const char* var_name, char target_id, int var_name_len = -1);
rf_msg rf_receive(bool dump_for_other = true);

// allen bij debuggen, diagnotische functeis
#ifdef DEBUG
	int rf_test_suit();
#endif
