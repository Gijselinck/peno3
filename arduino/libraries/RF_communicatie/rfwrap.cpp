//#include <cstring> // voor functies zoals strlen, memchar, memcpy etc, zit al in Arduino.h

#include "rfwrap.h"

#include "Arduino.h" // om te printen naar de Seriële console
#include "VirtualWire.h"

// Todo:
// - stop strings in EEP-rom on Ram te ontladen
// - Test de constructor voor ontvangen met bytes


// Header eigenschappen van een bericht
const char h_name[] = "ESAT5A2";
const char h_separator = '#'; // dit getal mag nooit doorgezonden worden anders zijn er problemen! (controleer met floats, 2 byte variabelen)

const int ATTEMPT_NB = 5;
const int ATTEMPT_WAIT = 500; // in milliseconden
const int MSG_GET_ITEM_NB = 6;
const int MSG_SET_ITEM_NB = 9;

// een globale array waarin berichten voor verzenden in geconstrueerd gaan worden.
// dit is beter globaal omdat het dan niet telkens een nieuwe geheugenallocatie vraagt die kan
// leiden tot gefragmenteerd geheugen
byte msg_buf[MAX_MESSAGE_LEN];

// id van deze arduino
char h_id = -1; // -1 voor niet-geïnitialiseerde waarde


//Lokale prototypes
void add_bytes( byte* target, int &pos, const char* to_copy, int copy_len = 1);
void add_bytes( byte* target, int &pos, const char to_copy);
void add_bytes( byte* target, int &pos, const byte* to_copy, int copy_len = 1);

//bool is_correct_header(byte* message);
//bool is_response_msg(byte* message, int len, char* sender_id, byte* sent_data);
//bool is_confirm_msg(byte* message, int len, char* sender_id);

// Aan te ropen voor andere functies te gebruiken
void rf_setup( const char device_id,  uint8_t rx_pin, uint8_t tx_pin, int8_t ptt_pin, unsigned int speed){
	// stel de naam in die in de header gebruikt wordt om dit toestel te identificeren
	h_id = device_id;
	
	// setup VirtualWire bibliotheek
	vw_set_tx_pin(tx_pin);
	vw_set_rx_pin(rx_pin);
	if(ptt_pin > -1)
		vw_set_ptt_pin(ptt_pin); // deze pin gaat aan als de zender/ontvanger aan springt na aanropen vw_rx_start();
	vw_setup(speed);
	// activeer zenden/ontvangen
	vw_rx_start(); 
}


void add_bytes( byte* target, int &pos, const char* to_copy, int copy_len)
{
	for( int i = 0; i < copy_len; i++)
	{
		target[pos] = to_copy[i];
		pos++;
	}
};

void add_bytes(byte* target, int &pos, const char to_copy)
{
	add_bytes(target, pos, &to_copy);
}
void add_bytes(byte* target, int &pos, const byte* to_copy, int copy_len)
{
	add_bytes(target, pos, (const char*) to_copy, copy_len);
}


// verscheidene overloads van rf_setup
bool rf_set( const char* var_name, const byte* data, char target_id, uint8_t length, bool wait_on_response, rf_data_type data_type)
{
	// maak bericht aan
	rf_msg msg(var_name, data, length, data_type, target_id, wait_on_response);
	
	return msg.send();
}

bool rf_set( const char* var_name, const char* data, char target_id, uint8_t length, bool wait_on_response){
	return rf_set( var_name, (const byte*) data, length, target_id, wait_on_response, text);
}

bool rf_set( const char* var_name, const int* data, char target_id, uint8_t length, bool wait_on_response){
	const byte* data_p = (const byte*) data;
	//int = 2 bytes (op arduino), char 1 byte => lengte maal 2
	return rf_set( var_name, data_p, target_id, length*sizeof(int), wait_on_response, integer );
}

bool rf_set( const char* var_name, const unsigned int* data, char target_id, uint8_t length, bool wait_on_response){
	const uint8_t* data_p = (const uint8_t*) data;
	//int = 2 bytes (op arduino), char 1 byte => lengte maal 2
	return rf_set(var_name, data_p, target_id, length*sizeof(unsigned int),wait_on_response, u_integer );
}

bool rf_set( const char* var_name, const float* data, char target_id, uint8_t length, bool wait_on_response){
	const uint8_t* data_p = (const uint8_t*) data;
	//float = 4 bytes (op arduino), char 1 byte => lengte maal 4
	return rf_set(var_name, data_p, target_id, length*sizeof(float),wait_on_response, u_integer );
}


rf_msg::rf_msg( const byte* data_buf , uint8_t data_len, bool dump_for_other)
{
	// setting pointers to zero to prevent deleting random data in destructor,
	// in case these are not set in this constructor because of an invalid message.
	_var_name = 0;
	_data_array = 0;
	
	const uint8_t* curr_char = data_buf;
	
	// Controleer of het wel degelijk met '#' begint, anders fout
	if ( *curr_char == h_separator)
	{
		// Eerste teken klopt, ga 1 stap verder kijken bij begin nieuw element
		curr_char++;

		// controleer op header, indien fout: zet is_valid false, return
		// sizeof(h_name) - 1 omdat \0 op het einde niet mee doet
		if( memcmp( curr_char, h_name, sizeof(h_name) - 1 ) )
		{
			_is_valid = false;
			return;
		}
		curr_char 		+= sizeof(h_name) - 1;
		_receiver_id 	= *curr_char++;
		_sender_id 		= *curr_char++;
		_cmd_type		= (command_type) *curr_char++;
		
		if(dump_for_other) // stoppen indien het bericht niet aan deze arduino gericht is
		{
			if( _receiver_id != h_id)
			{
				_is_valid = false;
				return;
			}
		}
				
		switch(_cmd_type)
		{
			case CONFIRM:
				break;
				
			case RESPONSE:
				_data_len	= *curr_char++;
				_data_array = new byte[_data_len];
				// In geval van array's wordt gekopieerd met memcpy
				// zodanig dat als de array waar data_buf naar wijst
				// uit scope gaat de gegevens behouden blijven
				memcpy(_data_array, curr_char, _data_len);
				curr_char += _data_len;
				break;
				
				
			case SET:
			case GET:
				_var_name_len	= *curr_char++;
				_var_name		= new char[_var_name_len];
				memcpy(_var_name, curr_char, _var_name_len);
				curr_char += _var_name_len;
				// Voor get stopt het hier
				if(_cmd_type == GET)
					break;
				_data_len	= *curr_char++;
				_data_array = new byte[_data_len];
				memcpy(_data_array, curr_char, _data_len);
				curr_char	+= _data_len;
				_type		= (rf_data_type) *curr_char++;
				_wait_on_response = *curr_char++;
				break;
		}
		
		// Controle of bericht eindigt met de h_separator
		if ( *(curr_char++) != h_separator)
		{
			_is_valid = false;
			return;
		}

		// Controleer of we niet meer hebben verwerkt dan wat er gegeven is
		if ( (curr_char - data_buf) != data_len)
		{
			_is_valid = false;
			return;
		}
		
		
		// geef de zender zijn antwoord indien hij erachter vraagt
		if (_wait_on_response)
		{
			rf_confirm confirm_msg(_sender_id);
			confirm_msg.send();
		}

	}
	else
	{
		_is_valid = false;
		return;
	}

	// Op dit punt aangekomen is vorm bericht goed bevonden
	// en inhoud opgeslagen:
	_is_valid = true;
}

//constructor voor get
rf_msg::rf_msg( const char* var_name_to_get, uint8_t target_id, int var_name_len) 
{
	// get type: stel type in
	_cmd_type = GET;
	
	if ( var_name_len < 1)
	{
		_var_name_len = strlen(var_name_to_get);
	}
	else
	{
		_var_name_len = (uint8_t) var_name_len;
	}

	_receiver_id	= target_id;
	_sender_id 		= h_id;

	// wordt gekopieerd, waarnaar var_name_to_get mag best verdwijenen, het is in de klasse opgeslagen
	_var_name = new char[_var_name_len];
	memcpy(_var_name, var_name_to_get, _var_name_len);

	_data_array 	= 0;
	_data_len		= 0;
	_type 			= nul;

	_wait_on_response = false;
	_is_valid = true; 
}

// default constructor
rf_msg::rf_msg()
{
	_is_valid = false;
	_wait_on_response = false;
	// setting pointers to zero to prevent deleting random data in destructor
	_var_name = 0;
	_data_array = 0;
	_var_name_len = 0;
	_data_len = 0;
}

//Destructor: geeft dynamisch geheugen weer vrij
rf_msg::~rf_msg()
{
	delete[] _var_name;
	delete[] _data_array;
}

rf_msg::operator bool() const
{
	return _is_valid;
}

//Copy constructor
//Initialiseert nieuw object a.d.h.v oude
rf_msg::rf_msg( const rf_msg &msg_to_copy)
{
	// reserveer nieuwe geheugen ruimte om _var_name en _data_array in te stockeren
	_var_name_len	= msg_to_copy._var_name_len;
	_data_len		= msg_to_copy._data_len;
	_var_name		= new char[_var_name_len];
	_data_array		= new byte[_data_len];
	memcpy(_var_name,	msg_to_copy._var_name, _var_name_len);
	memcpy(_data_array,	msg_to_copy._data_array, _data_len);
	
	// de rest ook nog kopiëren
	_is_valid			= msg_to_copy._is_valid;
	_receiver_id		= msg_to_copy._receiver_id;
	_sender_id			= msg_to_copy._sender_id;
	_type				= msg_to_copy._type;
	_cmd_type			= msg_to_copy._cmd_type;
	_wait_on_response	= msg_to_copy._wait_on_response;
	
}

// Overlaod '='-operator
// Overschrijft bestaand object a.d.h.v oud
rf_msg& rf_msg::operator= (const rf_msg &other)
{
	
	rf_msg temp(other); // hiermee wordt de copy constructor opgeroepen
	
	// Nu moet alles uit temp in *this gestoken worden en andersom 
	// ook, zodanig dat al het oude van this, waarnaar temp nu naar verwijst,
	// deftig afgebroken wordt als het einde van scoope wordt bereikt.
	swap(temp);
		
	return *this;
}


// constructor voor set
rf_msg::rf_msg( const char* var_name_to_set, const byte* data, uint8_t data_len, rf_data_type type, uint8_t target_id, bool wait_on_response, int var_name_len)
{
	//type commando
	_cmd_type = SET;

	if (var_name_len < 1)
	{
		_var_name_len = strlen(var_name_to_set);
	}
	else
		_var_name_len = (uint8_t) var_name_len;

	_var_name		= new char[_var_name_len];
	memcpy(_var_name, var_name_to_set, _var_name_len);
	_data_len		= data_len;
	_data_array		= new byte[data_len];
	memcpy(_data_array, data, data_len);
	_type 			= type;
	_sender_id 		= h_id;
	_receiver_id 	= target_id;
	_wait_on_response = wait_on_response;

	_is_valid = true;
}


void rf_msg::shallow_copy(rf_msg &target, rf_msg &source)
{
	target._data_array  = source._data_array;
	target._data_len	= source._data_len;
	target._cmd_type	= source._cmd_type;
	target._is_valid	= source._is_valid;
	target._receiver_id	= source._receiver_id;
	target._sender_id	= source._sender_id;
	target._type		= source._type;
	target._var_name	= source._var_name;
	target._var_name_len= source._var_name_len;
	target._wait_on_response = source._wait_on_response;
}

void rf_msg::swap(rf_msg &other)
{
	rf_msg temp;
	shallow_copy(temp, *this);
	shallow_copy(*this, other);
	shallow_copy(other, temp);
	// vermijden dat de destructor van temp _data_array en _var_name delete
	// want deze pointers verwijzen door laatste regel naar hetzelfde als other 
	temp._var_name		= 0;
	temp._data_array	= 0;
}

void rf_msg::print_to(byte* buf_out)
{
	int p = 0;
	add_bytes(buf_out, p, h_separator);
	add_bytes(buf_out, p, h_name, sizeof(h_name) - 1); // -1 omdat de \0 niet mee doet
	add_bytes(buf_out, p, _receiver_id);
	add_bytes(buf_out, p, _sender_id);
	
	add_bytes(buf_out, p, (uint8_t) _cmd_type);
	
	switch(_cmd_type)
	{
		case SET:
		case GET:
			add_bytes(buf_out, p, _var_name_len);
			add_bytes(buf_out, p, _var_name, _var_name_len);

			if(_cmd_type == GET)
				break;
			
			add_bytes(buf_out, p, _data_len);
			add_bytes(buf_out, p, _data_array, _data_len);
			
			add_bytes(buf_out, p, (byte) _type);
			add_bytes(buf_out, p, (byte) _wait_on_response);
			break;

		case CONFIRM:
			// niets meer toe te voegen eigenlijk
			break;

		case RESPONSE:
			add_bytes(buf_out, p, _data_len);
			add_bytes(buf_out, p, _data_array, _data_len);
			break;

	}
#ifdef DEBUG
	if (p > MAX_MESSAGE_LEN)
	{
	  Serial.println("rf_msg::print_to : message to long");
	}
#endif
	// eindigen met separator -> extra controle
	add_bytes(buf_out, p, h_separator);
}

int rf_msg::print_len()
{
	// begint en eindigt met separator, voor h_name doet \0 op het einde niet mee
	int len =  2*sizeof(h_separator) + sizeof(h_name) - 1 + 2 * sizeof(h_id) + sizeof(byte);

	switch(_cmd_type)
	{
		case SET:
			// #bytese lengte + byte die datalengte aangeeft + 1 wait_on_response byte + 1 data_type_byte
			len += _data_len + 3;
		case GET:
			// SET voort dit ook uit
			// #bytes naam + byte voor lengte van de naam 
			len += _var_name_len + 1;
			break;
		case CONFIRM:
			break;
		case RESPONSE:
			len += _data_len + 1;
			break;
	}
	return len;
}

void rf_msg::pretty_print_header()
{
  Serial.print(F("RF Communicatie: "));
}

// print een array een per één
template <typename T>
void pretty_print_bytes( T to_print, uint8_t print_len)
{
	for(int i = 0; i < print_len; i++)
	{
		// print de bytes (gecast) als T type
		Serial.print( *to_print );
		to_print++;
	}
}

// speciefiek om de array data bytes te printen
void pretty_print_data( byte* data, uint8_t &len, rf_data_type type)
{
  // nul, text, u_char, integer, u_integer, floating, command 
  switch(type)
  {
    case text:
      pretty_print_bytes( (char*) data, len);
      break;
    case u_char:
	  pretty_print_bytes( (unsigned char*) data, len);
	  break;
    case u_integer:
      pretty_print_bytes( (unsigned int*) data, len/sizeof(unsigned int));
      break;
    case integer:
      pretty_print_bytes((int*) data, len/sizeof(int));
      break;
    case floating:
      pretty_print_bytes( (float*) data, len/sizeof(float));
      break;

  }
}
void rf_msg::pretty_print()
{
  pretty_print_header();
  
  if( is_valid() )
  {
    Serial.print(h_separator);
    Serial.print(h_name);
    Serial.print(h_separator);
    Serial.print( (int) _receiver_id );
    Serial.print(h_separator);
    Serial.print( (int) _sender_id );
    Serial.print(h_separator);
    
    
    switch ( _cmd_type )
    {
      case GET:
	Serial.print("GET");
	Serial.print(h_separator);
	// we weten niet zeker of var_name 0 terminated is, kunnen niet gwn Serial.print() doen:
	pretty_print_bytes( _var_name, _var_name_len);
	break;
      case SET:
	Serial.print("SET");
	Serial.print(h_separator);
	// we weten niet zeker of var_name 0 terminated is:
	pretty_print_bytes(_var_name, _var_name_len);
	Serial.print(h_separator);
	pretty_print_data(_data_array,_data_len, _type);
	break;
      case RESPONSE:
	Serial.print("RESPONSE");
	Serial.print(h_separator);
	pretty_print_bytes(_data_array, _data_len);
	
	break;
      case CONFIRM:
	Serial.print("CONFIRM");
    }
      
    // afsluiten: print line
    Serial.println(h_separator);
  }
  else
      Serial.println("Vorm onjuist");
}

// 
bool rf_msg::send(rf_msg *p_response_msg)
{
	if (is_valid())
	{
	  // Stuur door en wacht totdat bericht helemaal weg is
	  uint8_t message_len = print_len();
	  byte message[MAX_MESSAGE_LEN];
	  print_to(message);
	  
	  #ifdef RAW_PRINT
		 Serial.print("RAW bytes sent: ");
		 for(int i = 0; i < message_len; i++)
		 {
			 Serial.print( (char) message[i]);
		 }
		 Serial.println("");
	  #endif
	  
	  vw_send(message, message_len);
	  vw_wait_tx();
	  if( is_of_cmd_type(SET) && _wait_on_response)
	  {
		// nu wachten op confirmatie
		uint8_t buflen = VW_MAX_MESSAGE_LEN;
		for(int i = 0; i < ATTEMPT_NB; i++)
		{
			if( vw_wait_rx_max(ATTEMPT_WAIT)) //blokkeert tot er een bericht is ontvangen of ATTEMPT_WAIT is overschreden
			{
				vw_get_message(msg_buf, &buflen);
				rf_msg msg(msg_buf, buflen);
				if( msg.is_of_cmd_type(CONFIRM) )
					return true;
			}
			else
			{
				// stuur opnieuw door, mss is het bericht verloren
				vw_send(message, message_len);
				vw_wait_tx();
			}
		 }
		 return false;
	   }
	   if( is_of_cmd_type(GET))
	   {
			// we wachten op een RESPONSE en indien we timeouten vesturen we een nieuwe request
			uint8_t buflen = VW_MAX_MESSAGE_LEN;
			for(int i = 0; i < ATTEMPT_NB; i++)
			{
				if( vw_wait_rx_max(ATTEMPT_WAIT)) //blokkeert tot er een bericht is ontvangen of ATTEMPT_WAIT is overschreden
				{
					vw_get_message(msg_buf, &buflen);
					rf_msg msg(msg_buf, buflen);
					if( msg.is_of_cmd_type(RESPONSE) )
					{
						// indien er een p_response_msg meegegeven is (niet NULL), stop daar de RESPONSE in
						if (p_response_msg)  
						{
							*p_response_msg = msg;
						}
						return true;
					}
				}
				else
				{
					// opnieuw waarde opvragen
					vw_send(message, message_len);
					vw_wait_tx();
				}
			}
			return false;
	   }
	  return true;
	}
	return false;
}
bool rf_msg::is_valid()
 {
 	return _is_valid;
 }

bool rf_msg::is_of_type(rf_data_type type)
{
 	return is_valid() && type == _type;
}

bool rf_msg::is_of_cmd_type(command_type type)
{
	return is_valid() && type == _cmd_type;
}

rf_confirm::rf_confirm(uint8_t &target_id)
{
	_receiver_id 	= target_id;
	_sender_id 		= h_id;
	_cmd_type 		= CONFIRM;
	
	_is_valid 		= true;
}

rf_response::rf_response(byte* response_data, int data_len, uint8_t &target_id )
{
	_cmd_type	= RESPONSE;	
	_data_array	= new byte[data_len];
	memcpy(_data_array, response_data, data_len);
	_data_len	= data_len;
	_type		= nul;
	_receiver_id 	= target_id;
	_sender_id 	= h_id;
	_is_valid	= true;
}


// dit moet regelmatig opgeroepen worden, anders gaan oude pakketjes verloren?? Hoe vaak?
// dit kan nooit een confirm krijgen omdat dit gericht aan dit toestel is alleen als er in get en set op gewacht wordt!
rf_msg rf_receive(bool dump_for_other)
{
	 // VirtualWire neemt dit getal als max_waarde en vermindert dit
	 // als je dit bv op nul zet zal je nooit meer dan nul bytes ontvangen!
	 uint8_t buflen = VW_MAX_MESSAGE_LEN;
	
	 if( vw_get_message(msg_buf, &buflen))
	 {
		#ifdef RAW_PRINT
			Serial.print("Raw data recieved, "); Serial.print( buflen ); Serial.print(" bytes:");
			for(int i = 0; i < buflen; i++)
			{
				Serial.print((char) msg_buf[i]);
			}
			Serial.println(" End Raw");
		#endif
		
	 	//Er is een bericht ontvangen, controleer de vorm
	 	rf_msg msg(msg_buf, buflen, dump_for_other);
	 	if (msg.is_valid())
	 	{
	 		if( msg.is_of_cmd_type(SET) && msg._wait_on_response)
	 		{
	 			// Confirmatie bericht versturen
	 			rf_confirm confirm_msg(msg._sender_id);

	 			confirm_msg.print_to(msg_buf);
	 			vw_send(msg_buf, confirm_msg.print_len());

	 			// wachten op versturen
	 			vw_wait_tx();
	 		}
	 	}
	 	else
	 		return rf_msg(); // lege terug sturen => _is_valid = false;
		
	 	return msg;
	 }
	 return rf_msg(); // lege terug sturen => _is_valid = false;
}


rf_msg rf_get(const char* var_name, char target_id, char var_name_len)
{

	//maak bericht
	rf_msg msg(var_name, var_name_len, target_id);

	//zend bericht
	uint8_t send_len = msg.print_len();
	byte buf_to_send[MAX_MESSAGE_LEN];
	msg.print_to(buf_to_send);

	vw_send(buf_to_send,send_len);
	vw_wait_tx();

	//wacht op antwoord
	uint8_t buflen = VW_MAX_MESSAGE_LEN; // maximale lengte, vw_get_message() vermindert dit (maar zal nooit langer dan dit returnen)
	for( int i = 0; i < ATTEMPT_NB; i++)
	{
		if (vw_wait_rx_max(ATTEMPT_WAIT)) //blokeert tot een bericht is ontvangen of timeout is overschreden
		{
			if (vw_get_message(msg_buf, &buflen))
			{
				rf_msg msg(msg_buf, buflen);
				if (msg.is_of_cmd_type(RESPONSE))
					return msg;
			}
		}
	}
	return rf_msg(); // een ongeldige message (rf_get().is_valid() == false)
}

// diagnotische functies
#ifdef DEBUG
int rf_test_suit()
{
	int error = 0;
	// test de functies voor het printen van bytest
	// we beginnen met h_name
	pretty_print_bytes(h_name, (uint8_t) sizeof(h_name));
	Serial.println("");
	rf_msg a("test",5);
	rf_msg b("test",(byte*) "hey", 3,  text, 5, true);
	if(a.is_valid())
		a.pretty_print();
	else
		error+=1;
		
	if(b.is_valid())
		b.pretty_print();
	else
		error+=1;
	uint8_t target = 5;
	rf_confirm c(target);
	rf_response d( (byte*) "hey", 3, target);
	if(c.is_valid())
		c.pretty_print();
	else
		error+=1;
		
	if(d.is_valid())
		d.pretty_print();
	else
		error+=1;
	
	return error;
}
#endif