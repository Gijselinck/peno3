// Deze klasse houd een paar van een pointer en een lengte bij
// om eenvoudig naar een subarray in een andere array te kunnen verwijzen
template<typename T>
class pair
{
public:
	const T* p;
	int len;

	pair()
	{
		len = 0;
		p = 0;
	}
	pair(const T* address, int length)
	{
		len = length;
		p = address;
	}
};
