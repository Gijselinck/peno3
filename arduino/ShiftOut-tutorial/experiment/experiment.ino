//**************************************************************//
//  Name    : shiftOutCode, experiment                                
//  Author  : Tom Gijselinck <tomgijselinck@gmail.com>
//  Date    : 15 okt 2014   
//  Modified: 15 okt 2014                                 
//  Version : 2.0                                             
//  Notes   : Code for using a 74HC595 Shift Register
//****************************************************************

#include <math.h>       /* pow */

//Pin connected to ST_CP of 74HC595
int latchPin = 11; //Green
//Pin connected to SH_CP of 74HC595
int clockPin = 12; //Yellow
////Pin connected to DS of 74HC595
int dataPin = 10; //Blue


void setup() {
  //set pins to output so you can control the shift register
  pinMode(latchPin, OUTPUT);
  pinMode(clockPin, OUTPUT);
  pinMode(dataPin, OUTPUT);
  Serial.begin(9600);
}

void loop() {
  int x = pow(2,2);
  Serial.println(x);
  for (int i = 0; i < 5; i++) {
    sendData(231); //11100111 pattern
    delay(250);
    sendData(85); //01010101 pattern
    delay(250);
  }
  sendData(0);
  delay(1000);
  sendData(1);
  delay(1000);
  int data;
  for (int level = 2; level < 9; level++) {
    data = pow(2,level);
    Serial.println(level);
    Serial.println(data);
    sendData(data);
    delay(1000);
  }
  delay(2000);
  sendData(255);
  delay(1000);
  sendData(127);
  delay(1000);
  sendData(63);
  delay(5000);
}

void sendData(int data) {
  Serial.println(data);
  digitalWrite(latchPin, LOW);
  shiftOut(dataPin, clockPin, MSBFIRST, data); //11100111 pattern
  digitalWrite(latchPin, HIGH);
  delay(1);
}
  
