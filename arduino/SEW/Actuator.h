#include "Arduino.h"
#include "ShiftRegister.h"

// Vanaf deze waarde worden de pinnen omgezet naar het shiftregister
const byte SHIFT_REGISTER_OFFSET = 14;
// aantal pinnnen shift register
const byte SHIFT_RESTSTER_OUTPUT_PINS = 8;

/**
 * A class of actuators involving a status and an arduino pin.
 *
 * @version 1.1
 * @author	Tom Gijselinck <tomgijselinck@gmail.com>, Maxime Feyerick
 */
class Actuator
{
  public:

  	/**
 	   * Initialize this new actuator with given pin.
 	   *
 	   * @param	pin
 	   *		The arduino pin for this new actuator.
 	   * @param controller
 	   *		The adress of the controller for this new actuator (use &).
 	   */
  	Actuator(uint8_t pin, ShiftRegisterController *controller);

  	/**
	   * Return the status of this actuator. Returns 0 (false) or 1 (true)
	   */
	virtual int getStatus();

    /**
  	 * Set the status of this actuator to the given status.
  	 *
  	 * @param	status
  	 * 			The new status for this actuator. Nonzero is true/on , zero is false/off
  	 */
  	virtual void setStatus(int status);

	  /**
	   * Return the arduino pin of this actuator.
	   */
  	int getPin();

  protected:
	/**
  	 * Variable referencing the status of this actuator.
  	 */
  	bool _status;
  private:

  	/**
  	 * Variable referencing the pin number of the arduino board where
  	 * this actuator is connected to.
  	 */
  	uint8_t _pin;

    /**
     * Variable (pointer) referencing the controller of this actuator.
     */
    ShiftRegisterController * _controller;
};

/**
 * A class of Scalable actuators involving a number as status and an arduino pin.
 *
 * @version 1.0
 * @author	Maxime Feyerick
 */
class ScalableActuator : public Actuator
{
private:
	// ScalableActuator heeft een _value
	int _value; 
public:
	// Constructor
	ScalableActuator(uint8_t pin);
	// Get the current status of the scalable actuator
	int getStatus();
	
	// Set the value of the scalable actuator to 'value'
	void setStatus(int value);
};

/**
 * A class of animations involving a lot of things.
 * (simplified: one Animation controls one Actuator)
 *
 * @version 1.1 
 * @author	Robbert Camps <me@robbertc5.com>, Maxime Feyerick
 */
class Animation
{
  public:

  	/**
 	 * Initialize this new animation with given actuators and animation states.
 	 *
 	 * @param	animatedActuator
 	 *			pointer to the actuator that has to be animated.
	 * @param	animationstates
	 *			The amount of animations states/ steps in the animation
	 *			that the actuator should have.
 	 */
  	Animation(Actuator* animatedActuator, int numberOfAnimationStates, int* animationStateList = 0);
	
	// Copy Constructor
	Animation( const Animation &Animation_to_copy);
	// Destructor
	~Animation();
	// assignment overload
	Animation& operator= (const Animation &Animation_to_copy);

  	/**
	 * Add a state to the animation
	 *
	 * @param	stateNumber
	 * 			The number of the state in the animation.
	 * @param	state_val
	 * 			The value the actuator should be set on in that state.
	 */
  	void modifyState(int state_val, int stateNumber);

	 /**
	 * Set the state after wich the animation has to stop en restart with
	 * the StartState
	 *
	 * @param	state
	 * 			The state after wich the animation restarts.
	 */
  	void setStopState(int state);

  	/**
	 * Set the state wich the animation has to restart to.
	 *
	 * @param	state
	 * 			The state after wich the animation restarts.
	 */
  	void setStartState(int state);
	
	/**
	 * Update all the actuators to the next state.
	 */
  	void nextState();
	
	/**
	 * Start the animation.
	 */
  	void start();
	
	/**
	 * Stop the animation.
	 */
  	void stop();

  	/**
	 * Set the state.
	 *
	 * @param	state
	 * 			The index of the state that needs to be actualized.
	 */
  	void setActuatortoState(int state);
	
	/**
	 * Sets the internal list of inamtions by taking a copy.
	 * Copys the number of animations the object has been constructed with.
	 *
	 * @param	state_lst
	 * 			Pointer to where the array contianing the animation list is stored
	 */
	void setStateList(int* state_lst);

  private:
	void setActuator();
	
	// copies al the content from Animation_to_copy to this one
	void copyAnimationFrom(const Animation &Animation_to_copy);
  	/**
  	 * pointer to the array where the animation is stored
  	 */
  	int* _animationStates;

  	/*
  	 * Variable referencing the number of animationStates.
  	 */
  	int _numberOfAnimationStates;
	
	/**
  	 * Variable referencing the status to wich the animation will restart.
  	 */
  	int _startState;

  	/**
  	 * Variable referencing the status after wich the animation will restart.
  	 */
  	int _stopState;
	
  	/**
  	 * Variable referencing the current status of this animation.
  	 */
  	int _currentState;

  	/**
  	 * Variable referencing whether the animation is running or not.
  	 */
  	bool _running;


  	/**
  	 * Variable referencing the animated actuator.
  	 */
  	Actuator *_actuator;

};


/**
 * A class of sensors involving an arduino pin.
 *
 * @version 1.0
 * @author	Tom Gijselinck <tomgijselinck@gmail.com>
 */
class Sensor
{
  public:
	/**
 	 * Initialize this new sensor with given pin.
 	 *
 	 * @param	pin
 	 *		The arduino pin for this new sensor.
 	 * @param controller
 	 *		The adress of the controller for this new sensor (use &).
 	 */
  	Sensor(int pin);

  	/**
  	 * Return the arduino pin of this sensor.
  	 */
  	int getPin();

  	/**
  	 * Return the reading of this sensors.
  	 */
  	int getReading();


  private:
  	/**
  	 * Variable referencing the arduino pin of this sensor.
  	 */
  	int _pin;

};