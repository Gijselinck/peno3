#include "Arduino.h" //access to standard Arduino functions
#include "Actuator.h" //access to definitions in header file

/**
 * A class of actuators involving a status and an arduino pin.
 *
 * @version 1.1
 * @author	Tom Gijselinck, Maxime Feyerick
 */

Actuator::Actuator(uint8_t pin, ShiftRegisterController *controller) {
	_controller = controller;
	pinMode(pin, OUTPUT);
	_pin = pin; //immutable
	//setStatus(false);
}
ScalableActuator::ScalableActuator(uint8_t pin) : Actuator(pin, NULL) {
	setStatus(0);
}
int Actuator::getStatus() {
	return _status;
}

int ScalableActuator::getStatus() {
	return _value;
}

void Actuator::setStatus(int status) {
	if (getPin() >= SHIFT_REGISTER_OFFSET) {
		// controle of dat er geen te hoge waarde wrodt gegeven en ofdat shiftregsiter wel bestaat
		if( getPin() < (SHIFT_REGISTER_OFFSET + SHIFT_RESTSTER_OUTPUT_PINS) && _controller != NULL)
			_controller->writeToShiftRegister(getPin()- SHIFT_REGISTER_OFFSET, status);
	} else {
		//not on shift register
		if (status ) {
			//Serial.print("High on pin "); Serial.println(getPin());
			digitalWrite(getPin(), HIGH);
		} else {
			//Serial.print("Low on pin "); Serial.println(getPin());
			digitalWrite(getPin(), LOW);
		}
	}
	_status = status;

}

void ScalableActuator::setStatus(int value) {
	_value = value;
	_status = (bool) value;
	
	// Assuming not on shift register (PWM not compatible)
	analogWrite(getPin(), value);
	//Serial.print("AnalogWrite on pin "); Serial.print(getPin());Serial.print(" with value ");Serial.println(value);
}

int Actuator::getPin() {
	return _pin;
}



/**
 * A class of animations involving a lot of things.
 * (simplified: one Animation controls one Actuator)
 *
 * @version 1.1 
 * @author	Robbert Camps <me@robbertc5.com>, Maxime Feyerick
 */

Animation::Animation(Actuator* animatedActuator, int numberOfAnimationStates, int* animationStateList) {
	_animationStates = new int[numberOfAnimationStates];
	_numberOfAnimationStates = numberOfAnimationStates;
	if (animationStateList)
	{
		memcpy(_animationStates, animationStateList, numberOfAnimationStates*sizeof(_animationStates[0]));
	}
	else
	{
		for(int i = 0; i<_numberOfAnimationStates; i++)
		{
			_animationStates[i] = 0; 
		}
	}
	_currentState	= 0;
	_stopState		= _numberOfAnimationStates-1;
	_startState		= 0;
	_running		= false;
	_actuator		= animatedActuator;
	setActuator();
}
Animation::Animation( const Animation &Animation_to_copy)
{
	copyAnimationFrom(Animation_to_copy);	
}
Animation& Animation::operator= (const Animation &Animation_to_copy)
{
	// verwijder eerst oude toegewezen geheugen
	delete[] _animationStates;
	// kopieer nu, dit wijst nieuw dynamisch geheugen toe
	// (de grootte kan bv anders zijn)
	copyAnimationFrom(Animation_to_copy);
	
	return *this;
}
Animation::~Animation()
{
	delete[] _animationStates;
}

// deze functie delete[] niet het dynamisch geheugen dat eerde was toegevezen
// op het adres _animationStates 
void Animation::copyAnimationFrom(const Animation &Animation_to_copy)
{
	_numberOfAnimationStates =  Animation_to_copy._numberOfAnimationStates;
	_animationStates = new int[_numberOfAnimationStates];
	memcpy(_animationStates, Animation_to_copy._animationStates, _numberOfAnimationStates*sizeof(_animationStates[0]));
	_currentState	= Animation_to_copy._currentState;
	_startState		= Animation_to_copy._startState;
	_stopState		= Animation_to_copy._stopState;
	_running		= Animation_to_copy._running;
	_actuator		= Animation_to_copy._actuator;
}

void Animation::modifyState(int state_val, int stateNumber) {
	if(stateNumber < _numberOfAnimationStates)
	{
		_animationStates[state_val] = state_val;
	}
}

void Animation::setStartState(int state) {
  	_startState = state;
}


void Animation::setStopState(int state) {
  	_stopState = state;
}

void Animation::nextState() {
	if(_running==true){
		_currentState++;
		if(_currentState > _stopState){
			_currentState = _startState;
		}
		setActuator();
	}
}

void Animation::setActuator() {
	{
		_actuator->setStatus(_animationStates[_currentState]);
	}
}

void Animation::start() {
	_running = true;
	setActuator();
}

void Animation::stop(){
	_running = false;
}

void Animation::setActuatortoState(int state){
	_currentState = state;
	setActuator();
}
void Animation::setStateList(int* state_lst){
	memcpy(_animationStates, state_lst, _numberOfAnimationStates*sizeof(_animationStates[0]));
}

/**
 * A class of sensors involving an arduino pin.
 *
 * @version 1.0
 * @author	Tom Gijselinck <tomgijselinck@gmail.com>
 */
Sensor::Sensor(int pin) {
	pinMode(pin, INPUT);
	_pin = pin;
}

int Sensor::getPin() {
	return _pin;
}

int Sensor::getReading() {
	return analogRead(_pin);
}