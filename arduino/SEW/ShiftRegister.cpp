#include "Arduino.h" //access to standard Arduino functions
#include "ShiftRegister.h" //access to definitions in header file


/**
 * Easy management of a shift register. Setting pins above 13 
 * (the maximum of the Arduino) will be interpreted as extended 'virtual' pins 
 * provided by the shift register.
 *
 * @version 1.1
 * @author	Tom Gijselinck <tomgijselinck@gmail.com>, Maxime Feyerick
 */


ShiftRegisterController::ShiftRegisterController(unsigned int dataPin, unsigned int latchPin, unsigned int clockPin):
	_dataPin(dataPin), _latchPin(latchPin), _clockPin(clockPin) {
 
 	for (int i = 0; i < 8; i++){
          _shiftRegister[i] = 0;
   	}
 	pinMode(_latchPin, OUTPUT);
 	pinMode(_clockPin, OUTPUT);
 	pinMode(_dataPin, OUTPUT);
 }


bool* ShiftRegisterController::getShiftRegister() {
 	return _shiftRegister;
 }

bool ShiftRegisterController::getValueAtRegisterPin(int pin) {
	return _shiftRegister[pin];
}

void ShiftRegisterController::writeToShiftRegister(int pin, bool value) {
	_shiftRegister[pin] = value;
	//convert binary to decimal
	int decimal = 0;
	if (_shiftRegister[0] == 1) {decimal += 1;} //2^0
	for (int i = 1; i < 8; i++) {
		int pow2 = 0;
		if (_shiftRegister[i] == 1) {
			pow2 = 1;
			for (int j = 0; j < i; j++) {
				pow2 *= 2;
			}
		}
		decimal += pow2;
	}
	//write register values to register
	digitalWrite(_latchPin, LOW);
	shiftOut(_dataPin, _clockPin, MSBFIRST, decimal);
	digitalWrite(_latchPin, HIGH);
}

void ShiftRegisterController::shiftRegisterSetAllTrue() {
	for (int i = 0; i < 8; i++) {
		_shiftRegister[i] = 1;
	}
	digitalWrite(_latchPin, LOW);
	shiftOut(_dataPin, _clockPin, MSBFIRST, 255);
	digitalWrite(_latchPin, HIGH);
}

void ShiftRegisterController::shiftRegisterSetAllFalse() {
	for (int i = 0; i < 8; i++) {
		_shiftRegister[i] = 0;
	}
	digitalWrite(_latchPin, LOW);
	shiftOut(_dataPin, _clockPin, MSBFIRST, 0);
	digitalWrite(_latchPin, HIGH);
}