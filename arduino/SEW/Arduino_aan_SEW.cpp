/*
 * Arduino_aan_SEW.cpp - P&O3 Team ESAT5A2 - Slimme Energiewoning
 *
 * Created: 23/11/2014 23:36:47
 *  Author: Maxime Feyerick
 *
 */ 

#include <avr/io.h>
#include "Arduino.h"
#include "Actuator.h"
#include "rfwrap.h"
#include "MemoryFree.h"

//id van dit huis
const byte SEW_ID = 1;
// tijd tss animatiestates (in ms)
const long ANIMATION_DELTA = 2000;

//Controller voor het shift register
ShiftRegisterController shiftControl(7,8,9);

//Array van Actuators, positie in deze lijst bepaalt id
Actuator* actuatorLst[] =	{	new Actuator(4,NULL),
								new ScalableActuator(5)
							}; 

////Animatie
int anim1[] = {1, 0, 1, 0, 1, 0};
int anim2[] = {0,50, 100, 150, 200, 250};
Animation animationLst[] =	{	Animation(actuatorLst[0], 6, anim1),
								Animation(actuatorLst[1], 6, anim2) 
							};

//Array van Sensors, positie in deze lijst bepaalt id
Sensor sensorLst[] = { Sensor(5) };

// Arduino.h roept deze functies op 
void setup();
void loop();


void setup() {
	
	//initialiseer rfwrap
	rf_setup(SEW_ID);
	
	Serial.begin(9600);
	
	// Illustratie van Virtuele functies
	//ScalableActuator test(7);
	//Actuator* p_test = &test;
	//p_test->setStatus(5);
	//int b = p_test->getStatus();
	//Serial.print(b);
	
	//Setup van de animatie
	animationLst[0].start();
	animationLst[1].start();
	//.... 
	Serial.println("Setup SEW Done");	
}


// the loop function runs over and over again forever
void loop() {
	static unsigned long last_time = millis();
	static rf_msg msg; // gwn static om te vermijden dat object steeds opgeropen/afgebroken wordt
	// verwerk berichten
	if (msg = rf_receive())
	{
		// het CCP verzend als naam het id in ascii code
		unsigned int msg_id =  *msg._var_name; 
		
		if( msg.is_of_cmd_type(GET) ) // een SET commando: we moeten de waarden van een sensor terugsturen
		{
			Serial.println("Got GET-message");
			if(msg_id < sizeof(sensorLst))
			{
				int response_val = sensorLst[msg_id].getReading();
				rf_response return_msg((byte*) &response_val, sizeof(int), msg._sender_id);
				return_msg.send();
			}
		}
		else if(msg.is_of_cmd_type(SET)) // een SET commando: we moeten waarde van een actuator instellen
		{
			Serial.println("Got SET-message");
			if (msg_id < sizeof(actuatorLst))
			{
				//bij afspraak zend CCP alleen maar int
				int msg_val =  (int) *msg._data_array;
				Serial.println(msg_val);
				//stel de actuator in		
				actuatorLst[msg_id]->setStatus(msg_val);
			}
			
		};
		
	}
	// Animaties
	if (millis() - last_time > ANIMATION_DELTA)
	{
		// Laat de animaties 1 stap lopen
		for(unsigned int i = 0; i < sizeof(actuatorLst)/sizeof(actuatorLst[0]); i++)
		{
			animationLst[i].nextState();
		}
		
		// stelt tijd laatste animatiestap opnieuw in
		last_time = millis();
	}
}

