/**
 * Easy management of a shift register. Setting pins above 13 
 * (the maximum of the Arduino) will be interpreted as extended 'virtual' pins 
 * provided by the shift register.
 *
 * @version 1.1
 * @author	Tom Gijselinck <tomgijselinck@gmail.com>, Maxime Feyerick
 */
class ShiftRegisterController
{
  public:

 	/**
	 * Initialize this new controller
	 *
	 * @param	houseNumber
	 *			The house number for this new controller.
	 */
 	ShiftRegisterController(unsigned int dataPin = 4, unsigned int latchPin = 5, unsigned int clockPin = 6);

	/**
	 * Return the house number of this controller.
	 */
 	int getHouseNumber();

 	/**
	 * Return the current contents of the shift register of this 
	 * controller.
	 */
 	bool* getShiftRegister();

 	bool getValueAtRegisterPin(int pin);

 	/**
	 * Set value of the given register pin of the register of this
	 * controller to the given value.
	 *
	 * @param	pin
	 *			The register pin to set the new value of the register of
	 *			this controller.
	 * @param	value
	 *			The new value to set at the given register pin of the 
	 *			register of this controller.
	 */
 	void writeToShiftRegister(int pin, bool value);

 	/**
	 * Set all register pins of the register of this controller to 1.
	 */
 	void shiftRegisterSetAllTrue();

 	/**
	 * Set all register pins of the register of this controller to 0.
	 */
 	void shiftRegisterSetAllFalse();


  private:

 	/**
 	 * Variable referencing the contents of the shift register of
 	 * this controller.
 	 */
 	bool _shiftRegister[8];
	 
	 /**
 	 * Variable referencing the data pin of this controller.
 	 */
 	int _dataPin;

 	/**
 	 * Variable referencing the latch pin of this controller.
 	 */
 	int _latchPin;

 	/**
 	 * Variable referencing the clock pin of this controller.
 	 */
 	int _clockPin;
 	

};

