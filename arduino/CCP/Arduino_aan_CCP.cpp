/*
 * Arduino_aan_CCP.cpp
 *
 * Created: 23/11/2014 23:36:47
 *  Author: Maxime
 */ 
//#define DIAGNOSTIC_PRINT

#include <avr/io.h>
#include "math.h"
#include "Arduino.h"
#include "rfwrap.h"

enum ccu_ontvang_type	{ccu_Set = 'S', ccu_Get = 'G'};
enum ccu_zend_type		{ccu_Ok = 'O', ccu_Error = 'E'};

const byte RF_led_pin	= 2;
const byte CCU_led_pin	= 3;

void setup();
void loop();
bool tests();
void serialPrintChars(char* str, int len);
bool interpretCcuMsg(const char* ccu_msg, int ccu_msg_len, uint8_t &wijknummer_out, uint8_t &toestel_out, ccu_ontvang_type &ontvang_type, unsigned int &waarde_out);
void sendCcuErrorMsg(int error_code);
// stuur een waarde  als antwoord op een request van de CCU trg
void sendCcuMsg( unsigned int waarde);

	
// TODO:
// - test of de timeout waarden van rfwrap ok zijn

void setup() {
	//Status ledjes
	pinMode(RF_led_pin, OUTPUT);
	pinMode(CCU_led_pin, OUTPUT);
	Serial.begin(9600);
	
	// De CCU heeft code nul
	rf_setup(0); 
	// Stuur bericht aan CCU om te zeggen dat we klaar zijn om te ontvangen
	Serial.print("Setup Done");
}


void loop() {
	// algemeen principe: communicatie gaat uit van CCU, deze vraagt op.
  
	// Wacht tot CCU ons iets heeft doorgestuurd
	while( !Serial.available())
		continue;
	
	// begin ontvangen ccu
	digitalWrite(CCU_led_pin, HIGH);
	
	// Lees eerste byte, deze geeft lengte van ontvangen bericht
	int ccu_msg_len = Serial.read();
	
	// wacht tot rest van bericht is doorgestuurd
	while ( Serial.available() < ccu_msg_len)
		continue;
	
	char *ccu_msg = new char[ccu_msg_len];
	for (int i = 0; i < ccu_msg_len; i++)
	{
		ccu_msg[i] = Serial.read();
	}
	
	
	//einde ontvangen ccu
	digitalWrite(CCU_led_pin, LOW);
	#ifdef DIAGNOSTIC_PRINT
		Serial.println("got ccu msg");
	#endif
	
	// interpreteer het bericht
	uint8_t wijknummer, toestel_id;
	unsigned int waarde;
	ccu_ontvang_type msg_type;
	if (interpretCcuMsg(ccu_msg, ccu_msg_len, wijknummer, toestel_id, msg_type, waarde) )
	{
		#ifdef DIAGNOSTIC_PRINT
		Serial.println(wijknummer);
		Serial.println(toestel_id);
		Serial.println(waarde);
		Serial.println(msg_type);
		#endif
		// Maak bericht om door te zenden naar gepaste huisje
		switch(msg_type)
		{
			case ccu_Get:	// het verwerken van een GET-message komende van de CCU
			{
				rf_msg ccu_get_msg((char*) &toestel_id, wijknummer, 1);
				
				#ifdef DIAGNOSTIC_PRINT
					ccu_get_msg.pretty_print(); 
				#endif
				
				// hier gaan we response stoppen op send
				rf_msg ccu_get_response;
				
				// verstuur het bericht, rfwrap houdt zicht bezich met pogingnen 
				// en return false indien het verzenden niet lukt
				digitalWrite(RF_led_pin, HIGH);
				if (ccu_get_msg.send(&ccu_get_response))
				{
					#ifdef DIAGNOSTIC_PRINT
						ccu_get_response.pretty_print();
					#endif
					// we gaan ervan uit dat alleen maar int worden verstuurd/ontvangen
					sendCcuMsg( (int) *ccu_get_response._data_array);
				}
				else
					// SEW-response timeout, geen correct antwoord ontvangen van sew-arduino binnen tijdsbestek
					sendCcuErrorMsg(2);
				digitalWrite(RF_led_pin, LOW);
				break;
			}
			case ccu_Set: // het verwerken van een SET-message komende van de CCU
			{
				// sorry dat deze constructor een beetje onoverzichtelijk blijkt...
				rf_msg ccu_set_msg( (char*) &toestel_id /*var_name*/ , (byte*) &waarde, sizeof(int),  u_integer,
									wijknummer /*target_id*/, true /*wait_on_response*/, 1 /*var_name_len*/);
				#ifdef DIAGNOSTIC_PRINT
					ccu_set_msg.pretty_print();
					Serial.println( *( (int*) ccu_set_msg._data_array) );
				#endif
				digitalWrite(RF_led_pin, HIGH);
				if ( !ccu_set_msg.send())
					// SEW-response timeout, geen correct antwoord ontvangen van sew-arduino binnen tijdsbestek
					sendCcuErrorMsg(2);	
				else
					sendCcuMsg(0); // een okay met waarde 0 als het verzenden gelukt is
				digitalWrite(RF_led_pin, LOW);	
				break;
			}
			default:
				// we herkennnen het bericht van het CCU niet => stuur "invalid CCU msg" error naar CCU
				sendCcuErrorMsg(5);
		}
		
	} 
	// geef dynamisch geheugen vrij
	delete[] ccu_msg;
}

/*
Interpreteert een cstring afkomstig van de CCU, returnt true indien de vorm correct is, anders false.
Via de referenced arguments (eindigend met _out) worden de gegevens in het bericht doorgegeven.

Parameters:
ccu_msg			: pointer naar de char[] die het het bericht bevat
ccu_msg_len		: lengte van de array waarnaar ccu_msg verwijst
wijknr_out		: hierin wordt het wijknummer van het bericht gestockeerd
toestel_out		: hierin wordt het toestel_id uit het bericht opgeslagen
waarde_out		: hierin wordt de waarde uit het bericht opgeslagen, hier wordt
				  niets aan gedaan als het een SET message is
*/
bool interpretCcuMsg(const char* ccu_msg, int ccu_msg_len, uint8_t &wijknr_out, 
	uint8_t &toestel_out, ccu_ontvang_type &ontvang_type, unsigned int &waarde_out)
{
	wijknr_out  = ccu_msg[0];
	toestel_out = ccu_msg[1];
	ontvang_type= (ccu_ontvang_type) ccu_msg[2];
	
	waarde_out = 0;
	if (ontvang_type == ccu_Set)
	{
		for (int i = 3; i < ccu_msg_len; i++)
		{
			if( !isdigit(ccu_msg[i]) )
				return false;
			int decimaal = 1;
			for (int j= 0; j < ccu_msg_len - 1 - i; j++)
			{
				decimaal *=10 ;
				
			}
			waarde_out +=  ( (int) ( ccu_msg[i] - '0') )*decimaal;
		}
	}
	return true;
}

void sendCcuErrorMsg(int error_code)
{
	int msg_len = 2; // bv "E2" voor fout met code 2 
	char ccu_msg[] = {(char) msg_len, ccu_Error, '0' + error_code};
	
	// Stuur het door en wacht to het volledig doorgezonden is	
	serialPrintChars(ccu_msg, sizeof(ccu_msg));
	Serial.flush();
}

void sendCcuMsg(unsigned int waarde)
{
	/* 
	int exponent_base_10 = 0;
	int temp_waarde = waarde;
	while ( (temp_waarde /= 10) < 10 )
	{
		exponent_base_10++;
	}
	int msg_len = 2 + exponent_base_10 + 1;	// 2 vaste bytes voor lengte en bericht type, 
											// exponent van de wetenshcappelijke schrijfwijze base met base 10
											// + 1 cijfers nodig om waarde als string mee voor te stellen.
	char* ccu_msg = new char[msg_len];
	
	//vul de msg in
	ccu_msg[0] = (char) (msg_len - 1); // de eerste byte geeft aan hoeveel tekens er nog volgen
	ccu_msg[
	*/
	
	// Construeer bericht, x is placeholder voor grootte van alles na eerste byte;
	String msg = String('x') + String( (char) ccu_Ok) + String(waarde);
	int ccu_msg_len = msg.length(); // lengte zonder '/0'
	msg[0] = ccu_msg_len - 1;
	
	// Stuur bericht door en wacht tot volledig doorgezonden.
	Serial.print(msg);
	Serial.flush();
}

bool tests()
{
	sendCcuErrorMsg(4);
	sendCcuMsg(45);
	char test_msg_ccu[] = {(char) 1, (char) 2,'S', '1', '2', '3'};
	uint8_t wijknummer;
	uint8_t toestel;
	unsigned int waarde;
	ccu_ontvang_type type;
	interpretCcuMsg(test_msg_ccu, sizeof(test_msg_ccu), wijknummer, toestel, type, waarde);
	if (waarde != 123)
		return false;
	return true;
}

void serialPrintChars(char* str, int len)
{
	for(int i = 0; i < len; i++)
	{
		Serial.print(str[i]);
	}
}