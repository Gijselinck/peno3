De arduino heeft 14 output pinnen. In veel gevallen kan dit te weinig zijn. Het
is mogelijk om het aantal output pinnen van een arduino uit te breiden.
Hiervoor kan gebruik worden gemaakt van shift registers.

Wat is een shift register?
---------------------------
Een shift register is een set van n flip-flops die worden gebruikt om n bits aan
informatie op te slagen waarbij de inhoud van deze n flip-flops kan worden 
verschoven. De term register wordt gebruikt om te verwijzen naar n-bit 
structuren opgebouwd uit flip-flops. Een register gebruikt een 
gemeenschappelijke klok voor elke flip-flop. De output van elke flip-flop is
verbonden aan de data ingang van de volgende flip-flop in de keten. Dit 
resulteert dus in een circuit dat de aanwezige data verschuift met één positie
door de data aan de ingang in te schuiven en de laatse bit in de rij weg te 
schuiven.

Hoe data schrijven naar een shift register?
--------------------------------------------
De latchPin is verbonden met de ingang op de register RCLK (register clock). Als
deze ingang op HIGH staat zullen de outputs de nieuwe shift registerwaarden 
geven. Zolang de latchPin een actief laag signaal ontvangt kunnen de register-
waarden dus worden aangepast zonder dat de outputs worden gewijzigd. Op deze
manier lijkt alles in één stap te gebeuren.
```c++
// define latchPin, clockPin and dataPin
digitalWrite(latchPin, LOW);
// shift out data bit per bit
shiftOut(dataPin, clockPin, MSBFIRST, data);
digitalWrite(latchPin, HIGH);
```

Registers cascaderen
---------------------
Het is mogelijk meerdere registers aan elkaar te koppelen. Voor n registers
moeten er n bytes worden verstuurd, waarbij de eerste byte op het laatste 
register komt te staan en de laatste byte op de eerste register. 
[zie Example 2 van de arduino ShiftOut tutorial voor meer info]

Bronnen
----------
+ http://www.arduino.cc/en/Tutorial/ShiftOut
+ http://arduino.stackexchange.com/questions/117/is-there-a-way-to-have-more-than-14-output-pins-on-arduino
+ http://www.thekanes.org/2010/06/09/expanding-the-arduino-cheap-ways-to-add-output-pins-and-power/
