taken te doen voor:
====================
week 5
-------
### maandag 20 okt ###
+ verslag

#### verslag - taakverdeling ####
+ Inleiding --> Pauline
+ Opstelling --> Steven
+ Slimme energiewoning --> Pauline (shift registers Tom)
+ CCU --> Maxime
+ UI --> Tom
+ Demonstratie --> Robbert
+ Besluit --> Robbert
+ **Opmerkingen:** 
  + bijlagen: tabel prijzen etc. (Pauline),
  + referenties: hou bronnen bij!

### donderdag 23 okt ###
+ verslag nakijken


week 4
-------
### maandag 13 okt ###
+ welke taken er te doen zijn (zie ook map `taakstructuur/`)
+ ev. aan te vullen ...

### donderdag 16 okt ###
+ verslag


week 3
--------
### maandag ###
+ git leren
+ presentatie maken (kladversie)
+ verder info zoeke
+ django ontdekken (tutorial)

### donderdag ###
+ presentatie

#### presentatie - taken ####
+ vragen halen uit presentatie --> pauline
+ concept.md (praktische modellering huis) --> tom
+ doelstellingen.md --> maxime
+ besluit --> robbert
+ inleiding (o.a. probleemstelling) --> steven
+ samenvoegen --> steven
