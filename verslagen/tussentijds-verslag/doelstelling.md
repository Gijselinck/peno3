Doelstelling
==============
Probleemstelling:
----------------
Tot voor kort kon het aanbod van energie goed worden afgesteld op de vraag van
de consument. Dit is veel moeilijker geworden door twee zaken. Ten eerste de 
recente introductie van hernieuwbare energiebronnen. Deze worden namelijk
gestuurd door de weersomstandigheden en niet door de energievraag. Ten tweede de
sluiting van verouderde energiecentrales die resulteert in een daling van de
capaciteit van onze klassieke productie.
De productie van energie is dus onvoorspelbaar en soms ontstaan 
energieoverschotten en energietekorten. Om potentiële energietekorten op te
vangen worden aanvullend piek-energiecentrales gebruikt. Daarenboven wordt de 
energierekening duurder omdat het gebruik niet wordt afgestemd op het aanbod.
Dit alles resulteert in een inefficiënt gebruik van energie en een duurdere
energierekening voor de consument.

Slimme energiewoningen zouden moeten tegemoetkomen aan dit probleem. Met behulp
van Smart Metering kan energieverbruik worden verplaatst naar periodes met 
lagere energieprijs. 
 


Eisen individuele componenten
-----------------------------
### arduino (huis) ###
	- ontvangen van informatie van sensors
	- sturen van toestellen (actuators)
	- simpele automatiseringen
	- communicatie met CCU
	- foutmeldingen (debug; e.g. sensor malfunction; vr technici)
	- ...

### arduino (CCU) ###
	- communicatie met huis en server (transmitter)

### server/CCU ###
	- communicatie
	- beslissingsstructuur
	- voorspellen van energietarief volgende dag
	- database
	- input market information
	- web interface
	- gebruikersprofielen
	- export electricity bill
	- real-time data visualisation


Uitwerking
----------------
