welke toestellen?
----------------
koelkast(§), verwarming(§), wasmachine, verlichting, auto(§) (opladen), entertainment,
zwembad, boiler, koken, vaatwasser, droogkast, toestellen
energiebronnen: zon (elec + wamte vr water), wind, ...
energieopslag: (?)


organisatie opslag/winning energie?
----------------
centraal (residentie) => gezamenlijk zonnepannelenpark


focus?
----------------
web application, representatie appliances, ...?


sensors:
----------------
temperatuur (buiten + binnen), licht, aanwezigheid (model: bepaalde plaatsen
in huis plaatsbaar waar dan een sensor is, e.g. magneet) 


actuators
----------------
elektromotor, LED, buzzer, relais, weerstand, 


voorstelling
----------------
+ algemeen: zwarte doek voor dag/nacht beter voor te stellen, digitale klok
  voor tijdsimulatie, dag/nacht-lichtsensor, tijdsimulatie met mogelijkheid tot
  interactie
+ auto: led voor opladen/niet opladen
+ tv: doorzichtige film met variabele lichtintensiteit + speaker
+ zonnepaneel
+ boiler: rode led, cilinder
+ wasmachine: ronddraaien (motortje)
+ vaatwasser: draaien + blauw licht
+ windmolen: aan/uit knop


criteria voor energiebeheer
----------------------------
+ energieprijs (real time / voorspelling --> hoe?)
+ voorkeuren
+ lokale productie van RES
+ ...
