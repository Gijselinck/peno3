In deze map werken we aan het tussentijds verslag. Hier kan al het nodige
materiaal verzameld worden (bv. extra info, opmerking, etc.).

./verslag/
--------------
Het verslag. Voor te compileren, zie dat je alle ingeladen packages hebt.
De voorpagina zal voor het indienen worden aangepast.

**OPMERKING:** om hetzelfde resultaat te krijgen bij compilatie moet de
gewijzigde kulemt documentclass gebruikt worden!
