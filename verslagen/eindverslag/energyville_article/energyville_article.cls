

\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{energyville_article}[2014/06/11 EnergyVille artivle class]

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Font
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\RequirePackage{lmodern}
\RequirePackage{fancyhdr}
\RequirePackage{datetime}
\RequirePackage{draftwatermark}

%%% 'sans serif' option
%\DeclareOption{sans}{
%  \renewcommand{\familydefault}{\sfdefault}
%}

%%% 'roman' option
%\DeclareOption{roman}{
%  \renewcommand{\familydefault}{\rmdefault}
%}


%% Article options
\DeclareOption{10pt}{
  \PassOptionsToClass{\CurrentOption}{article}
}
\DeclareOption{11pt}{
  \PassOptionsToClass{\CurrentOption}{article}
}
\DeclareOption{12pt}{
  \PassOptionsToClass{\CurrentOption}{article}
}
\DeclareOption{nodraft}{
    \SetWatermarkText{}
}
\ExecuteOptions{nodraft}

\DeclareOption{timestamp}{
    \settimeformat{hhmmsstime}
    \renewcommand{\dateseparator}{-}
    \yyyymmdddate
    \newcommand{\todayiso}{\the\year \dateseparator \twodigit\month \dateseparator \twodigit\day}
    \chead{\color{evlightgray} DRAFT VERSION [\todayiso\,\currenttime]}
}

\DeclareOption{draft}{
    \ExecuteOptions{timestamp}  
    % WATERMARK
    \SetWatermarkScale{1.4}
    \SetWatermarkText{DRAFT}
    \SetWatermarkColor{evlightgray}
}



%\ExecuteOptions{sans}
\ExecuteOptions{10pt}
\ProcessOptions\relax

\LoadClass[a4paper]{article}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Section headers
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\RequirePackage{titlesec}
\RequirePackage{bookmark}

\titleformat{\section}[block]
  {\raggedright\Large\bfseries\color{white}}
  {}
  {0pt}
  {\colorsection}

\titleformat{name=\section,numberless}[block]
  {\raggedright\Large\bfseries\color{white}}
  {}
  {0pt}
  {\colorsectionnl}

\titlespacing*{\section}{0pt}{*4}{0pt}

\newcommand{\colorsection}[1]{%
  \colorbox{evdarkgreen}{\parbox{\dimexpr\textwidth-3.5\fboxsep}
    {
        \vspace{0.25em}\thesection\hspace{0.75em}#1\vspace{0.25em}}
    }
    \vspace{-0.5em}
}
\newcommand{\colorsectionnl}[1]{%
  \colorbox{evlightgreen}{\parbox{\dimexpr\textwidth-3.5\fboxsep}
    {
        \vspace{0.25em}#1\vspace{0.25em}}
    }
    \vspace{-0.5em}
}

\titleformat{\subsection}%[display]
{\raggedright\large\bfseries\color{evdarkgreen}}
{\thesubsection}{0.75em}{}

\titleformat{\subsubsection}%[display]
{\raggedright\itshape\bfseries\color{evdarkgreen}}
{\thesubsubsection}{0.75em}{}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Geometry
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\RequirePackage{geometry}
\geometry{margin=2.5cm}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Colors
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\RequirePackage[usenames,dvipsnames]{xcolor}
\definecolor{evdarkgreen}{RGB}{4,104,57}
\definecolor{maincolor1}{RGB}{4,104,57}
\definecolor{evlightgreen}{RGB}{141,198,63}
\definecolor{maincolor2}{RGB}{141,198,63}
\definecolor{evlightgray}{RGB}{137,137,137}
\definecolor{evdarkgray}{RGB}{89,89,89}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% New commands
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\def\title#1{\gdef\@title{#1}\gdef\inserttitle{#1}}
\def\@title{\@latex@error{No \noexpand\title given}\@ehc}

\def\date#1{\gdef\@date{#1}\gdef\insertdate{#1}}
\def\@date{\@latex@error{No \noexpand\date given}\@ehc}


\def\author#1{\gdef\@author{#1}\gdef\insertauthor{#1}}
\def\@author{\@latex@error{No \noexpand\author given}\@ehc}
\newcommand{\reporttype}[2][]{\gdef\@reporttype{#2}\gdef\insertreporttype{#2}\gdef\insertshortreporttype{#1}}
\def\@reporttype{\@latex@error{No \noexpand\reporttype given}\@ehc}

\def\subtitle#1{\gdef\insertsubtitle{#1}}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Footer and Header
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\RequirePackage{lastpage}
\setlength{\headheight}{15.2pt}
\pagestyle{fancy}
\renewcommand{\headrulewidth}{0.5pt}
\renewcommand{\footrulewidth}{0.5pt}

%\patchcmd{\@fancyhead}{\rlap}{\color{evdarkgreen}\rlap}{}{}
\patchcmd{\headrule}{\hrule}{\color{evdarkgreen}\hrule}{}{}
%\patchcmd{\@fancyfoot}{\rlap}{\color{evdarkgreen}\rlap}{}{}
\patchcmd{\footrule}{\hrule}{\color{evdarkgreen}\hrule}{}{}

\lhead{\raisebox{-0.3em}{\includegraphics[height=2em,clip=true]{images/houses_short}}\,\,\insertauthor}
\rhead{\nouppercase{\leftmark}\,\,\raisebox{-0.3em}{\includegraphics[height=2em,clip=true]{images/ev_logo_right}}}

\lfoot{\raisebox{-1em}{\includegraphics[height=2em,clip=true]{images/ev_logo_left}}\insertshortreporttype}
%\lfoot{\insertshortreporttype}
\cfoot{\insertdate}
\rfoot{\thepage/\pageref{LastPage}}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Title
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\RequirePackage{graphicx}
\renewcommand\maketitle{
    \begin{titlepage}
        %LOGO
        \phantom{a}
        \vfill
        
        \begin{flushright}
        {\color{evdarkgray}\rule{\textwidth}{0.4pt}}\\[0.4em]
        {\large \bfseries \insertreporttype}\\[2em]
        {\Huge \bfseries \color{evlightgreen}\@title}\\[2em]
        {\huge  \color{evdarkgreen}\insertsubtitle}\\[2em]
        {\Large \bfseries\itshape \color{evdarkgray}\@author}\\[2em]
        
        
        
        % Bottom of the page
        {\large \color{evdarkgray} \@date}
        {\color{evdarkgray}\rule{\textwidth}{0.4pt}}\\[2em]
        
        % Logo
        \includegraphics[height=1.5cm]{./images/kuleuven_logo}\hfill
        \includegraphics[height=1.5cm]{./images/vito_logo}\hfill
        \includegraphics[height=1.5cm]{./images/ev_logo}
        \end{flushright}
    \end{titlepage}
     \cleardoublepage
}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Itemize/Enumerate
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\newcommand{\localtextbulletone}{\textcolor{evlightgreen}{\rule[0.2em]{.4em}{.4em}}}
\renewcommand{\labelitemi}{\localtextbulletone}
\renewcommand{\labelitemii}{\localtextbulletone}
\renewcommand{\labelitemiii}{\localtextbulletone}
\renewcommand{\labelitemiv}{\localtextbulletone}
\renewcommand{\labelenumi}{\color{evlightgreen}\arabic{enumi}.}
\renewcommand{\labelenumii}{\labelenumi\color{evlightgreen}\arabic{enumii}.}
\renewcommand{\labelenumiii}{\labelenumii\color{evlightgreen}\arabic{enumiii}.}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Hyperref
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\RequirePackage{hyperref}
\AtBeginDocument{%
    \hypersetup{
    bookmarks=True,
    bookmarksnumbered=True,
    bookmarksdepth=3,
    hidelinks,
    pdfauthor={\insertauthor},
    pdfsubject={\insertreporttype},
    pdftitle={\inserttitle}
    }
}
\endinput
