<?php
if(!isset($in_index)){header("HTTP/1.0 404 Not Found");  exit();};

if(isset($_GET['toestel'])){
	$id = intval($_GET['toestel']);
	$tabel = "toestel"; // de naam van dit ding in de database
	$titel = "Toestel"; // de uitgeschreven nederlandse tekst
	$dit = "Dit";		// het woord dat voor $titel moet komen
	// $plots = Array(Array('activiteitsgraad','verbruik'),Array('status','doelstatus')); // welke data (kolomnamen) er allemaal geplot moeten worden
	$plots = Array(Array('verbruik',false)); // welke data (kolomnamen) er allemaal geplot moeten worden
}
elseif (isset($_GET['heb'])) {
	$id = intval($_GET['heb']);
	$tabel = "heb";
	$titel = "Hernieuwbare energiebron";
	$dit = "Deze";
	$plots = Array(Array('productie','status'));
}
elseif (isset($_GET['sensor'])) {
	$id = intval($_GET['sensor']);
	$tabel = "sensor";
	$titel = "Sensor";
	$dit = "Deze";
	$plots = Array(Array('waarde','status'));
}
else{
	die("Er valt niets te tekenen.");
}


if(!$ingelogd){
	die("Je bent niet ingelogd. <a href=\"/\">Ga terug naar de homepage.</a>");
}

// check of we dit ding beheren
$res = $db->query("SELECT * FROM `".$tabel."` WHERE `id` = '".intval($id)."' AND `wijknr` = '".intval($sesrij['wijknr'])."';");
if(!$ding = $res->fetch_array(MYSQLI_ASSOC)){
	die($dit." ".strtolower($titel)." bestaat niet of u bent niet de eigenaar ervan.");
}


$tpl_content = "grafiek";
$pagetitle = "Grafiek".((count($plots)>1)?"en":"")." ".strtolower($titel)." ".$ding['naam'];


if($tabel=="toestel"){
	$tabel = $ding['naam'];
	if($tabel=="televisie"){$tabel="tv";} // prachtig ontwerp is dit
	if($tabel=="koelkast"){$tabel="ijskast";} // ik hou van jullie
	if($tabel=="e-voertuig"){$tabel="EV";$plots=Array(Array('SOC',false));} // ok
	$res2 = $db->query("SELECT * FROM `".$tabel."` WHERE `wijknr` = '".intval($sesrij['wijknr'])."' ORDER BY `tijd` ASC;");
	echo $db->error;
}else{
	// get all the data
	$res2 = $db->query("SELECT * FROM `".$tabel."_gesch` WHERE `".$tabel."_id` = '".intval($id)."' ORDER BY `".$tabel."_gesch`.`tijd` ASC;");

}

$data = array();
while($datapunt = $res2->fetch_array(MYSQLI_ASSOC)){
	array_push($data, $datapunt);
}

?>