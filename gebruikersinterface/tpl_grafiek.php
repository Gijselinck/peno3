<?php
if(!isset($in_index)){header("HTTP/1.0 404 Not Found");  exit();};
?><h1 style="display: inline-block;"><?=$pagetitle;?>&nbsp;</h1>(<a style="display: inline-block;" href="/instellingen/">Terug naar de instellingen.</a>)<br>

<div class="center">

<script type="text/javascript">
time = <?=(($klok['tijdsstap'])? $klok['tijdsstap'] : "96");?>;

window.onload = function () {
  getSubData = function(name,time){
    var end = time;
    var length = 1;
    return allData[name].slice(Math.max(0,end-length), end);
  }
  var allData = [];
  var subData = [];
<?php foreach ($plots as $i => $kolommen) {?>
  <?php foreach ($kolommen as $j => $kol){ ?>
  allData['<?=$i;?>_<?=$j;?>'] = [
        <?php foreach ($data as $key => $datapunt) {
          echo "{label: \"".$datapunt['tijd']."\", y:".$datapunt[$kolommen[0]]."},";
        }?> 
        ];
  <?php } ?>
<?php } ?>

for(var i in allData){
  subData[i] = [];
  for(var j = 1; j<time; j++){
    subData[i].push({x:new Date(2000, 01, 01, 0, j*15, 0), y:getSubData(i,j)[0].y});
  }
}
updateCharts = function(time){
  if(time>=96){
    return;
  }
  for(var i in allData){
    subData[i].push({x:new Date(2000, 01, 01, 0, time*15, 0), y:getSubData(i,time)[0].y});
  }
  chart0.render();
  if (typeof chart1 != 'undefined'){
    chart1.render();
  }
  time++;
  setTimeout(function(){updateCharts(time);},<?=$klok['dt'];?>);
}

<?php foreach ($plots as $i => $kolommen) {?>
	var chart<?=$i;?> = new CanvasJS.Chart("chartContainer<?=$i;?>",
    {
      title:{
        text: "<?=(ucwords($kolommen[0]).( ($kolommen[1]!=false)? " en ".ucwords($kolommen[1]) : ""));?>",
      },      
      toolTip: {
        shared: true,
        content: function(e){
          var body ;
          var head ;
          head = "<span style = 'color:DodgerBlue; '><strong>"+ (e.entries[0].dataPoint.label)  + "</strong></span><br/>";

          body = "<span style= 'color:"+e.entries[0].dataSeries.color + "'> " + e.entries[0].dataSeries.name + "</span>: <strong>"+  e.entries[0].dataPoint.y + "</strong> ";
          <?php if($kolommen[1]!=false){ ?>
          body += "<br/> <span style= 'color:"+e.entries[1].dataSeries.color + "'> " + e.entries[1].dataSeries.name + "</span>: <strong>"+  e.entries[1].dataPoint.y + "</strong>";
          <?php } ?>
          return (head.concat(body));
        }
      },   
      axisY:{ 
        title: "<?=ucwords($kolommen[0]);?>",
        includeZero: false,
        suffix : "<?=($kolommen[0]=="verbruik"||$kolommen[0]=="productie")? " W" : "" ;?>",
        lineColor: "#369EAD"        
      },
      <?php if($kolommen[1]!=false){ ?>
      axisY2:{ 
        title: "<?=ucwords($kolommen[1]);?>",
        includeZero: false,
        suffix : "<?=($kolommen[1]=="verbruik"||$kolommen[1]=="productie")? " W" : "" ;?>",
        lineColor: "#C24642"
      },
      <?php } ?>
      axisX: {
        title: "Tijd",
        suffix : "",
        minimum: new Date(2000, 01, 01, 0, 15, 0),
        maximum: new Date(2000, 01, 01, 24, 0, 0),
      },
      data: [
      {        
        type: "<?=($kolommen[0]=="verbruik"||$kolommen[0]=="productie")? "area" : "line" ; /* verbruik en productie krijgt een ingekleurd oppervlakje */ ?>",  
        name: "<?=ucwords($kolommen[0]);?>",
        dataPoints: subData['<?=$i;?>_0']
      }, 
      <?php if($kolommen[1]!=false){ ?>
      {        
        type: "<?=($kolommen[1]=="verbruik"||$kolommen[1]=="productie")? "area" : "line" ; /* verbruik en productie krijgt een ingekleurd oppervlakje */ ?>",  
        <?php if($kolommen[1]!="doelstatus"){ ?> axisYType: "secondary"  , <?php } /* doelstatus heeft dezelfde schaal als status */ ?>
        name: "<?=ucwords($kolommen[1]);?>",
        dataPoints: subData['<?=$i;?>_1']
      } 
      <?php } ?>
      ]
    });

chart<?=$i;?>.render();
<?php } ?>

updateCharts(time);
}
</script>


<?php foreach ($plots as $i => $kolommen) {?>
<div id="chartContainer<?=$i;?>" style="height: 300px; width: 100%;"></div>
<?php } ?>


<script type="text/javascript" src="/js/canvasjs.min.js"></script>

</div>