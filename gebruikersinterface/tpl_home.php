<?php
if(!isset($in_index)){header("HTTP/1.0 404 Not Found");  exit();};
?><h1>Home</h1>
<?php if($ingelogd){
	?>
		<p>Uw totaal verbruik op dit moment: <strong><?=$verbruiknu;?></strong> kW.<br>
		
	<?php
	 /*Uw totale energieproductie op dit moment: <strong><?=$productie;?></strong> kW.<br></p>*/
}
?>
<h2>Stroomprijs<?=(($ingelogd)?" en uw Verbruik":"");?></h2>
<script type="text/javascript">

time = <?=(($klok['tijdsstap'])? $klok['tijdsstap'] : "96");?>;

window.onload = function () {
  getSubData = function(name,time){
    var end = time;
    var length = 1;
    return allData[name].slice(Math.max(0,end-length), end);
  }
  allData = [];
  allData["prijs"] = [
          <?php foreach ($prijs as $key => $prijstick) {
            echo "{label: \"".$prijstick['tijd']."\", y:".$prijstick["euro-kwh"]."},";
          }?>    
        ];
  <?php if($ingelogd){?>
  allData["verbruik"] = [
          <?php foreach ($verbruik as $key => $verbruiktick) {
            echo "{label: \"".$verbruiktick['tijd']."\", y:".$verbruiktick["verbruik"]."},";
          }?>    
        ];
<?php } ?>
  allData["zon"] = [
          <?php foreach ($zon as $key => $zontick) {
            echo "{label: \"".$zontick['tijd']."\", y:".$zontick["load_factor"]."},";
          }?>    
        ];
  allData["wind"] =[
          <?php foreach ($wind as $key => $windtick) {
            echo "{label: \"".$windtick['tijd']."\", y:".$windtick["load_factor"]."},";
          }?>    
        ];
  subData = [];

  for(var i in allData){
    subData[i] = [];
    for(var j = 1; j<time; j++){
      subData[i].push({x:new Date(2000, 01, 01, 0, j*15, 0), y:getSubData(i,j)[0].y});
    }
  }

updateCharts = function(time){
  if(time>=96){
    return;
  }
  for(var i in allData){
    subData[i].push({x:new Date(2000, 01, 01, 0, time*15, 0), y:getSubData(i,time)[0].y});
  }
  chart0.render();
  chart1.render();
  time++;
  setTimeout(function(){updateCharts(time);},<?=$klok['dt'];?>);
}

	var chart0 = new CanvasJS.Chart("chartContainer0",
    {
      title:{
        text: "Stroomprijs<?=(($ingelogd)?" en Verbruik":"");?>",
      },      
      toolTip: {
        shared: true,
        content: function(e){
          var body ;
          var head ;
          head = "<span style = 'color:DodgerBlue; '><strong>"+ (e.entries[0].dataPoint.label)  + "</strong></span><br/>";

          body = "<span style= 'color:"+e.entries[0].dataSeries.color + "'> " + e.entries[0].dataSeries.name + "</span>: <strong>"+  e.entries[0].dataPoint.y + "</strong>";
          <?php if($ingelogd){?>
          body += "<br/> <span style= 'color:"+e.entries[1].dataSeries.color + "'> " + e.entries[1].dataSeries.name + "</span>: <strong>"+  e.entries[1].dataPoint.y + "</strong>";
          <?php }?>
          return (head.concat(body));
        }
      },   
      axisY:{ 
        title: "Prijs",
        includeZero: false,
        suffix : " "+unescape("%u20AC")+"/kWh",
        lineColor: "#369EAD",
        minimum: 20,
        maximum: 60,    
      },
      axisY2:{ 
        title: "Verbruik",
        includeZero: false,
        suffix : " W",
        lineColor: "#C24642",
        minimum: 0,
        maximum: <?=$verbruikmax;?>,    
      },
      axisX: {
        title: "Tijd",
        suffix : "",
        minimum: new Date(2000, 01, 01, 0, 15, 0),
        maximum: new Date(2000, 01, 01, 24, 0, 0),
        valueFormatString: "HH:mm" ,
      },
      data: [
      {        
        type: "stepLine",  
        name: "Prijs",
        markerSize: 1, 
        dataPoints: subData["prijs"]
      },
      <?php if($ingelogd){?>
       {        
        type: "stepLine",  
        axisYType: "secondary",
        name: "Verbruik",
        markerSize: 1,  
        dataPoints: subData["verbruik"]
      },
      <?php } ?>
      ]
    });

chart0.render();

var chart1 = new CanvasJS.Chart("chartContainer1",
    {
      title:{
        text: "Zon en Wind",
      },      
      toolTip: {
        shared: true,
        content: function(e){
          var body ;
          var head ;
          head = "<span style = 'color:DodgerBlue; '><strong>"+ (e.entries[0].dataPoint.label)  + "</strong></span><br/>";

          body = "<span style= 'color:"+e.entries[0].dataSeries.color + "'> " + e.entries[0].dataSeries.name + "</span>: <strong>"+  e.entries[0].dataPoint.y + "</strong>";
          body += "<br/> <span style= 'color:"+e.entries[1].dataSeries.color + "'> " + e.entries[1].dataSeries.name + "</span>: <strong>"+  e.entries[1].dataPoint.y + "</strong>";
          
          return (head.concat(body));
        }
      },   
      axisY:{ 
        title: "Zon",
        includeZero: false,
        suffix : "",
        lineColor: "#369EAD",
        minimum: 0,
        maximum: 300,     
      },
      axisY2:{ 
        title: "Wind",
        includeZero: false,
        suffix : "",
        lineColor: "#C24642",
        minimum: 0,
        maximum: 0.6,
      },
      axisX: {
        title: "Tijd",
        suffix : "",
        minimum: new Date(2000, 01, 01, 0, 15, 0),
        maximum: new Date(2000, 01, 01, 24, 0, 0),
        valueFormatString: "HH:mm" ,
      },
      data: [
      {        
        type: "spline",  
        name: "Zon",
        markerSize: 1, 
        dataPoints: subData["zon"]
      },
       {        
        type: "spline",  
        axisYType: "secondary",
        name: "Wind",
        markerSize: 1, 
        dataPoints: subData["wind"]
      },
      ]
    });

chart1.render();

updateCharts(time);

}
</script>


<div id="chartContainer0" style="height: 300px; width: 100%;"></div>

<h2>Zon en Wind</h2>
<div id="chartContainer1" style="height: 300px; width: 100%;"></div>

<!--
<h2>Zon en Verbruik</h2>
<div id="chartContainer2" style="height: 300px; width: 100%;"></div>-->

<script type="text/javascript" src="/js/canvasjs.min.js"></script>