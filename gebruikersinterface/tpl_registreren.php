<?php
if(!isset($in_index)){header("HTTP/1.0 404 Not Found");  exit();};
?><h1>Registreren</h1>
<?php
if($verzonden && $gelukt){
	echo "U bent succesvol geregistreerd!<br>Uw wijknummer is <strong>".$wijknr."</strong>. U kan nu inloggen met het scherm linksboven.";
}else{
	if($verzonden){
		?>
		<div class="error"><?=$error;?></div>
		<?php
	}
	?>

	<form action="/registreren/" method="POST">
	<table>
		<tr><td style="padding-bottom: 10px; font-size: 16px;" colspan="2">Vul uw gegevens in om u te registreren.</td></tr>
		<tr><td>Naam: </td><td><input name="naam" type="text" value="" /></td></tr>
		<tr><td>Wachtwoord: </td><td><input name="wachtwoord" type="password" value="" /></td></tr>
		<tr><td>&nbsp;</td><td style="padding-top: 5px;"><input name="verzend" type="submit" value="Registreer" /></td></tr>
	</table>
	</form>

	<?php
}
?>