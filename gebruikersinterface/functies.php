<?php
if(!isset($in_index)){header("HTTP/1.0 404 Not Found");  exit();};

// require_once("verbind.php");

/*function parse_template($name){
	ob_start();
		include("tpl_".$name.".php");
		$output = ob_get_contents();
	ob_clean();
	return $output;
}*/

function escape($string) {
	global $db;
	if (get_magic_quotes_gpc()) {
		$string = stripslashes($string);
	}
	return $db->real_escape_string($string);
}

function prettynull($a){ // NULL -> "/"
	return is_null($a) ? "/" : $a;
}
function prettybool($a){ // 1/0 -> "aan"/"uit"
	return $a ? "Aan" : "Uit";
}

function pict($pic){
	return "<img src=\"http://".$_SERVER["HTTP_HOST"]."/images/pict/".$pic.".gif\" border=\"0\" style=\"vertical-align:-20%; margin-right:4px;\" />";
}
?>