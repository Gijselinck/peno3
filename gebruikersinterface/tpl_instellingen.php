<?php
if(!isset($in_index)){header("HTTP/1.0 404 Not Found");  exit();};

if($klok['tijdsstap']){
	?>
<script>
setTimeout(function(){ location.reload(true); }, <?=$klok['dt'];?>);
</script>
	<?php
}
?>

<script>
</script>
<h1>Instellingen</h1>
<h2>Toestellen</h2>
<table class="overzicht">
<thead><tr><td>Naam</td><!--<td>Activiteitsgraad</td><td>Status</td><td>Doelstatus</td>--><td>Verbruik</td><td>Planning</td><td>Grafiek</td></tr></thead>
<?php 
foreach($toestellen as $toestel){
	echo "<tr><td>".$toestel['naam']."</td><!--<td class=\"right\">".$toestel['activiteitsgraad']."%</td><td class=\"right\">".prettynull($toestel['status'])."</td><td class=\"right\">".prettynull($toestel['doelstatus'])."</td>--><td class=\"right\">".$toestel['verbruik']." W</td><td>".(($toestel['naam']!="koelkast")? "<a href=\"/instellingen/planning/?toestel=".$toestel['id']."\">".pict("database_table")."</a>" : "")."</td><td><a href=\"/grafiek/?toestel=".$toestel['id']."\">".pict("graph1")."</a></td></tr>";
}
?>
</table>
<a href="/instellingen/toestel_toevoegen/"><?=pict("add");?>Toestel toevoegen</a>

<h2>Verwarming</h2>
<table class="overzicht">
<thead><tr><td>Temperatuur</td><td>Doeltemperatuur</td><td>Verbruik</td><td>Planning</td><td>Grafiek</td></tr></thead>
<?php
	echo "<tr><td class=\"right\">".prettynull(intval($verwarming['status'])-273)." &deg;C</td><td class=\"right\">".prettynull(intval($verwarming['doelstatus'])-273)." &deg;C</td><td class=\"right\">".$verwarming['verbruik']." W</td><td><a href=\"/instellingen/planning/?toestel=".$verwarming['id']."\">".pict("database_table")."</a></td><td><a href=\"/grafiek/?toestel=".$verwarming['id']."\">".pict("graph1")."</a></td></tr>";
?>
</table>

<h2>Hernieuwbare energiebronnen</h2>
<table class="overzicht">
<thead><tr><td>Naam</td><td>Energieproductie</td><td>Status</td><td>Grafiek</td></tr></thead>
<?php 
foreach($hebs as $heb){
	echo "<tr><td>".$heb['naam']."</td><td class=\"right\">".$heb['productie']." W</td><td>".prettybool($heb['status'])."</td><td><a href=\"/grafiek/?heb=".$heb['id']."\">".pict("graph1")."</a></td></tr>";
}
?>
</table>
<a href="/instellingen/heb_toevoegen/"><?=pict("add");?>Hernieuwbare energiebron toevoegen</a>

<h2>Sensoren</h2>
<table class="overzicht">
<thead><tr><td>Naam</td><td>Waarde</td><td>Status</td><td>Grafiek</td></tr></thead>
<?php 
foreach($sensoren as $sensor){
	echo "<tr><td>".$sensor['naam']."</td><td class=\"right\">".$sensor['waarde']."</td><td>".prettybool($sensor['status'])."</td><td><a href=\"/grafiek/?sensor=".$sensor['id']."\">".pict("graph1")."</a></td></tr>";
}
?>
</table>
<a href="/instellingen/sensor_toevoegen/"><?=pict("add");?>Sensor toevoegen</a>