<?php
if(!isset($in_index)){header("HTTP/1.0 404 Not Found");  exit();};

$pagetitle = "Instellingen";
$tpl_content = "instellingen";

if(!$ingelogd){
	die("Je bent niet ingelogd. <a href=\"/\">Ga terug naar de homepage.</a>");
}

// laad alle toestellen in 
$res = $db->query("SELECT * FROM `toestel` WHERE `wijknr` = '".intval($sesrij['wijknr'])."';");
$toestellen = array();
$verwarming = null;
while($toestel = $res->fetch_array(MYSQLI_ASSOC)){
	if($toestel['naam']=="verwarming"){ // de verwarming tonen we in een aparte tabel
		$verwarming = $toestel;
	}else{
		array_push($toestellen, $toestel);
	}
}

// laad alle hebs in 
$res = $db->query("SELECT * FROM `heb` WHERE `wijknr` = '".intval($sesrij['wijknr'])."';");
$hebs = array();
while($heb = $res->fetch_array(MYSQLI_ASSOC)){
	array_push($hebs, $heb);
}

// laad alle sensoren in 
$res = $db->query("SELECT * FROM `sensor` WHERE `wijknr` = '".intval($sesrij['wijknr'])."';");
$sensoren = array();
while($sensor = $res->fetch_array(MYSQLI_ASSOC)){
	array_push($sensoren, $sensor);
}
?>