<?php
if(!isset($in_index)){header("HTTP/1.0 404 Not Found");  exit();};

$pagetitle = "Home";
$tpl_content = "home";

if($ingelogd){
	// vraag het laatste verbruik op
	$res = $db->query("SELECT `verbruik` FROM `energieverbruik` WHERE `wijknr` = '".intval($sesrij['wijknr'])."' ORDER BY `energieverbruik`.`tijd` DESC LIMIT 1;");
	if($rij = $res->fetch_array(MYSQLI_ASSOC)){
		$verbruiknu = $rij['verbruik'];
	}else{
		$verbruiknu = "<i>onbekend</i>";
	}

	// vraag het maximum verbruik op
	$res = $db->query("SELECT max(`verbruik`) as maximum FROM `energieverbruik` WHERE `wijknr`='".intval($sesrij['wijknr'])."';");
	if($rij = $res->fetch_array(MYSQLI_ASSOC)){
		$verbruikmaxdb = intval($rij['maximum']);
		$verbruikmax = 2000;
		while($verbruikmax<$verbruikmaxdb){
			$verbruikmax += 2000;
		}
	}else{
		$verbruikmax = 2000;
	}

	// vraag het verloop van verbruik op
	$res = $db->query("SELECT `tijd`,`verbruik` FROM `energieverbruik` WHERE `wijknr` = '".intval($sesrij['wijknr'])."' ORDER BY `energieverbruik`.`tijd` ASC;");
	$verbruik = array();
	while($rij = $res->fetch_array(MYSQLI_ASSOC)){
		array_push($verbruik, $rij);
	}

	// vraag de productie op
	/*$res = $db->query("SELECT `productie` FROM `energieproductie` WHERE `wijknr` = '".intval($sesrij['wijknr'])."' ORDER BY `energieproductie`.`tijd` DESC LIMIT 1;");
	if($rij = $res->fetch_array(MYSQLI_ASSOC)){
		$productie = $rij['productie'];
	}else{
		$productie = "<i>onbekend</i>";
	}*/
}

// vraag de stroomprijs op
$res = $db->query("SELECT * FROM `prijs` ORDER BY `prijs`.`tijd` ASC;");
$prijs = array();
while($prijstick = $res->fetch_array(MYSQLI_ASSOC)){
	array_push($prijs, $prijstick);
}

// vraag de zon op
$res = $db->query("SELECT * FROM `zon` ORDER BY `zon`.`tijd` ASC;");
$zon = array();
while($zontick = $res->fetch_array(MYSQLI_ASSOC)){
	array_push($zon, $zontick);
}

// vraag de wind op
$res = $db->query("SELECT * FROM `wind` ORDER BY `wind`.`tijd` ASC;");
$wind = array();
while($windtick = $res->fetch_array(MYSQLI_ASSOC)){
	array_push($wind, $windtick);
}
?>