<?php
if(!isset($in_index)){header("HTTP/1.0 404 Not Found");  exit();};
?><h1>Demo</h1>
<?php
if(trim($error)!=""){
	echo "<div class=\"error\">".$error."</div>";
}
if(trim($success)!=""){
	echo "<div class=\"success\">".$success."</div>";
}
?>
<h2>Stap 1: Optimalisatie</h2>
<p>Kies hier om een optimalisatie uit te voeren, of kies voor een alreeds berekende optimalisatie</p>
<table id="demotable">
<tr>
<td>
	<h3>Voer nieuwe optimalisatie uit</h3>
	<form action="/demo/" method="POST">
		<input type="submit" name="optimaliseer" value="Start optimalisatie" />
	</form>
</td>
<td>
	<h3>Kies een vooraf geoptimaliseerde planning</h3>
	<form action="/demo/" method="POST">
		<select name="optimalisatie">
		  <option value=""></option>
		  <option value="1">Planning 1</option>
		  <option value="2">Planning 2</option>
		  <option value="3">Planning 3</option>
		  <option value="4">Planning 4</option>
		  <option value="5">Planning 5</option>
		  <option value="6">Planning 6</option>
		</select>
		<input type="submit" name="kies_optimalisatie" value="Selecteer" />
	</form>
</td>
</tr>
</table>
<h2>Stap 2: Simulatie</h2>
<p>Hier start je de simulatie</p>
<form action="/demo/" method="POST">
	<input type="submit" name="simuleer" value="Start simulatie" />
</form>