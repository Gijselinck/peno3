<?php
if(!isset($in_index)){header("HTTP/1.0 404 Not Found");  exit();};

$pagetitle = "Planning toestel";
$tpl_content = "instellingen_planning";

$verzonden = false;
$gelukt = false;
$toon = true;
$error = "";

$starttijd = isset($_POST['starttijd']) ? $_POST["starttijd"].":00" : "";
$stoptijd = isset($_POST['stoptijd']) ? $_POST["stoptijd"].":00" : "";
$min_temp = isset($_POST['min_temp']) ? $_POST["min_temp"] : "";
$max_temp = isset($_POST['max_temp']) ? $_POST["max_temp"] : "";
//$status = isset($_POST['status']) ? $_POST["status"] : "";
$status = NULL;  // we gebruiken status niet meer


if(!isset($_GET['toestel'])){
	$error .= "Geef een toestel op.";
	$toon = false;
}else{
	$res = $db->query("SELECT * FROM `toestel` WHERE `id`='".intval($_GET['toestel'])."' AND `wijknr`='".intval($sesrij['wijknr'])."' LIMIT 1");
	if(!$toestel = $res->fetch_array(MYSQLI_ASSOC)){
		$error .= "Dit toestel bestaat niet of u bent niet de eigenaar ervan.";
		$toon = false;
	}else{
		// vraag al de al ingeplande planningen op
		if($toestel['naam']=="verwarming"){
			$res2 = $db->query("SELECT * FROM `instelling_verwarming` WHERE `verwarming_id`='".intval($_GET['toestel'])."' ORDER BY `starttijd` ASC;");
			echo $db->error;
		}else{
			$res2 = $db->query("SELECT * FROM `instelling` WHERE `toestel_id`='".intval($_GET['toestel'])."' ORDER BY `starttijd` ASC;");
		}
		$instellingen = array();
		while($instelling = $res2->fetch_array(MYSQLI_ASSOC)){
			array_push($instellingen, $instelling);
		}

		// handel de eventuele POST af
		if(isset($_POST['inplannen'])){
			// het form is verzonden
			$verzonden = true;

			if(trim($starttijd)==""||trim($stoptijd)==""/*||trim($status)==""*/&&(($toestel['naam']=="verwarming")&&(trim($min_temp)==""||trim($max_temp)==""))){
				$error .= "U heeft niet alle velden ingevuld!";
			}else{
				if($toestel['naam']=="verwarming"){
					// verwarming
					$query = "INSERT INTO `sew`.`instelling_verwarming` (`id`, `verwarming_id`, `starttijd`, `stoptijd`, `max_temp`, `min_temp`) VALUES (NULL, '".intval($toestel['id'])."', '".escape($starttijd)."', '".escape($stoptijd)."', '".(intval($max_temp)+273)."', '".(intval($min_temp)+273)."');";
				}else{
					// alle normale toestellen
					$query = "INSERT INTO `sew`.`instelling` (`id`, `toestel_id`, `starttijd`, `stoptijd`, `status`) VALUES (NULL, '".intval($toestel['id'])."', '".escape($starttijd)."', '".escape($stoptijd)."', '".intval($status)."');";
				}
				if(($res3 = $db->query($query))===true){
					$nieuw_id = $db->insert_id;
					if($nieuw_id>0){
						$gelukt = true;
						header('Location: /instellingen/planning/?toestel='.$toestel['id']); 
					}else{
						$error .= "Er liep iets fout. (Error 2)";
						$error .= "Mysql error:".$db->error;
					}
				}else{
					$error .= "Er liep iets fout. (Error 1)<br>";
					$error .= "Mysql error:".$db->error;
				}
			
			}
		}

		// handel de eventuele verwijder-aanvraag af
		if(isset($_GET['verwijder_instelling'])){
			// in dit stuk care ik niet voor fouten in de parameters, de gebruiker zal dan gewoon worden teruggestuurd
			// merk op dat we al hebben gecontroleerd of dit toestel van deze gebruiker is, we controleren in de query
			// dus alleen nog of het tot het juiste toestel behoort
			if(intval($_GET['verwijder_instelling'])>0){
				if($toestel['naam']=="verwarming"){
					// verwarming
					$db->query("DELETE FROM `sew`.`instelling_verwarming` WHERE `instelling_verwarming`.`id` = ".intval($_GET['verwijder_instelling'])." AND `verwarming_id` = ".$toestel['id'].";");
				}else{
					// normaal toestel
					$db->query("DELETE FROM `sew`.`instelling` WHERE `instelling`.`id` = ".intval($_GET['verwijder_instelling'])." AND `instelling`.`toestel_id` = ".$toestel['id'].";");
				}
			}else{
				die("Er liep iets fout. (Error 3");
			}

			// en herladen maar
			header('Location: /instellingen/planning/?toestel='.$toestel['id']); 
		}
	}
}
?>