<?php
if(!isset($in_index)){header("HTTP/1.0 404 Not Found");  exit();};
?><html>
<head>
	<title>
		<?=$pagetitle; ?> - Smart Energy Houses
	</title>
	<link rel="shortcut icon" href="/images/energyville_favicon.jpg" type="image/x-icon" />
	<link rel="stylesheet" type="text/css" href="/css/main.css" />
</head>
<body>
<div class="container">
	<div class="header">
		<?php if($ingelogd){?>
			<div class="linksboven">
				Welkom <strong><?=$sesrij['naam'];?></strong>!<br>
				Uw wijknummer: <strong><?=$sesrij['wijknr'];?></strong><br>
				(<a href="/uitloggen/">Uitloggen</a>)
			</div>
		<?php }else{ ?>
			<div class="linksboven" id="login">
				<?php include("tpl_inlogformulier.php");?>
			</div>
		<?php } ?>
		<img src="/images/energyville_logo_large.png" />
		<nav>
			<a href="/">Home</a>
			<?php if($ingelogd){ ?>
				<a href="/instellingen/">Instellingen</a>
			<?php } ?>
			<a href="/info/">Info</a>
			<a href="/contact/">Contact</a>
			<a href="/demo/">Demo</a>
		</nav>
	</div>
	<div class="content">
		<?php
			include("tpl_".$tpl_content.".php");
		?>
	</div>
</div>
</body>
</html>