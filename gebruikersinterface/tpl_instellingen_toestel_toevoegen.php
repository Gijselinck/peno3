<?php
if(!isset($in_index)){header("HTTP/1.0 404 Not Found");  exit();};
?><h1>Nieuw toestel</h1>
<?php
if($verzonden && $gelukt){
	echo "Uw toestel  is toegevoegd!";
}else{
	if($verzonden){
		?>
		<div class="error"><?=$error;?></div>
		<?php
	}
	?>

	<form action="/instellingen/toestel_toevoegen/" method="POST">
	<table>
		<tr><td style="padding-bottom: 10px; font-size: 16px;" colspan="3">Beschrijf uw nieuw toestel.</td></tr>
		<tr><td>Naam: </td><td>
			<select name="naam">
			  <option value=""></option>
			  <option value="droogkast">droogkast</option>
			  <option value="e-voertuig">e-voertuig</option>
			  <option value="fornuis">fornuis</option>
			  <option value="koelkast">koelkast</option>
			  <option value="televisie">televisie</option>
			  <option value="vaatwas">vaatwas</option>
			  <option value="verwarming">verwarming</option>
			  <option value="wasmachine">wasmachine</option>
			</select> 
		</td></tr>
		<tr><td>&nbsp;</td><td style="padding-top: 5px;"><input name="verzend" type="submit" value="Toevoegen" /></td></tr>
	</table>
	</form>

	<?php
}
?>