<?php
$in_index = true;
// check of de gebruiker is ingelogd
require_once("verbind.php");
require_once("functies.php");
require_once("usertest.php");
require_once("berekentijd.php");

$pagetitle = "Niet Gevonden";
$tpl_content = "404";

$url1=strtolower(escape( isset($_GET[1]) ? $_GET[1]:"" ));
$url2=strtolower(escape( isset($_GET[2]) ? $_GET[2]:"" ));
$url3=strtolower(escape( isset($_GET[3]) ? $_GET[3]:"" ));
$url4=strtolower(escape( isset($_GET[4]) ? $_GET[4]:"" ));

if($url1==""){
	include('home.php');
}
elseif($url1=="instellingen"){
	if($url2==""){
		include('instellingen.php');
	}elseif($url2=="planning"){
		include('instellingen_planning.php');
	}elseif($url2=="toestel_toevoegen"){
		include('instellingen_toestel_toevoegen.php');
	}elseif($url2=="heb_toevoegen"){
		include('instellingen_heb_toevoegen.php');
	}elseif($url2=="sensor_toevoegen"){
		include('instellingen_sensor_toevoegen.php');
	}
}
elseif($url1=="grafiek"){
	include('grafiek.php');
}
elseif($url1=="demo"){
	include('demo.php');
}
elseif($url1=="info"){
	$pagetitle = "Info";
	$tpl_content = "info";
}
elseif($url1=="contact"){
	$pagetitle = "Contact";
	$tpl_content = "contact";
}
elseif($url1=="registreren"){
	include('registreren.php');
}
elseif($url1=="inloggen"){
	include('inloggen.php');
}
elseif($url1=="uitloggen"){
	include('uitloggen.php');
}

include("tpl_index.php");
// echo parse_template("index");

// close connection to database
$db->close();
?>