<?php
if(!isset($in_index)){header("HTTP/1.0 404 Not Found");  exit();};
?><h1>Nieuwe sensor</h1>
<?php
if($verzonden && $gelukt){
	echo "Uw sensor is toegevoegd!";
}else{
	if($verzonden){
		?>
		<div class="error"><?=$error;?></div>
		<?php
	}
	?>

	<form action="/instellingen/sensor_toevoegen/" method="POST">
	<table>
		<tr><td style="padding-bottom: 10px; font-size: 16px;" colspan="3">Beschrijf uw nieuwe sensor.</td></tr>
		<tr><td>Naam: </td><td>
			<select name="naam">
			  <option value=""></option>
			  <option value="lichtsensor">lichtsensor</option>
			  <option value="temperatuursensor">temperatuursensor</option>
			</select> 
		</td></tr>
		<tr><td>&nbsp;</td><td style="padding-top: 5px;"><input name="verzend" type="submit" value="Toevoegen" /></td><td></td></tr>
	</table>
	</form>

	<?php
}
?>