<?php
if(!isset($in_index)){header("HTTP/1.0 404 Not Found");  exit();};

$pagetitle = "Registreren";
$tpl_content = "registreren";

$verzonden = false;
$gelukt = false;
$error = "";

if(isset($_POST['verzend'])){
	// het form is verzonden
	$verzonden = true;

	$naam 		= isset($_POST['naam']) ? $_POST["naam"] : "";
	$wachtwoord = isset($_POST['wachtwoord']) ? $_POST["wachtwoord"] : "";

	if(trim($naam)==""||trim($wachtwoord)==""){
		$error = "U heeft niet alle velden ingevuld!";
	}else{
		// check of deze naam al in gebruik is
		$res = $db->query("SELECT `naam` FROM `sew` WHERE `naam`='".escape($naam)."' LIMIT 1");
		if($res->num_rows!=0){
			$error="Deze naam is al in gebruik!";
		}else{
			$passhash = md5("qdv68rQo-sb29f@rc5.be".$wachtwoord."vpAou98DV9;=8df".$naam);
			if(($res = $db->query("INSERT INTO `sew` (`wijknr`, `naam`, `wachtwoord`, `sesid`) VALUES (NULL, '".escape($naam)."', UNHEX('".$passhash."'), NULL);"))===true){
				$wijknr = $db->insert_id;
				if($wijknr>0){
					$gelukt = true;
				}else{
					$error = "Er liep iets fout. (Error 2)";
				}
			}else{
				$error = "Er liep iets fout. (Error 1)";
			}
		}
	}
}

?>