<?php
if(!isset($in_index)){header("HTTP/1.0 404 Not Found");  exit();};

$pagetitle = "Hernieuwbare energiebron toevoegen";
$tpl_content = "instellingen_heb_toevoegen";

$verzonden = false;
$gelukt = false;
$error = "";

$naam = isset($_POST['naam']) ? $_POST["naam"] : "";

if(isset($_POST['verzend'])){
	// het form is verzonden
	$verzonden = true;

	if(trim($naam)==""){
		$error = "U heeft niet alle velden ingevuld!";
	}else{
		// check of deze naam al in gebruik is
		$res = $db->query("SELECT `naam` FROM `heb` WHERE `naam`='".escape($naam)."' AND `wijknr`='".intval($sesrij['wijknr'])."' LIMIT 1");
		if($res->num_rows!=0){
			$error="Deze naam is al in gebruik!";
		}else{
			if(($res = $db->query("INSERT INTO `sew`.`heb` (`id`, `wijknr`, `naam`, `energieproductie`, `status`) VALUES (NULL, '".intval($sesrij['wijknr'])."', '".escape($naam)."', '0', '0');"))===true){
				$nieuw_id = $db->insert_id;
				if($nieuw_id>0){
					$gelukt = true;
					header('Location: /instellingen/'); 
				}else{
					$error = "Er liep iets fout. (Error 2)";
				}
			}else{
				$error = "Er liep iets fout. (Error 1)";
			}
		}
	}
}

?>