<?php
if(!isset($in_index)){header("HTTP/1.0 404 Not Found");  exit();};
?><h1 style="display: inline-block;">Planning van <?=$toestel['naam'];?>&nbsp;</h1>(<a style="display: inline-block;" href="/instellingen/">Terug naar de instellingen.</a>)<br>
<?php
if($verzonden && $gelukt){
	echo "Uw toestel is ingepland!";
}else{
	if(trim($error)!=""){
		?>
		<div class="error"><?=$error;?></div>
		<?php
	}
	if($toon){

		if(count($instellingen)){
			?>
			<table class="overzicht">
			<thead><tr><td>Starttijd</td><td>Stoptijd</td><?php if($toestel['naam']=="verwarming"){ ?> <td>Minimumtemperatuur</td><td>Maximumtemperatuur</td> <?php } ?><td>Verwijder</td></tr></thead>
			<?php 
			foreach($instellingen as $instelling){
				echo "<tr><td>".substr($instelling['starttijd'],0,5)."</td><td>".substr($instelling['stoptijd'],0,5)."</td>";
				if($toestel['naam']=="verwarming"){
					echo "<td>".(intval($instelling['min_temp'])-273)."</td><td>".(intval($instelling['max_temp'])-273)."</td>";
				}
				echo "<td><a href=\"/instellingen/planning/?toestel=".$toestel['id']."&verwijder_instelling=".$instelling['id']."\">".pict("delete")."</a></td></tr>";
			}
			?>
			</table>
		<?php
		}else{
			echo "U heeft dit toestel nog geen opdrachten gegeven.";
		}?>
	<br><br>
	Voer hier een nieuwe instelling in:<br>
	<form action="/instellingen/planning/?toestel=<?=$toestel['id'];?>" method="POST">
	<table>
		<tr><td>Starttijd: </td><td><input name="starttijd" type="text" value="<?=$starttijd;?>" /></td><td>vb: <i>14:30</i></td></tr>
		<tr><td>Stoptijd: </td><td><input name="stoptijd" type="text" value="<?=$stoptijd;?>" /></td><td>vb: <i>16:00</i></td></tr>
		<?php if($toestel['naam']=="verwarming"){ ?>
			<tr><td>Minimumtemperatuur: </td><td><input name="min_temp" type="text" value="<?=$min_temp;?>" /></td><td>vb: <i>21</i> (eenheid: &deg;C)</td></tr>
			<tr><td>Maximumtemperatuur: </td><td><input name="max_temp" type="text" value="<?=$max_temp;?>" /></td><td>vb: <i>25</i></td></tr>
		<?php } ?>
		<!--<tr><td>Status: </td><td><input name="status" type="text" value="<?=$status;?>" /></td><td></td></tr>-->
		<tr><td>&nbsp;</td><td style="padding-top: 5px;"><input name="inplannen" type="submit" value="Inplannen" /></td><td></td></tr>
	</table>
	</form>

	<?php
	}
}
?>