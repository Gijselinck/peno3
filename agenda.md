﻿Wat kan je terugvinden in dit bestand?
--------------------------------------
Hier worden de taken bijgehouden die de begeleiders voorstellen te doen tegen 
(en tijdens) de volgende teamzitting(en). Dit is dus niet noodzakelijk volledig
en dient mogelijks te moeten worden aangevuld. Zie ook `huistaken.md` voor
aanvullende taken.

week 5
-------
### ma 20 okt ###
+ zie week 4

### do 23 okt ###
+ ...

------------------------------

week 4
-------
### ma 13 okt ###
+ diagram maken van model en website
+ logboek bijhouden
+ arduino programmeren
+ radiocommunicatie testen
+ RF-communicatie protocol
+ componentenlijst opstellen
+ elektrische schema
+ ideëen meer concretiseren
+ experimenteren met python (voor de eigenlijke code te schrijven)
+ werk verdelen

### do 16 okt ###
+ zie maandag 13 okt

-------------------------------

week 3
-------
### ma 6 okt ###
+ krijgen presentatie (door arne)

### do 8 okt ###
+ geef presentatie over wat en hoe je het ziet, m.a.w. presenteer je concept

-------------------------------

week 2
--------
### do (room 2.58) ###
+ concept
+ niet programmeren
+ focus
+ ideas representation
+ logboek
