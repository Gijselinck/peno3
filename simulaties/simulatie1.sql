/* Testdata
 * 
 * author: Tom Gijselinck <tomgijselinck@gmail.com>
 * date: 5/12/2014
 */

DELETE FROM sew;
DELETE FROM toestel;
DELETE FROM sensor;
DELETE FROM heb;
DELETE FROM instelling;

INSERT INTO sew
VALUES		(1);

INSERT INTO	toestel (id, wijknr, naam, verbruik)
VALUES		(1, 1, 'vaatwas', 1500),
			(2, 1, 'televisie', 750),
			(3, 1, 'verwarming', 1800),
			(4, 1, 'wasmachine', 1900),
			(5, 1, 'e-voertuig', 3700);

INSERT INTO sensor (wijknr, naam)
VALUES		(1, 'lichtsensor');

INSERT INTO heb (wijknr, naam)
VALUES		(1, 'windmolen');

INSERT INTO instelling (toestel_id, starttijd, stoptijd)
VALUES		(2, '20:00', '22:00'),
			(1, '20:00', '6:30'),
			(4, '9:00', '16:00'),
			(5, '8:00', '18:00'),
			(5, '21:00', '22:30');

INSERT INTO instelling_verwarming (verwarming_id, starttijd, stoptijd, max_temp, min_temp)
VALUES		(3, '7:00', '9:00', 25, 18),
			(3, '18:00', '22:00', 27, 20);
