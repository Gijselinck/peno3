/* Testdata
 * 
 * author: Tom Gijselinck <tomgijselinck@gmail.com>
 * date: 6/12/2014
 */

DELETE FROM sew;
DELETE FROM toestel;
DELETE FROM sensor;
DELETE FROM heb;
DELETE FROM instelling;

INSERT INTO sew
VALUES		(1);

INSERT INTO	toestel (id, wijknr, naam, verbruik)
VALUES		(1, 1, 'vaatwas', 1500),
			(2, 1, 'televisie', 750),
			(3, 1, 'verwarming', 1800),
			(4, 1, 'wasmachine', 1900),
			(5, 1, 'e-voertuig', 3700),
			(6, 1, 'droogkast', 1300),
			(7, 1, 'fornuis', 580);

INSERT INTO sensor (wijknr, naam)
VALUES		(1, 'lichtsensor');

INSERT INTO heb (wijknr, naam)
VALUES		(1, 'windmolen');

INSERT INTO instelling (toestel_id, starttijd, stoptijd)
VALUES		(2, '13:00', '14:00'),
			(2, '20:00', '22:00'),
			(4, '9:00', '16:00'),
			(6, '16:30', '21:00'),
			(1, '14:00', '17:00'),
			(1, '20:00', '6:30'),
			(7, '12:45', '13:15'),
			(7, '18:00', '18:45'),
			(5, '8:00', '18:00'),
			(5, '21:00', '22:30');

INSERT INTO instelling_verwarming (verwarming_id, starttijd, stoptijd, max_temp, min_temp)
VALUES		(3, '7:00', '9:00', 25, 18),
			(3, '12:00', '14:00', 25, 18),
			(3, '16:00', '18:00', 20, 16),
			(3, '18:00', '22:00', 27, 20);
