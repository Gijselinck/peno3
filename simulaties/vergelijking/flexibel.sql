/* Testdata
 * 
 * author: Tom Gijselinck <tomgijselinck@gmail.com>
 * date: 14/12/2014
 */

DELETE FROM sew;
DELETE FROM toestel;
DELETE FROM heb;
DELETE FROM instelling;
DELETE FROM user_interface;

INSERT INTO sew
VALUES		(1, '1', UNHEX('06628d59d94ac38b2f3c0737bf13f23e'), NULL),   /* wachtwoord: boe */
			(21, '21', UNHEX('ea3a4105aee532ff5e6d31f8544e7d5b'), NULL); /* wachtwoord: boe */

INSERT INTO	toestel (id, wijknr, naam, verbruik)
VALUES		(1, 21, 'vaatwas', 1500),
			(2, 21, 'televisie', 750),
			(3, 21, 'verwarming', 1800),
			(4, 21, 'wasmachine', 1900),
			(5, 21, 'e-voertuig', 3700),
			(6, 21, 'droogkast', 1300),
			(7, 21, 'fornuis', 580),
			(8, 21, 'koelkast', 1560);

INSERT INTO heb (wijknr, naam)
VALUES		(1, 'windmolen');

INSERT INTO instelling (toestel_id, starttijd, stoptijd)
VALUES		(1, '20:00', '6:30'),
			(2, '20:00', '22:30'),
			(4, '9:00', '17:00'),
			(5, '9:00', '15:00'),
			(6, '12:00', '22:00'),
			(7, '19:00', '20:00'),
			(8, '8:00', '10:00');

INSERT INTO instelling_verwarming (verwarming_id, starttijd, stoptijd, max_temp, min_temp)
VALUES		(3, '0:00', '7:00', 23+273, 11+273),
			(3, '7:00', '22:00', 27+273, 12+273),
			(3, '22:00', '23:00', 23+273, 11+273);

INSERT INTO user_interface
VALUES		(true, true, false);