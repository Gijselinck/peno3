Probleem Oplossen en Ontwerpen deel 3 (ESAT5A2)
===============================================
**Opdracht**: optimaliseer gebruik van
 
+ lokale, hernieuwbare energiebronnen (HEB, eng. RES)
+ gebruik van dynamische energievraag 
+ de energieprijs

Maak hiervoor gebruik van Smart Energy Houses (SEH) die draadloos kunnen
communiceren met een Central Control Unit (CCU). Deze CCU doet aan optimalisatie
rekening houdend met de individuele voorkeuren van de residenten van elk huis.

arduino (huisje + CCU)
--------
Programmeren van de arduino, hardware-uitbreidingen, ...

energy-house-model
-------------------
Toestellen van SEH, circuits voor voorstelling toestellen, ...

functionele decompositie
-------------------------
Verdeling van het project in deeltaken.

gebruikersinterface
--------------------
Ontwerp en bouw van de gebruikersinterface m.b.v. MySQL en PHP.

presentaties
------------
Hier worden alle presentaties verzameld (eigen presentaties, begeleider 
presentaties, ...)

verslagen
----------
Hier staan de verslagen (tussentijds, eind, ...). Naast de eigenlijke verslagen
staan hier ook kladversies. Als je de tex bestanden wilt compileren, moet je 
eerste de modified kulemt documentclass zetten op je computer. Zie 
/verslagen/kulemt.zip voor instructies.

vragen-en-ideëen
-----------------
Hier kan je vragen en ideëen plaatsen, mogelijks nuttige informatie(bronnen), etc.
