Overzicht toestellen
=====================
Categorizering
---------------
+ **Hernieuwbare energiebronnen** zijn toestellen die energie leveren. Ze zijn 
  niet schakelbaar en niet planbaar. Afhankelijk van het weer genereren ze een
  hoeveelheid energie. Ze zijn onvoorspelbaar.
+ **Planbare toestellen** hebben de mogelijkheid om op een bepaald tijdstip in
  te plannen. Ze zijn dus voorspelbaar. In tegenstelling met schakelbare 
  toestellen zijn planbare toestellen niet schakelbaar en kunnen, eenmaal 
  opgestart, niet worden uitgeschakeld tot ze hun cyclus voltooid hebben.
+ **Schakelbare toestellen** zijn zowel planbaar als schakelbaar. Het zijn de 
  meeste flexibele toestellen daar ze zowel gepland kunnen worden als instant
  uitgeschakeld worden.
+ **Niet flexibele** toestellen werken onafhankelijk. Het is mogelijk dat ze 
  volgens een patroon werken, zoals het fornuis dat enkel 's avonds wordt 
  gebruikt. Ze zijn niet planbaar en niet schakelbaar.

Hernieuwbare energiebronnen
---------------------------- 
### windmolen ###
Aan/uit knop, elektromotor met gradaties voor windsterkte.
Potentiometer voor windsterkte te regelen?

### zonnepaneel ###
Niets speciaal.


Planbaar
----------
### wasmachine ###
Motortje geeft cyclus aan (power train).

### vaatwasser ###
Groen ledje voor aan/uit stand.


Schakelbaar
-----------
### koelkast ###
Twee blauwe ledjes om de temperatuur te visualiseren (gradaties in licht
evenredig met temp).
Groen ledje: (knipperen=moet opladen, constant=aan het opladen).
**Opmerking:** koelkast actuators lastig om te programmeren... simplify? (TomG)

### auto ###
Rood, oranje, groen met flikkersysteem.

### verwarming ###
Weerstand (al ingebouwd!)

### boiler (voorlopig niet!) ###
Rood (leeg), oranje (halfvol), groen (vol). Flikkerloop visualiseert opladen.
Boiler ontwerpen (!) in SE.


Niet flexibel
--------------
### tv ###
Wit ledje dat random flikkert.

### fornuis ###
4 rode ledjes.



Overzicht sensors (analoge ingangen arduino)
============================================
tristabiele schakelaar (windmolen)
------------------------------------
stand 1 = wind, stand 2 = neutraal, stand 3 = geen wind

lichtsensor
------------
simuleert bewolking

temperatuur
-----------
aanraken voor temperatuurswijziging









