+ windmolen: elektromotor, tristabiele schakelaar, mosfet
+ wasmachine: elektromotor (), mosfet
+ vaatwasser: groene led, weerstand (?ohm)
+ koelkast: twee blauwe leds, groene led, 3 weerstanden (?ohm)
+ auto: rood, groen, oranje led, 3 weerstanden
+ boiler: rood, groen, oranje led + 3 weerstanden
+ tv: wit led, weerstand
+ fornuis: 1 rode led, weerstand
+ weer: lichtsensor, temperatuurssensor 
+ arduino: shift register

Alles
------
Vorm: omschrijving; aantal; bestelnummer; eenheidsprijs (EHP)

+ weerstand (220 ohm): 	12	3979      stock
+ led, rood:			3	1667      stock
+ led, groen:			4	1661      stock
+ led, blauw			2	5199      stock
+ led, oranje			2	5198      stock
+ led, geel				1	1669      stock
+ elektromotor			2	238-9692  RS
+ lichtsensor			1	1652637   Farnell
+ temperatuurssensor	1	inventaris
+ kabels					Hanspeter
+ MOSFET				1	8657599   Farnell
+ tristabiele schak		1	9473300   Farnell
+ shift register		2
+ QUASAR - QAM-TX1 - RF MOD, TRANSMITTER, AM, ASK, 433MHZ; 1; 1304024; 4,83 �
+ QUASAR - QAM-RX2 - RF MODULE, RECEIVER, AM, SUPER REGEN; 1; 1304026; 6,38 �