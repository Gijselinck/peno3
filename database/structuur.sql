-- Auteurs: Robbert Camps <me@robbertc5.com>
--         Tom Gijselinck <tomgijselinck@gmail.com>

SET time_zone = "+00:00";

--
-- Database: `sew`
--

/* 
 * Moet hier staan wegens constraints (foreign keys) 
 */
DROP TABLE IF EXISTS `user_interface`;
DROP TABLE IF EXISTS `simulaties`;
DROP TABLE IF EXISTS `klok`;
DROP TABLE IF EXISTS `net_energieverbruik`;
DROP TABLE IF EXISTS `energieverbruik`;
DROP TABLE IF EXISTS `heb_gesch`;
DROP TABLE IF EXISTS `sensor_gesch`;
DROP TABLE IF EXISTS `toestel_gesch`;
DROP TABLE IF EXISTS `instelling`;
DROP TABLE IF EXISTS `instelling_verwarming`;
DROP TABLE IF EXISTS `sensor`;
DROP TABLE IF EXISTS `toestel`;
DROP TABLE IF EXISTS `heb`;

DROP TABLE IF EXISTS `zon`;
DROP TABLE IF EXISTS `wind`;
DROP TABLE IF EXISTS `Toutside`;
DROP TABLE IF EXISTS `wasmachine`;
DROP TABLE IF EXISTS `vaatwas`;
DROP TABLE IF EXISTS `verwarming`;
DROP TABLE IF EXISTS `Thouse`;
DROP TABLE IF EXISTS `EV`;
DROP TABLE IF EXISTS `ijskast`;
DROP TABLE IF EXISTS `prijs`;
DROP TABLE IF EXISTS `tv`;
DROP TABLE IF EXISTS `fornuis`;

DROP TABLE IF EXISTS `sew`;

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `ccu`
--

CREATE TABLE `user_interface` (
  `start_simulatie` boolean NOT NULL DEFAULT false,
  `start_optimalisatie` boolean NOT NULL DEFAULT false,
  `skip_optimalisatie` boolean NOT NULL DEFAULT false
) ENGINE=InnoDB;

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `simulaties`
--

CREATE TABLE `simulaties` (
  `id` int NOT NULL,
  `naam` varchar(32) NULL DEFAULT NULL,
  `uitleg` text DEFAULT NULL
) ENGINE=InnoDB;

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `klok`
--

CREATE TABLE IF NOT EXISTS `klok` (
  `tijdsstap` int(10) unsigned DEFAULT NULL,
  `tijd` time DEFAULT NULL,
  `dt` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `sew`
--

CREATE TABLE `sew` (
  `wijknr` smallint unsigned NOT NULL AUTO_INCREMENT,
  `naam` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `wachtwoord` binary(16) DEFAULT NULL,
  `sesid` binary(16) DEFAULT NULL,
  PRIMARY KEY (`wijknr`)
) ENGINE=InnoDB AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `energieproductie`
--

CREATE TABLE `net_energieverbruik` (
  `tijd` time NOT NULL,
  `wijknr` smallint unsigned NOT NULL,
  `net_verbruik` float unsigned NOT NULL,
  PRIMARY KEY (`tijd`,`wijknr`),
  FOREIGN KEY (`wijknr`) 
    REFERENCES `sew` (`wijknr`)
    ON DELETE CASCADE
    ON UPDATE CASCADE
) ENGINE=InnoDB;

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `energieverbruik`
--

CREATE TABLE `energieverbruik` (
  `tijd` time NOT NULL,
  `wijknr` smallint unsigned NOT NULL,
  `verbruik` float NOT NULL,
  PRIMARY KEY (`tijd`,`wijknr`),
  FOREIGN KEY (`wijknr`) 
    REFERENCES `sew` (`wijknr`)
    ON DELETE CASCADE
    ON UPDATE CASCADE
) ENGINE=InnoDB;

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `heb`
--

CREATE TABLE `heb` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `wijknr` smallint unsigned NOT NULL,
  `naam` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `productie` float unsigned NOT NULL,
  `status` tinyint(4) unsigned NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`),
  FOREIGN KEY (`wijknr`) 
    REFERENCES `sew` (`wijknr`)
    ON DELETE CASCADE
    ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `heb_gesch`
--

CREATE TABLE `heb_gesch` (
  `tijd` time NOT NULL,
  `heb_id` int(10) unsigned NOT NULL,
  `productie` float unsigned NOT NULL,
  `status` tinyint(3) unsigned NOT NULL COMMENT '100 is aan; 0 is uit',
  PRIMARY KEY (`tijd`,`heb_id`),
  FOREIGN KEY (`heb_id`) 
    REFERENCES `heb` (`id`)
    ON UPDATE CASCADE
) ENGINE=InnoDB;

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `toestel`
--

CREATE TABLE `toestel` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `wijknr` smallint unsigned NOT NULL,
  `naam` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `activiteitsgraad` tinyint(100) unsigned DEFAULT 0 COMMENT 'procent',
  `status` float unsigned DEFAULT 0,
  `doelstatus` float unsigned DEFAULT 0,
  `verbruik` float unsigned NOT NULL,
  PRIMARY KEY (`id`),
  FOREIGN KEY (`wijknr`) 
    REFERENCES `sew` (`wijknr`)
    ON DELETE CASCADE
    ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `toestel_gesch`
--

CREATE TABLE `toestel_gesch` (
  `tijd` time NOT NULL,
  `toestel_id` int(10) unsigned NOT NULL,
  `activiteitsgraad` float NOT NULL,
  `status` float NOT NULL,
  `doelstatus` float NOT NULL,
  `verbruik` float NOT NULL,
  PRIMARY KEY (`tijd`,`toestel_id`),
  FOREIGN KEY (`toestel_id`) 
    REFERENCES `toestel` (`id`)
    ON UPDATE CASCADE
) ENGINE=InnoDB;

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `instelling`
--

CREATE TABLE `instelling` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `toestel_id` int(10) unsigned NOT NULL,
  `starttijd` time NOT NULL,
  `stoptijd` time NOT NULL,
  `status` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  FOREIGN KEY (`toestel_id`) 
    REFERENCES `toestel` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `instelling_verwarming`
--

CREATE TABLE `instelling_verwarming` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `verwarming_id` int(10) unsigned NOT NULL,
  `starttijd` time NOT NULL,
  `stoptijd` time NOT NULL,
  `max_temp` int(4) NOT NULL,
  `min_temp` int(4) NOT NULL,
  PRIMARY KEY (`id`),
  FOREIGN KEY (`verwarming_id`)
    REFERENCES `toestel` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=1;

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `sensor`
--

CREATE TABLE `sensor` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `wijknr` smallint unsigned NOT NULL,
  `naam` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `waarde` float unsigned NOT NULL DEFAULT 0,
  `status` tinyint(3) unsigned NOT NULL DEFAULT 0 COMMENT '100 is aan; 0 is uit',
  PRIMARY KEY (`id`),
  FOREIGN KEY (`wijknr`) 
    REFERENCES `sew` (`wijknr`)
    ON DELETE CASCADE
    ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `sensor_gesch`
--

CREATE TABLE `sensor_gesch` (
  `tijd` time NOT NULL,
  `sensor_id` int(10) unsigned NOT NULL,
  `waarde` float unsigned NOT NULL,
  `status` tinyint(3) unsigned NOT NULL COMMENT '100 is aan; 0 is uit',
  PRIMARY KEY (`tijd`,`sensor_id`),
  FOREIGN KEY (`sensor_id`) 
    REFERENCES `sensor` (`id`)
    ON UPDATE CASCADE
) ENGINE=InnoDB;

-- --------------------------------------------------------





--
-- GAMS tabellen
--
-- Opmerking:
-- Er wordt vanuit gegegaan dat er zich maximaal één instantie van elk type
-- toestel bevindt in elk huis. Bijgevolg is "wijknr" een voldoende sterke key
-- en is "id" niet nodig.


--
-- Tabelstructuur voor tabel `zon`
--

CREATE TABLE `zon` (
  `tijd` time NOT NULL,
  `load_factor` float unsigned NOT NULL,
  PRIMARY KEY (`tijd`)
) ENGINE=InnoDB;

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `wind`
--

CREATE TABLE `wind` (
  `tijd` time NOT NULL,
  `load_factor` float unsigned NOT NULL,
  PRIMARY KEY (`tijd`)
) ENGINE=InnoDB;

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `Toutside`
--

CREATE TABLE `Toutside` (
  `tijd` time NOT NULL,
  `temperatuur` float unsigned NOT NULL,
  PRIMARY KEY (`tijd`)
) ENGINE=InnoDB;

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `wasmachine`
--

CREATE TABLE `wasmachine` (
  `wijknr` smallint unsigned NOT NULL,
  `tijd` time NOT NULL,
  `verbruik` float unsigned NOT NULL,
  PRIMARY KEY (`wijknr`, `tijd`),
  FOREIGN KEY (`wijknr`) 
    REFERENCES `sew` (`wijknr`)
    ON DELETE CASCADE
    ON UPDATE CASCADE
) ENGINE=InnoDB;

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `vaatwas`
--

CREATE TABLE `vaatwas` (
  `wijknr` smallint unsigned NOT NULL,
  `tijd` time NOT NULL,
  `verbruik` float unsigned NOT NULL,
  PRIMARY KEY (`wijknr`, `tijd`),
  FOREIGN KEY (`wijknr`) 
    REFERENCES `sew` (`wijknr`)
    ON DELETE CASCADE
    ON UPDATE CASCADE
) ENGINE=InnoDB;

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `verwarming`
--

CREATE TABLE `verwarming` (
  `wijknr` smallint unsigned NOT NULL,
  `tijd` time NOT NULL,
  `verbruik` float unsigned NOT NULL,
  PRIMARY KEY (`wijknr`, `tijd`),
  FOREIGN KEY (`wijknr`) 
    REFERENCES `sew` (`wijknr`)
    ON DELETE CASCADE
    ON UPDATE CASCADE
) ENGINE=InnoDB;

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `Thouse`
--

CREATE TABLE `Thouse` (
  `wijknr` smallint unsigned NOT NULL,
  `tijd` time NOT NULL,
  `temperatuur` float unsigned NOT NULL,
  PRIMARY KEY (`wijknr`, `tijd`),
  FOREIGN KEY (`wijknr`) 
    REFERENCES `sew` (`wijknr`)
    ON DELETE CASCADE
    ON UPDATE CASCADE
) ENGINE=InnoDB;

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `EV`
--

CREATE TABLE `EV` (
  `wijknr` smallint unsigned NOT NULL,
  `tijd` time NOT NULL,
  `SOC` float unsigned NOT NULL,
  `evchargin` int unsigned DEFAULT NULL,
  PRIMARY KEY (`wijknr`, `tijd`),
  FOREIGN KEY (`wijknr`) 
    REFERENCES `sew` (`wijknr`)
    ON DELETE CASCADE
    ON UPDATE CASCADE
) ENGINE=InnoDB;

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `ijskast`
--

CREATE TABLE `ijskast` (
  `wijknr` smallint unsigned NOT NULL,
  `tijd` time NOT NULL,
  `verbruik` float unsigned NOT NULL,
  PRIMARY KEY (`wijknr`, `tijd`),
  FOREIGN KEY (`wijknr`) 
    REFERENCES `sew` (`wijknr`)
    ON DELETE CASCADE
    ON UPDATE CASCADE
) ENGINE=InnoDB;

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `price`
--

CREATE TABLE `prijs` (
  `tijd` time NOT NULL,
  `euro-kwh` float unsigned NOT NULL,
  PRIMARY KEY (`tijd`)
) ENGINE=InnoDB;

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `tv`
--

CREATE TABLE `tv` (
  `wijknr` smallint unsigned NOT NULL,
  `tijd` time NOT NULL,
  `verbruik` float unsigned NOT NULL,
  PRIMARY KEY (`wijknr`, `tijd`),
  FOREIGN KEY (`wijknr`) 
    REFERENCES `sew` (`wijknr`)
    ON DELETE CASCADE
    ON UPDATE CASCADE
) ENGINE=InnoDB;

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `fornuis`
--

CREATE TABLE `fornuis` (
  `wijknr` smallint unsigned NOT NULL,
  `tijd` time NOT NULL,
  `verbruik` float unsigned NOT NULL,
  PRIMARY KEY (`wijknr`, `tijd`),
  FOREIGN KEY (`wijknr`) 
    REFERENCES `sew` (`wijknr`)
    ON DELETE CASCADE
    ON UPDATE CASCADE
) ENGINE=InnoDB;



-- --------------------------------------------------------

--
-- INSERTS
--

INSERT INTO simulaties
VALUES  ('1', 'Planning 1: standaard', 'Een standaard planning met eenvoudige instellingen'),
        ('2', 'Planning 2: flexibel', 'Een flexibele planning om te vergelijken met een niet-flexibele planning'),
        ('3', 'Planning 3: niet-flexibel', 'Een niet-flexibele planning om te vergelijken met een flexibele planning');