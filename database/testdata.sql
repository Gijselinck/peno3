/* Testdata
 * 
 * author: Tom Gijselinck <tomgijselinck@gmail.com>
 * date: 2/12/2014
 */

DELETE FROM sew;
DELETE FROM toestel;
DELETE FROM sensor;
DELETE FROM heb;
DELETE FROM instelling;
DELETE FROM user_interface;

INSERT INTO sew
VALUES		(1, '1', UNHEX('06628d59d94ac38b2f3c0737bf13f23e'), NULL),   /* wachtwoord: boe */
			(21, '21', UNHEX('ea3a4105aee532ff5e6d31f8544e7d5b'), NULL); /* wachtwoord: boe */

INSERT INTO	toestel
VALUES		(1, 1, 'koelkast', 100, 5, 5, 1500),
			(2, 1, 'televisie', 0, NULL, NULL, 750),
			(3, 1, 'verwarming', 60, 18, 22, 1800),
			(4, 21, 'koelkast', 0, 5, 8, 900),
			(5, 21, 'televisie', 100, NULL, NULL, 500),
			(6, 21, 'verwarming', 50, 18, 16, 1600),
			(7, 21, 'wasmachine', 0, NULL, NULL, 3500),
			(8, 1, 'vaatwas', 0, NULL, NULL, 1600),
			(9, 1, 'wasmachine', 0, NULL, NULL, 3100),
			(10, 1, 'e-voertuig', 0, NULL, NULL, 5000),
			(11, 21, 'e-voertuig', 0, NULL, NULL, 4200),
			(12, 21, 'vaatwas', 0, NULL, NULL, 650);

INSERT INTO sensor
VALUES		(1, 1, 'lichtsensor', NULL, 0),
			(2, 21, 'lichtsensor', 65, 0);

INSERT INTO heb
VALUES		(1, 1, 'windmolen', 2900, 1),
			(2, 21, 'windmolen', 2850, 1),
			(3, 21, 'zonnepanneel', 450, 1);

INSERT INTO instelling
VALUES		(1, 7, '9:00', '16:00', 100), /* 100 betekent voltooid */
			(2, 5, '20:00', '22:00', 100), /* tv kijken */
			(3, 8, '20:00', '23:00', 100),
			(4, 9, '10:00', '16:00', 100),
			(5, 10, '5:00', '20:00', 100), /* auto huis 1 --> gaat niet*/
			(6, 11, '8:00', '9:00', 100), /* auto huis 21 */
			(7, 12, '20:00', '6:30', 100);

INSERT INTO instelling_verwarming
VALUES		(1, 3, '7:00', '9:00', 25+273, 20+273),
			(2, 3, '18:00', '22:00', 22+273, 18+273),
			(3, 6, '6:30', '8:00', 20+273, 18+273),
			(4, 6, '12:30', '14:00', 20+273, 16+273),
			(5, 6, '18:00', '21:00', 22+273, 16+273);

INSERT INTO user_interface
VALUES		(true, true, false);

INSERT INTO klok
VALUES		(0, '00:00:00', 1);