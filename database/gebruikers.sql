-- Voer deze query's enkel de eerste keer (bij het installeren van de database) uit!


# Rechten voor `sew_php`@`%`

GRANT USAGE ON *.* TO 'sew_php'@'%' IDENTIFIED BY PASSWORD '*B3E2D5A1769164CCC451FE6051A78B6F7AC7A574';

GRANT SELECT, INSERT, UPDATE, DELETE ON `sew`.* TO 'sew_php'@'%';