/* Testdata voor simulatie sew
 * 
 * author: Tom Gijselinck <tomgijselinck@gmail.com>
 * date: 10/12/2014
 *
 * opmerking: maakt gebruik van testdata.sql
 */

DELETE FROM `zon`;
DELETE FROM `wind`;
DELETE FROM `Toutside`;
DELETE FROM `wasmachine`;
DELETE FROM `vaatwas`;
DELETE FROM `verwarming`;
DELETE FROM `Thouse`;
DELETE FROM `EV`;
DELETE FROM `ijskast`;
DELETE FROM `prijs`;
DELETE FROM `tv`;
DELETE FROM `fornuis`;


INSERT INTO zon
VALUES		('00:00', 100),
			('12:00', 100);

INSERT INTO wind
VALUES		('00:00', 100),
			('12:00', 100);

INSERT INTO Toutside
VALUES		('00:00', 283),
			('12:00', 283);

INSERT INTO wasmachine
VALUES		(1, '9:00',100),
			(1, '17:00',150);

INSERT INTO vaatwas
VALUES		(1, '8:30',80),
			(1, '19:30',230);

INSERT INTO verwarming
VALUES		(1, '7:00', 1500),
			(1, '10:00', 500),
			(1, '17:00', 1900),
			(1, '22:00', 500);

INSERT INTO Thouse
VALUES		(1, '00:00', 283),
			(1, '8:00', 293),
			(1, '11:00', 287),
			(1, '18:00', 295),
			(1, '23:00', 289);

INSERT INTO EV
VALUES		(1, '8:00', 100/*, 1200*/),
			(1, '10:00', 65/*, 0*/),
			(1, '18:00', 30/*, 0*/),
			(1, '22:00', 50/*, 1200*/);

INSERT INTO ijskast
VALUES		(1, '7:00', 1500),
			(1, '10:00', 500),
			(1, '17:00', 1900),
			(1, '22:00', 500);

INSERT INTO prijs
VALUES		('8:00', 2.6),
			('11:00', 3.9),
			('23:00', 1.7);

INSERT INTO tv
VALUES		(1, '00:00', 3),
			(1, '17:00', 138),
			(1, '18:30', 3),
			(1, '22:00', 138),
			(1, '23:00', 3);

INSERT INTO fornuis
VALUES		(1, '0:00', 0),
			(1, '13:00', 350),
			(1, '14:00', 0),
			(1, '18:00', 350),
			(1, '19:00', 0);