DROP VIEW IF EXISTS instellingView;

CREATE VIEW instellingView AS
SELECT i.id, i.toestel_id, i.starttijd, i.stoptijd, t.naam, t.wijknr
FROM instelling AS i
	INNER JOIN toestel AS t ON (i.toestel_id = t.id);