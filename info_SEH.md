Slimme energiewoningen
-----------------------
+ http://www.linear-smartgrid.be/
+ http://homes.esat.kuleuven.be/~hhoschle/peno3/
+ http://www.fifthplay.com/en/smart-homes-en
+ http://www.infrax.be/nl/wat-te-doen-bij/slimme-meter
+ http://www.eandis.be/eandis/slimme_meters.htm
+ http://www.e-hub.org/smart-energy-control.html & http://www.e-hub.org/
+ https://www.vito.be/NL/HomepageAdmin/Home/WetenschappelijkOnderzoek/Energietechnologie/Pages/wetenschap_energietechnologie.aspx

Elektriciteit algemeen
-----------------------
+ Prijs van elektriciteit wordt dubbel zo duur (deredactie.be, 17/10/2014):
  http://deredactie.be/permalink/2.36140?video=1.2121347
+ Het Belgische energiesysteem in 2050: Waar naartoe? - Beschrijving van een 
  Referentiescenario voor België:
  http://www.plan.be/publications/publication_det.php?lang=nl&KeyPub=1388