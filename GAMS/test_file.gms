$eolcom #
sets
g cat1 /Tv, fornuis/
h catd /dryer/ # dryer
i catwm /washing_machine/
j catdw /dishwasher/
k cat3 /fridge /
l cat4 / heating/
n cat5 /EV/
*o cat6 /airco/?
t tijd /01*10/;
alias(t,ta);

* ALLES in nederlands of alles in engels?!
* denk eraan: Temperatuur in Kelvin!

Scalars
deltaT ��n tijdsstap /900/

dd duration cycle dryer
wmd duration cycle washing machine
dwd  duration cycle dishwasher

mCp4 heat capacity cat4 /540000/
UAhouse heat transmission coeffici�nt /150/  # eng
COP4 Coefficient of performance /3/

Eff efficiency of electricity transmission /0.90/;

Table tstartd(t,h) possible start times
         dryer
01         0
02         0
03         0
04         0
05         0
06         0
07         0
08         0
09         0
10         0     ;
Table tstartwm(t,i) possible start times
        washing_machine
01               0
02               0
03               0
04               1
05               1
06               1
07               1
08               0
09               0
10               0      ;
Table tstartdw(t,j) possible start times
        dishwasher
01            0
02            0
03            0
04            1
05            1
06            1
07            1
08            1
09            1
10            0   ;
Parameters

* Supply
Sgrid(t) Supply grid
Sres(t) Supply local RES

* Categorie 1
dcat1(t,g) demand category 1

* Categorie 2
* We suppose that every washing machine has the same cycle.
* The same for the dryers and dishwashers
dc(ta) demand-cycle dryer
/01 00
 02 00
 03 00/
nbcd(h) number of dryer cycles
/dryer 00/
wmc(ta) demand-cycle washing machine
/01 23
 02 18
 03 6/
nbcwm(i) number of washing machine cycles
/washing_machine 1/

* let op: final start = tijd die gebruiker ingeeft min lengte cyclus
dwc(ta) demand-cycle dishwasher
/01 50
02 20
03 20
04 5/
nbcdw(j) number of dishwasher cycles
/dishwasher 1/


*Categorie 3
P(k) power
/fridge 200/
* UA3/mCp3 is equal to 5.56E-5
UA3(k)
/fridge 0.0000556/
* COP3/mCp3 is equal to 1.68E-5
COP3(k) Coefficient of performance
/fridge 0.0000167/
mCp3(k) heat capacity cat3
/fridge 1/;
Table Tmin3(t,k) minimum Temperature
        fridge
01       273
02       273
03       273
04       273
05       273
06       273
07       273
08       273
09       273
10       273;
Table Tmax3(t,k) maximum Temperature
        fridge
01       278
02       278
03       278
04       278
05       278
06       278
07       278
08       278
09       278
10       278;

Parameters
*Categorie 4
Pmax(l) maximum vermogen van apparaat
/heating 2000/
Tmin4(t) minimum temperatuur
/01 293
02 293
03 293
04 293
05 293
06 293
07 293
08 293
09 293
10 293/
Tmax4(t) maximum temperatuur
/01 303
02 303
03 303
04 303
05 297
06 297
07 297
08 297
09 297
10 297/;

* Categorie 5
Table SOC_wanted(t,n) SOC wanted by the user
         EV
01       400
02       400
03       400
04       400
05       400
06       400
07       400
08       400
09       1600
10       1600           ;

Table Pdr(t,n) driving power of EV
         EV
01       0.6
02       0.6
03       0
04       0
05       0
06       0
07       0
08       0
09       0
10       0           ;


Table xdr(t,n) driving EV
         EV
01       1
02       1
03       0
04       0
05       0
06       0
07       0
08       0
09       0
10       0;
Parameters
PmaxEV(n) maximum power of charging
/EV 3/
* eigenlijk in Joule per kwartier ! (voor alle vermogens)
SOCmax(n) maximum SOC
/EV 2000/
* 40 kWh inhoud batterij


* Algemeen
Toutside(t) buitenhuistemperatuur
/01 279
02 279
03 279
04 279
05 279
06 279
07 279
08 279
09 279
10 279/

price(t) prijs elektriciteit
/01 10
02 10
03 10
04 5
05 10
06 10
07 10
08 10
09 10
10 10/;

dd = card(wmc);
wmd = card(wmc);
dwd = card(dwc);

Variables
z1 cost
z2 cost
zh cost
zi cost
zj cost
z3 cost
z4 cost
z5 cost
z cost
xch charging or discharging;

Binary variables
zfr(t) toestand cat3 aan-uit;

Positive variables
*wat kan mijn huis beslissen
Pd(t,h) power of dryer
tid(t,h) starting time cycle dryer
Pwm(t,i) power of washing machine
tiwm(t,i) starting time cycle wasmachine
Pdw(t,j) power of dishwasher
tidw(t,j) starting time cycle dishwasher
Pin3(t,k) vermogen cat3
Qin3(t,k) Heat going in
Qout3(t,k) Heat going out
Tinside3(t,k) Temperature in the freezer

Pin4(t,l) vermogen cat4
Pconv(t) Energie uit het net
Qin4(t,l) Heat going in
Qout4(t) Heat going out
Curt(t) Curtailment
Thouse(t) binnenhuistemperatuur
SOC(t,n) State of charge Electric Vehicle (EV)
SOCrel(t,n) SOC relative to total capacity  ;
*Thouse.fx('01')=293;
*Thouse.fx('02')=293;
*Thouse.fx('03')=293;
*Thouse.fx('04')=293;
*Thouse.fx('05')=293;
*Thouse.fx('06')=293;
*Thouse.fx('07')=293;
*Thouse.fx('08')=293;
*Thouse.fx('09')=293;
*Thouse.fx('10')=293;
*Tinside3.fx('01')=278;



Equations
* Categorie 1
qobj1 objective function
* Categorie 2
*   Dryer
qPd(t,h) power equation of dryer
qd(t,h) check the number of dryers
qdc check if one dryer does exactly one cycle
qtd(t,h) toegelaten starttijd
qzh cost dryer
*   Washing machine
qPwm(t,i) power equation of washing machine
qwm(t,i) check the number of washing machines
qwmc check if one washing machine does exactly one cycle
qtwm(t,i) toegelaten starttijd
qzi cost washing machine
*   Dishwasher
qPdw(t,j) power equation of dishwasher
qdw(t,j) check the number of dishwashers
qdwc check if one dishwasher does exactly one cycle
qtdw(t,j) toegelaten starttijd
qzj cost dishwasher

*   objective
qobj2 objective function

* Categorie 3
qTmin3(t,k) Minimum temperatuur
qTmax3(t,k) Maximum temperatuur
qevolt3(t,k) hoofdvgml 3
qQin3(t,k)
qQOut3(t,k)
qPin3(t,k)
qobj3
* Categorie 4
qTmin4(t) Minimum temperatuur
qTmax4(t) Maximum temperatuur
qevolt4(t,l) hoofdvgl
qQin4(t,l) warmte naar apparaat
qQout4(t) warmte naar buiten
qPin4(t,l) beperking op vermogen
qobj4
* Categorie 5
qrestr1(t,n)  restriction on xch
qrestr2(t,n)  restriction on xch
qsituation(t,n) situation of EV
qSOCmax(t,n) maximum energy available
qevolt5(t,n) head equation
qSOCw(t,n) State of charge of EV
qobj5
qSOCrel
* Totaal
qobj
;

*Categorie 1
qobj1.. z1 =e= sum((t,g),dcat1(t,g)*price(t))  ;
*Categorie 2
*  Dryer
qPd(t,h).. Pd(t,h) =e= sum(ta$dd,tid(t-(ord(ta)-1),h)*dc(ta));
qd(t,h).. sum(ta,tid(t-(ord(ta)-1),h)) =l= card(h);
* 'o' aan rechterkant om infeasible weg te doen (uiteindelijk vanuit python automatiseren)
qdc.. sum(h,sum(t$(ord(t)<card(t)-(dd-2)),tid(t,h))) =e= sum(h,nbcd(h));
* RHS veranderd van card(h) naar 'o'

*  Washing machine
qPwm(t,i).. Pwm(t,i) =e= sum(ta$wmd,tiwm(t-(ord(ta)-1),i)*wmc(ta));
qwm(t,i).. sum(ta,tiwm(t-(ord(ta)-1),i)) =l= card(i); # 1 = aantal wasmachines
qwmc.. sum(i,sum(t$(ord(t)<card(t)-(wmd-2)),tiwm(t,i))) =e= sum(i,nbcwm(i)); # aantal cycli die moeten worden uitgevoerd.
*  Dishwasher
qPdw(t,j).. Pdw(t,j) =e= sum(ta$dwd,tidw(t-(ord(ta)-1),j)*dwc(ta));
qdw(t,j).. sum(ta,tidw(t-(ord(ta)-1),j)) =l= card(j); # 1 = aantal wasmachines
qdwc.. sum(j,sum(t$(ord(t)<card(t)-(dwd-2)),tidw(t,j))) =e= sum(j,nbcdw(j)); # aantal cycli die moeten worden uitgevoerd.


qtd(t,h).. tid(t,h) =l= tstartd(t,h);
qtwm(t,i).. tiwm(t,i) =l= tstartwm(t,i);
qtdw(t,j).. tidw(t,j) =l= tstartdw(t,j);

*  objective
qzh .. zh =e= sum(h,sum((t),Pd(t,h)*price(t)));
qzi  .. zi =e= sum(i,sum((t),Pwm(t,i)*price(t))) ;
qzj ..  zj =e= sum(j,sum((t),Pdw(t,j)*price(t)))  ;
qobj2..  z2 =e= zh + zi+ zj;

*Categorie 3
qTmin3(t,k)..   Tinside3(t,k)=g=Tmin3(t,k);
qTmax3(t,k)..   Tinside3(t,k)=l=Tmax3(t,k);
qevolt3(t,k)$ (ord(t)<card(t)).. Tinside3(t+1,k) =e= Tinside3(t,k) + (deltaT*(Qout3(t,k)-Qin3(t,k)))/mCp3(k);
* eigenlijk zou Pin3(t) negatief moeten zijn. Dus daar moeten we rekening mee houden.
qQin3(t,k).. Qin3(t,k)=e= COP3(k)*Pin3(t,k);
qQOut3(t,k).. Qout3(t,k) =e= UA3(k)*(Thouse(t)-Tinside3(t,k));
qPin3(t,k).. Pin3(t,k) =e= zfr(t)*P(k);
qobj3.. z3 =e= sum((t,k),Pin3(t,k)*price(t));

* Categorie 4
qTmin4(t).. Thouse(t)=g= Tmin4(t);
qTmax4(t).. Thouse(t)=l= Tmax4(t);
qQin4(t,l).. Qin4(t,l)=e= COP4*Pin4(t,l);
qQout4(t).. Qout4(t)=e= UAhouse*(Thouse(t)-Toutside(t)) ;
qevolt4(t,l)$ (ord(t)<card(t)).. Thouse(t++1) =e= Thouse(t) + (deltaT*(Qin4(t,l)-Qout4(t)))/mCp4;
qPin4(t,l).. Pin4(t,l)=l=Pmax(l); # voor elke l, zijn er t vgln.
qobj4..   z4 =e= sum((t,l),Pin4(t,l)*price(t));

*categorie 5
qrestr1(t,n).. xch(t,n) =l= 1;
qrestr2(t,n).. xch(t,n) =g= -1;
qsituation(t,n).. xdr(t,n)+abs(xch(t,n)) =l= 1;
qSOCw(t,n).. SOC(t,n) =g= SOC_wanted(t,n);
qSOCmax(t,n).. SOC(t,n) =l= SOCmax(n);
qevolt5(t,n)..  SOC(t++1,n) =e=
         SOC(t,n) +
         (deltaT*Eff*PmaxEV(n)*xch(t,n))-(deltaT*Pdr(t,n)*xdr(t,n));
qobj5.. z5 =e= sum((t,n),PmaxEV(n)*xch(t,n)*price(t));
qSOCrel(t,n).. SOCrel(t,n) =e= SOC(t,n)/SOCmax(n);
* als 5 testen tijdsstap op 5 of een echte energieinhoud batterij gerbuiken!
* Totaal
qobj.. z =e= z2+z3+z4+z5; # moet nog uitgebreid worden naar 1,2 en 5



Model SEW /
*qobj1,
qPd,qd,qdc,qtd,qzh
qPwm,qwm,qwmc,qtwm,qzi,
qPdw,qdw,qdwc,qtdw,qzj,
qobj2,
qTmin3,qTmax3,qevolt3,qQin3,qQOut3,qPin3,qobj3,
qTmin4,qTmax4,qQin4,qQout4,qevolt4,qPin4,qobj4
qrestr1,qrestr2,qsituation,qSOCw,qSOCmax,qevolt5,qobj5,qSOCrel
qobj
/;


Solve SEW using minlp minimizing z;

display zh.l,zi.l,zj.l,z2.l,z3.l,z4.l,z5.l,z.l
         Pd.l,Pwm.l,Pdw.l,Tinside3.l,Pin3.l,Thouse.l,Pin4.l
         SOC.l,xch.l,SOCrel.l;

*z3.l,Tinside3.l,Pin3.l,z4.l,Thouse.l,Pin4.l


* Vragen:
*        Efficientie goed zo? lijkt te makkelijk
*        Waar kunnen we echte waarden v batterij vinden? want SOC is niet gewoon een procent. (dan zou het even goed gewoon een integer value kunnen zijn)
*        lege categorie mogelijk?

* eenheden enzovoorts doen kloppen!! pas op, vermogens zijn per kwartier, niet per seconde.
* Marginale kost x.m kunnen we misschien op het einde gebruiken om aan
* gebruikers te laten zien wat ze gewonnen hebben. Notatie vb Display x.l, x.m;

* randpunten van qevolt? (met (t+1) of (t-1) ..)
* Misschien soort van manual schrijven om apparaten bij cat2 toe te voegen? want niet zo makkelijk als anderen.
* We kunnen een airco installeren om het volledig te doen kloppen. Anders geen scenario als Toutside > Thouse. (pas op:Qout zal dan negatief zijn)
