$onempty
$GDXIN %SupplyDataFileName%
$eolcom #
sets
i house
t tijd ;
alias(t,ta);
$LOAD i t


Scalars
deltaT ��n tijdsstap /900/
* Cat 2
dd duration cycle dryer /3/
wmd duration cycle washing machine  /3/
dwd  duration cycle dishwasher  /4/
* Cat 4
mCp4 heat capacity cat4 /540000/
UAhouse heat transmission coefficient /150/
COP4 Coefficient of performance /3/

Eff efficiency of electricity transmission /0.90/;

Parameters
* Supply
Sres(t) Supply local RES

* Categorie 1
dcat1(t,i) demand category 1

* Categorie 2
* We suppose that every washing machine has the same cycle.
* The same for the dryers and dishwashers
dc(ta) demand-cycle dryer
nbcd(i) number of dryer cycles
tstartd(t,i) possible start times

wmc(ta) demand-cycle washing machine
nbcwm(i) number of washing machine cycles
tstartwm(t,i) possible start times

* let op: final start = tijd die gebruiker ingeeft min lengte cyclus
dwc(ta) demand-cycle dishwasher
nbcdw(i) number of dishwasher cycles
tstartdw(t,i) possible start times

*Categorie 3
* P(k) was 200 bij het testen
P(i) power cat3
* UA3/mCp3 is equal to 5.56E-5
UA3(i)
* COP3/mCp3 is equal to 1.68E-5
COP3(i) Coefficient of performance
mCp3(i) heat capacity cat3
Tmin3(t,i) minimum Temperature
Tmax3(t,i) maximum Temperature

*Categorie 4
Pmax(i) maximum vermogen van apparaat
Tmin4(t,i) minimum temperatuur
Tmax4(t,i) maximum temperatuur

* Categorie 5
SOC_wanted(t,i) SOC wanted by the user
Pdr(t,i) driving power of EV
xdr(t,i) driving EV
PmaxEV(i) maximum power of charging
SOCmax(i) maximum SOC
* SOC in procent nu: normaal totale energieinhoud?
* een standard tesla model S heeft inhoud batterij van 60 kWh
* en maximaal laadvermogen 225 kW

* Algemeen
Toutside(t) buitenhuistemperatuur
price(t) prijs elektriciteit;

$LOAD Sres dcat1 dc nbcd tstartd wmc nbcwm tstartwm dwc nbcdw tstartdw
$LOAD P UA3 COP3 mCp3 Tmin3 Tmax3 Pmax Tmin4 Tmax4 SOC_wanted Pdr xdr
$LOAD PmaxEV SOCmax Toutside price

dd = card(dc);
wmd = card(wmc);
dwd = card(dwc);

Variables
z1 cost
z2 cost
zh cost
zi cost
zj cost
z3 cost
z4 cost
z5 cost
z cost
xch charging or discharging
Sgrid(t,i) consumtion grid;

Binary variables
zfr(t) toestand cat3 aan-uit
xch(t,i)  charging
xdch(t,i) discharging;

Positive variables
*wat kan mijn huis beslissen
Pd(t,i) power of dryer
tid(t,i) starting time cycle dryer
Pwm(t,i) power of washing machine
tiwm(t,i) starting time cycle wasmachine
Pdw(t,i) power of dishwasher
tidw(t,i) starting time cycle dishwasher
Pin3(t,i) vermogen cat3
Qin3(t,i) Heat going in
Qout3(t,i) Heat going out
Tinside3(t,i) Temperature in the freezer

Pin4(t,i) vermogen cat4
Pconv(t) Energie uit het net
Qin4(t,i) Heat going in
Qout4(t,i) Heat going out
Curt(t) Curtailment
Thouse(t,i) binnenhuistemperatuur
SOC(t,i) State of charge Electric Vehicle (EV)
SOCrel(t,i) SOC relative to total capacity
Ptot(t,i) total power;


Equations
* Categorie 1
qobj1 objective function
* Categorie 2
*   Dryer
qPd(t,i) power equation of dryer
qd(t,i) check the number of dryers
qdc check if one dryer does exactly one cycle
qtd(t,i) toegelaten starttijd
qzh cost dryer
*   Washing machine
qPwm(t,i) power equation of washing machine
qwm(t,i) check the number of washing machines
qwmc check if one washing machine does exactly one cycle
qtwm(t,i) toegelaten starttijd
qzi cost washing machine
*   Dishwasher
qPdw(t,i) power equation of dishwasher
qdw(t,i) check the number of dishwashers
qdwc check if one dishwasher does exactly one cycle
qtdw(t,i) toegelaten starttijd
qzj cost dishwasher
*   objective
qobj2 objective function

* Categorie 3
qTmin3(t,i) Minimum temperatuur
qTmax3(t,i) Maximum temperatuur
qevolt3(t,i) hoofdvgml 3
qQin3(t,i)
qQOut3(t,i)
qPin3(t,i)
qobj3
* Categorie 4
qTmin4(t,i) Minimum temperatuur
qTmax4(t,i) Maximum temperatuur
qevolt4(t,i) hoofdvgl
qQin4(t,i) warmte naar apparaat
qQout4(t,i) warmte naar buiten
qPin4(t,i) beperking op vermogen
qobj4
* Categorie 5
qsituation(t,i) situation of EV
*qSOCw(t,i) State of charge wanted
qSOCmax(t,i) maximum energy available
qevolt5(t,i) head equation
qobj5
qSOCstatus
* Totaal
qobj
qPtot(t,i)
qgrid(t,i)
;

*Categorie 1
qobj1.. z1 =e= sum((t,i),dcat1(t,i)*(price(t)/4))  ;
*Categorie 2
*  Dryer
qPd(t,i).. Pd(t,i) =e= sum(ta$dd,tid(t-(ord(ta)-1),i)*dc(ta));
qd(t,i).. sum(ta,tid(t-(ord(ta)-1),i)) =l= card(i);
qdc.. sum(i,sum(t$(ord(t)<card(t)-(dd-2)),tid(t,i))) =e= sum(i,nbcd(i));

*  Washing machine
qPwm(t,i).. Pwm(t,i) =e= sum(ta$wmd,tiwm(t-(ord(ta)-1),i)*wmc(ta));
qwm(t,i).. sum(ta,tiwm(t-(ord(ta)-1),i)) =l= card(i);
qwmc.. sum(i,sum(t$(ord(t)<card(t)-(wmd-2)),tiwm(t,i))) =e= sum(i,nbcwm(i));
*  Dishwasher
qPdw(t,i).. Pdw(t,i) =e= sum(ta$dwd,tidw(t-(ord(ta)-1),i)*dwc(ta));
qdw(t,i).. sum(ta,tidw(t-(ord(ta)-1),i)) =l= card(i);
qdwc.. sum(i,sum(t$(ord(t)<card(t)-(dwd-2)),tidw(t,i))) =e= sum(i,nbcdw(i));


qtd(t,i).. tid(t,i) =l= tstartd(t,i);
qtwm(t,i).. tiwm(t,i) =l= tstartwm(t,i);
qtdw(t,i).. tidw(t,i) =l= tstartdw(t,i);

*  objective
qzh .. zh =e= sum(i,sum((t),Pd(t,i)*(price(t)/4)));
qzi  .. zi =e= sum(i,sum((t),Pwm(t,i)*(price(t)/4))) ;
qzj ..  zj =e= sum(i,sum((t),Pdw(t,i)*(price(t)/4)))  ;
qobj2..  z2 =e= zh + zi+ zj;

*Categorie 3
qTmin3(t,i)..   Tinside3(t,i)=g=Tmin3(t,i);
qTmax3(t,i)..   Tinside3(t,i)=l=Tmax3(t,i);
qevolt3(t,i)$ (ord(t)<card(t)).. Tinside3(t+1,i) =e=
                 Tinside3(t,i) + (deltaT*(Qout3(t,i)-Qin3(t,i)))/mCp3(i);
qQin3(t,i).. Qin3(t,i)=e= COP3(i)*Pin3(t,i);
qQOut3(t,i).. Qout3(t,i) =e= UA3(i)*(Thouse(t,i)-Tinside3(t,i));
qPin3(t,i).. Pin3(t,i) =e= zfr(t)*P(i);
qobj3.. z3 =e= sum((t,i),Pin3(t,i)*(price(t)/4));

* Categorie 4
qTmin4(t,i).. Thouse(t,i)=g= Tmin4(t,i);
qTmax4(t,i).. Thouse(t,i)=l= Tmax4(t,i);
qQin4(t,i).. Qin4(t,i)=e= COP4*Pin4(t,i);
qQout4(t,i).. Qout4(t,i)=e= UAhouse*(Thouse(t,i)-Toutside(t)) ;
qevolt4(t,i)$ (ord(t)<card(t)).. Thouse(t++1,i) =e=
                 Thouse(t,i) + (deltaT*(Qin4(t,i)-Qout4(t,i)))/mCp4;
qPin4(t,i).. Pin4(t,i)=l=Pmax(i); # voor elke i, zijn er t vgln.
qobj4..   z4 =e= sum((t,i),Pin4(t,i)*(price(t)/4));

* Categorie 5

qsituation(t,i).. xdr(t,i)+xch(t,i)+xdch(t,i) =l= 1;
qSOCmax(t,i).. SOC(t,i) =l= SOCmax(i);
qevolt5(t,i)..  SOC(t+1,i) =e=
         SOC(t,i) +
         (deltaT*Eff*PmaxEV(i)*xch(t,i))-(deltaT*Eff*PmaxEV(i)*xdch(t,i))-(deltaT*Pdr(t,i)*xdr(t,i));
qobj5.. z5 =e= sum((t,i),PmaxEV(i)*xch(t,i)*price(t))-sum((t,i),PmaxEV(i)*xdch(t,i)*(price(t)/4000));
qSOCstatus(i).. SOC('96',i) =g= SOC('1',i);

* Totaal
qPtot(t,i).. Ptot(t,i) =e= dcat1(t,i)+ Pd(t,i) + Pwm(t,i) + Pdw(t,i) + Pin3(t,i) + Pin4(t,i)+ PmaxEV(i)*xch(t,i) - PmaxEV(i)*xdch(t,i);
qgrid(t,i).. Sgrid(t,i) =e= (dcat1(t,i)/4000)+ (Pd(t,i) + Pwm(t,i) + Pdw(t,i) + Pin3(t,i) + Pin4(t,i)+ PmaxEV(i)*xch(t,i) - PmaxEV(i)*xdch(t,i))/4000 - Sres(t)/4;
qobj.. z =e= sum((t,i),(price(t)*Sgrid(t,i)));


Model SEW /
qobj1,
qPd,qd,qdc,qtd,qzh
qPwm,qwm,qwmc,qtwm,qzi,
qPdw,qdw,qdwc,qtdw,qzj,
qobj2,
qTmin3,qTmax3,qevolt3,qQin3,qQOut3,qPin3,qobj3,
qTmin4,qTmax4,qQin4,qQout4,qevolt4,qPin4,qobj4,
qsituation,qSOCmax,qevolt5,qobj5, qSOCstatus
qobj, qPtot, qgrid
/;

*option limrow=100;
Solve SEW using mip minimizing z;