sets
t time /01*10/
n cat5 /EV/;

Scalars
deltaT een tijdsstap /900/
Eff efficiency of electricity transmission /0.90/;
* normale waarde efficiŽntie?

Table SOC_wanted(t,n) SOC wanted by the user
         EV
01       400
02       400
03       400
04       400
05       400
06       400
07       400
08       400
09       1600
10       1600           ;
*SOC_wanted altijd >= 20% van batterij inhoud

Table Pdr(t,n) driving power of EV
         EV
01       0.6
02       0.6
03       0
04       0
05       0
06       0
07       0
08       0
09       0
10       0           ;


Table xdr(t,n) driving EV
         EV
01       1
02       1
03       0
04       0
05       0
06       0
07       0
08       0
09       0
10       0;
Parameters
PmaxEV(n) maximum power of charging
/EV 3/
SOCmax(n) maximum SOC
/EV 2000/

price(t) prijs elektriciteit
/01 10
02 10
03 10
04 10
05 10
06 10
07 10
08 10
09 10
10 10/;

Variable
z5 cost
xch charging or discharging;

Positive variables
SOC(t,n) State of charge Electric Vehicle (EV)
SOCrel(t,n) SOC relative to total capacity
*leadSOC(t,n) State of charge one step further
;
*leadSOC.fx('01','EV') = 0.2;

Equations
*qcirc(t,n) circular equation
qrestr1(t,n) restriction on xch
qrestr2(t,n) restriction on xch
qsituation(t,n) situation of EV
qSOCmin(t,n) minimum energy available
qSOCmax(t,n) maximum energy available
qevolt5(t,n) head equation
qSOCw(t,n) State of charge of EV
qobj5
qSOCrel;

*qcirc(t,n).. leadSOC(t,n) =e= SOC(t++1,n);
qrestr1(t,n).. xch(t,n) =l= 1;
qrestr2(t,n).. xch(t,n) =g= -1;
qsituation(t,n).. xdr(t,n)+abs(xch(t,n)) =l= 1;
qSOCw(t,n).. SOC(t,n) =g= SOC_wanted(t,n);
qSOCmax(t,n).. SOC(t,n) =l= SOCmax(n);
qevolt5(t,n)..  SOC(t++1,n) =e=
         SOC(t,n) +
         (deltaT*Eff*PmaxEV(n)*xch(t,n))-(deltaT*Pdr(t,n)*xdr(t,n));
qobj5.. z5 =e= sum((t,n),PmaxEV(n)*xch(t,n)*price(t));
qSOCrel(t,n).. SOCrel(t,n) =e= SOC(t,n)/SOCmax(n);

Model cat5 /qrestr1,qrestr2,qsituation,qSOCw,qSOCmax,qevolt5,qobj5,qSOCrel/;

Solve cat5 using minlp minimizing z5;

display z5.l,SOC.l,xch.l,SOCrel.l;
