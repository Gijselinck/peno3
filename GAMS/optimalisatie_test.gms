$onempty
$GDXIN %SupplyDataFileName%
$eolcom #
sets

l cat4

scalars
mCp4 heat capacity cat4 /540000/
UAhouse heat transmission coefficient /150/
COP4 Coefficient of performance /3/
;


*Categorie 4
Pmax(l) maximum vermogen van apparaat
/heating 2000/
Tmin4(t) minimum temperatuur
Tmax4(t) maximum temperatuur





$LOAD Tmin4 Tmax4 



Variables
z4 cost
Positive variables:

Pin4(t,l) vermogen cat4
Pconv(t) Energie uit het net
Qin4(t,l) Heat going in
Qout4(t) Heat going out
Thouse(t) binnenhuistemperatuur
;


equations
* Categorie 4
qTmin4(t) Minimum temperatuur
qTmax4(t) Maximum temperatuur
qevolt4(t,l) hoofdvgl
qQin4(t,l) warmte naar apparaat
qQout4(t) warmte naar buiten
qPin4(t,l) beperking op vermogen
qobj4

;



* Categorie 4
qTmin4(t).. Thouse(t)=g= Tmin4(t);
qTmax4(t).. Thouse(t)=l= Tmax4(t);
qQin4(t,l).. Qin4(t,l)=e= COP4*Pin4(t,l);
qQout4(t).. Qout4(t)=e= UAhouse*(Thouse(t)-Toutside(t)) ;
qevolt4(t,l)$ (ord(t)<card(t)).. Thouse(t++1) =e=
                 Thouse(t) + (deltaT*(Qin4(t,l)-Qout4(t)))/mCp4;
qPin4(t,l).. Pin4(t,l)=l=Pmax(l); # voor elke l, zijn er t vgln.
qobj4..   z4 =e= sum((t,l),Pin4(t,l)*price(t));




* Totaal
qobj.. z =e= z4; # moet nog uitgebreid worden naar 1,2 en 5



Model SEW /


qTmin4,qTmax4,qQin4,qQout4,qevolt4,qPin4,qobj4
qobj
/;


Solve SEW using minlp minimizing z;

display z.l, Thouse.l

