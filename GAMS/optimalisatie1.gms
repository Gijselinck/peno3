$eolcom #

*Dit bestand staat achter op test-file!! tinside3(t,k)  (o.a.?)

sets
g cat1 /Tv, fornuis/
h catd /dryer/ # dryer
i catwm /washing_machine/
j catdw /dishwasher/
k cat3 /fridge /
l cat4 / heating/
n cat5 /EV/
*o cat6 /airco/?
t tijd /01*10/;
alias(t,ta);

* ALLES in nederlands of alles in engels?!
* denk eraan: Temperatuur in Kelvin!

Scalars
deltaT ��n tijdsstap /900/

dd duration cycle dryer
wmd duration cycle washing machine
dwd  duration cycle dishwasher

mCp4 heat capacity cat4 /540000/
UAhouse warmteoverdrachtcoe�ffici�nt /150/  # eng
COP4 Coefficient of performance /3/;

Table tstartd(t,h) possible start times
         dryer
01         0
02         0
03         0
04         0
05         0
06         0
07         0
08         0
09         0
10         0     ;
Table tstartwm(t,i) possible start times
        washing_machine
01               0
02               0
03               1
04               1
05               1
06               0
07               0
08               0
09               0
10               0      ;
Table tstartdw(t,j) possible start times
        dishwasher
01            0
02            0
03            0
04            0
05            1
06            1
07            1
08            1
09            1
10            0   ;
Parameters

* Supply
Sgrid(t) Supply grid
Sres(t) Supply local RES

* Categorie 1
dcat1(t,g) demand category 1

* Categorie 2
* We suppose that every washing machine has the same cycle.
* The same for the dryers and dishwashers
dc(ta) demand-cycle dryer
/01 00
 02 00
 03 00/
nbcd(h) number of dryer cycles
/dryer 00/
wmc(ta) demand-cycle washing machine
/01 23
 02 18
 03 6/
nbcwm(i) number of washing machine cycles
/washing_machine 1/
* let op: final start = tijd die gebruiker ingeeft min lengte cyclus
dwc(ta) demand-cycle dishwasher
/01 50
02 20
03 20
04 5/
nbcdw(j) number of dishwasher cycles
/dishwasher 1/


*Categorie 3
P(k) power
/fridge 200/
* UA3/mCp3 is equal to 5.56E-5
UA3(k)
/fridge 0.0000556/
* COP3/mCp3 is equal to 1.68E-5
COP3(k) Coefficient of performance
/fridge 0.0000167/
mCp3(k) heat capacity cat3
/fridge 1/
Tmin3(t) minimum Temperature
/01 270
02  270
03  270
04  270
05  270
06  270
07  270
08  270
09  270
10 270/
Tmax3(t) maximum Temperature
/01 278
02 276
03 276
04 278
05 278
06 278
07 278
08 278
09 278
10 278/


*Categorie 4
Pmax(l) maximum vermogen van apparaat
/heating 2000/
Tmin4(t) minimum temperatuur
/01 293
02 293
03 293
04 293
05 293
06 293
07 293
08 293
09 293
10 293/
Tmax4(t) maximum temperatuur
/01 297
02 297
03 297
04 303
05 303
06 300
07 297
08 297
09 297
10 297/;

* Categorie 5
Table SOC_wanted(t,n) SOC wanted by the user
         EV
01       0.2
02       0.2
03       0.2
04       0.3
05       0.4
06       0.5
07       0.6
08       0.7
09       0.8
10       0.8           ;

Table Pdr(t,n) driving power of EV
         EV
01       0.05
02       0.05
03       0
04       0
05       0
06       0
07       0
08       0
09       0
10       0           ;


Table xdr(t,n) driving EV
         EV
01       1
02       1
03       0
04       0
05       0
06       0
07       0
08       0
09       0
10       0;
Parameters
PmaxEV(n) maximum power of charging
/EV 0.2/
SOCmax(n) maximum SOC
/EV 1/
* SOC in procent nu: normaal totale energieinhoud ofzo?
SOCmin(n) minimum SOC
/EV 0.20/

* Algemeen
Toutside(t) buitenhuistemperatuur
/01 273
02 273
03 273
04 273
05 273
06 273
07 273
08 273
09 273
10 273/

price(t) prijs elektriciteit
/01 10
02 10
03 10
04 10
05 10
06 10
07 10
08 10
09 10
10 10/;

dd = card(wmc);
wmd = card(wmc);
dwd = card(dwc);

Variables
z1 cost
z2 cost
zh cost
zi cost
zj cost
z3 cost
z4 cost
z5 cost
z cost;

Binary variables
zfr(t) toestand cat3 aan-uit
xch(t,n) charging EV
xdch(t,n) discharging EV;
Positive variables
*wat kan mijn huis beslissen
Pd(t,h) power of dryer
tid(t,h) starting time cycle dryer
Pwm(t,i) power of washing machine
tiwm(t,i) starting time cycle wasmachine
Pdw(t,j) power of dishwasher
tidw(t,j) starting time cycle dishwasher
Pin3(t,k) vermogen cat3
Qin3(t,k) Heat going in
Qout3(t,k) Heat going out
Tinside3(t) Temperature in the freezer
* tinside ook per vriezer (t,k) (?)

Pin4(t,l) vermogen cat4
Pconv(t) Energie uit het net
Qin4(t,l) Heat going in
Qout4(t) Heat going out
Curt(t) Curtailment
Thouse(t) binnenhuistemperatuur
SOC(t,n) State of charge Electric Vehicle (EV)
Pch(t,n) charging power of EV
Pdch(t,n) discharging power of EV;
* Laatste drie verplaatst van param naar var. (?)
* Is dch gewoon als ergens anders in huis stroom nodig is ofzo?
*Thouse.fx('01')=290;
*Thouse.fx('02')=290;
*Thouse.fx('03')=290;
*Thouse.fx('04')=290;
*Thouse.fx('05')=290;
*Thouse.fx('06')=290;
*Thouse.fx('07')=290;
*Thouse.fx('08')=290;
*Thouse.fx('09')=293;
*Thouse.fx('10')=293;
Tinside3.fx('01')=275;



Equations
* Categorie 1
qobj1 objective function
* Categorie 2
*   Dryer
qPd(t,h) power equation of dryer
qd(t,h) check the number of dryers
qdc check if one dryer does exactly one cycle
qtd(t,h) toegelaten starttijd
qzh cost dryer
*   Washing machine
qPwm(t,i) power equation of washing machine
qwm(t,i) check the number of washing machines
qwmc check if one washing machine does exactly one cycle
qtwm(t,i) toegelaten starttijd
qzi cost washing machine
*   Dishwasher
qPdw(t,j) power equation of dishwasher
qdw(t,j) check the number of dishwashers
qdwc check if one dishwasher does exactly one cycle
qtdw(t,j) toegelaten starttijd
qzj cost dishwasher

*   objective
qobj2 objective function

* Categorie 3
qTmin3(t) Minimum temperature
qTmax3(t) Maximum temperature
qevolt3(t,k) hoofdvgl 3
qQin3(t,k)
qQOut3(t,k)
qPin3(t,k)
qobj3
* Categorie 4
qTmin4(t) Minimum temperatuur
qTmax4(t) Maximum temperatuur
qevolt4(t,l) hoofdvgl
qQin4(t,l) warmte naar apparaat
qQout4(t) warmte naar buiten
qPin4(t,l) beperking op vermogen
qobj4
* Categorie 5
qsituation(t,n) situation of EV
qSOCmin(t,n) minimum energy available
qSOCmax(t,n) maximum energy available
qevolt5(t,n) head equation
qPch(t,n)  maximum power of charging
qPdch(t,n) maximum power of discharging
qSOC(t,n) State of charge of EV
qobj5
* Totaal
qobj
;

*Categorie 1
qobj1.. z1 =e= sum((t,g),dcat1(t,g)*price(t))  ;
*Categorie 2
*  Dryer
qPd(t,h).. Pd(t,h) =e= sum(ta$dd,tid(t-(ord(ta)-1),h)*dc(ta));
qd(t,h).. sum(ta,tid(t-(ord(ta)-1),h)) =l= card(h);
* 'o' aan rechterkant om infeasible weg te doen (uiteindelijk vanuit python automatiseren)
qdc.. sum(h,sum(t$(ord(t)<card(t)-(dd-2)),tid(t,h))) =e= sum(h,nbcd(h));
* RHS veranderd van card(h) naar 'o'

*  Washing machine
qPwm(t,i).. Pwm(t,i) =e= sum(ta$wmd,tiwm(t-(ord(ta)-1),i)*wmc(ta));
qwm(t,i).. sum(ta,tiwm(t-(ord(ta)-1),i)) =l= card(i); # 1 = aantal wasmachines
qwmc.. sum(i,sum(t$(ord(t)<card(t)-(wmd-2)),tiwm(t,i))) =e= sum(i,nbcwm(i)); # aantal cycli die moeten worden uitgevoerd.
*  Dishwasher
qPdw(t,j).. Pdw(t,j) =e= sum(ta$dwd,tidw(t-(ord(ta)-1),j)*dwc(ta));
qdw(t,j).. sum(ta,tidw(t-(ord(ta)-1),j)) =l= card(j); # 1 = aantal wasmachines
qdwc.. sum(j,sum(t$(ord(t)<card(t)-(dwd-2)),tidw(t,j))) =e= sum(j,nbcdw(j)); # aantal cycli die moeten worden uitgevoerd.


qtd(t,h).. tid(t,h) =l= tstartd(t,h);
qtwm(t,i).. tiwm(t,i) =l= tstartwm(t,i);
qtdw(t,j).. tidw(t,j) =l= tstartdw(t,j);

*  objective
qzh .. zh =e= sum(h,sum((t),Pd(t,h)*price(t)));
qzi  .. zi =e= sum(i,sum((t),Pwm(t,i)*price(t))) ;
qzj ..  zj =e= sum(j,sum((t),Pdw(t,j)*price(t)))  ;
qobj2..  z2 =e= zh + zi+ zj;

*Categorie 3
qTmin3(t)..   Tinside3(t)=g=Tmin3(t);
qTmax3(t)..   Tinside3(t)=l=Tmax3(t);
qevolt3(t,k)$ (ord(t)<card(t)).. Tinside3(t+1) =e= Tinside3(t) + (deltaT*(Qout3(t,k)-Qin3(t,k)))/mCp3(k);
* eigenlijk zou Pin3(t) negatief moeten zijn. Dus daar moeten we rekening mee houden.
qQin3(t,k).. Qin3(t,k)=e= COP3(k)*Pin3(t,k);
qQOut3(t,k).. Qout3(t,k) =e= UA3(k)*(Thouse(t)-Tinside3(t));
qPin3(t,k).. Pin3(t,k) =e= zfr(t)*P(k);
qobj3.. z3 =e= sum((t,k),Pin3(t,k)*price(t));

* Categorie 4
qTmin4(t).. Thouse(t)=g= Tmin4(t);
qTmax4(t).. Thouse(t)=l= Tmax4(t);
qQin4(t,l).. Qin4(t,l)=e= COP4*Pin4(t,l);
qQout4(t).. Qout4(t)=e= UAhouse*(Thouse(t)-Toutside(t)) ;
qevolt4(t,l)$ (ord(t)<card(t)).. Thouse(t+1) =e= Thouse(t) + (deltaT*(Qin4(t,l)-Qout4(t)))/mCp4;
qPin4(t,l).. Pin4(t,l)=l=Pmax(l); # voor elke l, zijn er t vgln.
qobj4..   z4 =e= sum((t,l),Pin4(t,l)*price(t));

*categorie 5
qsituation(t,n).. xdr(t,n)+xch(t,n)+xdch(t,n)=l= 1;
qSOCmin(t,n).. SOC(t,n) =g= SOCmin(n);
qSOCmax(t,n).. SOC(t,n) =l= SOCmax(n);
qevolt5(t,n)$(ord(t)<card(t)).. SOC(t+1,n)=e= SOC(t,n) + (deltaT*Pch(t,n)*xch(t,n))- (deltaT*Pdch(t,n)*xdch(t,n))-(deltaT*Pdr(t,n)*xdr(t,n));
qPch(t,n).. Pch(t,n)=e= PmaxEV(n); # nu =e= ipv =l= want non linear werkte niet
qPdch(t,n).. Pdch(t,n)=e= PmaxEV(n);  # nu =e= ipv =l= want non linear werkte niet
qSOC(t,n).. SOC(t,n)=g=SOC_wanted(t,n);
qobj5.. z5 =e= sum((t,n),xch(t,n)*price(t));
* als 5 testen tijdsstap op 5 of een echte energieinhoud batterij gerbuiken!
* Totaal
qobj.. z =e= z2+z4+z3; # moet nog uitgebreid worden naar 1,2 en 5


Model SEW /
*qobj1,
qPd,qd,qdc,qtd,qzh
qPwm,qwm,qwmc,qtwm,qzi,
qPdw,qdw,qdwc,qtdw,qzj,
qobj2,
qTmin3,qTmax3,qevolt3,qQin3,qQOut3,qPin3,qobj3,
qTmin4,qTmax4,qQin4,qQout4,qevolt4,qPin4,qobj4
*qsituation,qPdch,qPch,qSOCmin,qSOCmax,qevolt5,qSOC,qobj5,
qobj
/;


Solve SEW using mip minimizing z;
* Ik denk non-linear door Pch*xch en Pdch*xdch (2 variabelen maal elkaar)

*Problemen met minimaliseren:
* Iets met de getallen misschien waardoor het gewoon niet kan uitkomen.
* xch is 0.02?? zou niet mogen..

display zi.l,zj.l,z2.l,z3.l,z4.l,Pd.l,Pwm.l,Pdw.l,Tinside3.l,Pin3.l,Thouse.l,Pin4.l;
* Vragen:
*        Waar kunnen we echte waarden v batterij vinden? want SOC is niet gewoon een procent. (dan zou het even goed gewoon een integer value kunnen zijn)
*        lege categorie mogelijk?


* Marginale kost x.m kunnen we misschien op het einde gebruiken om aan
* gebruikers te laten zien wat ze gewonnen hebben. Notatie vb Display x.l, x.m;

* randpunten van qevolt? (met (t+1) of (t-1) ..)
* Misschien soort van manual schrijven om apparaten bij cat2 toe te voegen? want niet zo makkelijk als anderen.
* We kunnen een airco installeren om het volledig te doen kloppen. Anders geen scenario als Toutside > Thouse. (pas op:Qout zal dan negatief zijn)
