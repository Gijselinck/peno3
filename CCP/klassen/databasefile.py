DROP TABLE demandcat1;
CREATE TABLE demandcat1 (name TEXT, value float, PRIMARY KEY(name));
INSERT INTO demandcat1 VALUES ('SEATTLE', 350);
INSERT INTO demandcat1 VALUES ('SAN-DIEGO', 600);

SELECT * FROM supply;
DROP TABLE demand;
CREATE TABLE demand (name TEXT, value float, PRIMARY KEY(name));
INSERT INTO demand VALUES ('NEW-YORK', 325);
INSERT INTO demand VALUES ('CHICAGO', 300);
INSERT INTO demand VALUES ('TOPEKA', 275);

SELECT * FROM demand;
DROP TABLE distance;
CREATE TABLE distance (start TEXT, end TEXT, value float, PRIMARY KEY(start, end));
INSERT INTO distance VALUES ('SEATTLE', 'NEW-YORK', 2.5);
INSERT INTO distance VALUES ('SEATTLE', 'CHICAGO', 1.7);
INSERT INTO distance VALUES ('SEATTLE', 'TOPEKA', 1.8);
INSERT INTO distance VALUES ('SAN-DIEGO', 'NEW-YORK', 2.5);
INSERT INTO distance VALUES ('SAN-DIEGO', 'CHICAGO', 1.8);
INSERT INTO distance VALUES ('SAN-DIEGO', 'TOPEKA', 1.4);

SELECT * FROM distance;
DROP TABLE transport;
CREATE TABLE transport (start TEXT, end TEXT, value float, timestamp TEXT,
PRIMARY KEY(start, end, timestamp));
