class CommException(Exception):
    """Exceptie voor Communicatie met Arduino CCU"""

    def __init__(self, error_str = None, err_val = 0):
        
        if error_str == None:
            self.err_val = err_val
        elif error_str.isdigit():
            self.err_val = int(error_str)
        else:
            self.err_val = -1

    def __str__(self):
        repr_str = "Arduino Communication Error, Code " + str(self.err_val) + ": " 

        #voeg hier code's toe
        if self.err_val == 0 or self.err_val == -1:
            repr_str += "Onbekende fout"
        elif self.err_val == 1:
            repr_str += "Arduino response timeout"
        elif self.err_val == 2:
            repr_str += "SEW response timeout"
        elif self.err_val == 3:
            repr_str += "Invalid Arduino response"
        elif self.err_val == 4:
            repr_str += "Setup Failed"
        elif self.err_val == 5:
            repr_str += "Invalid CCU message received on Arduino"
       
        return repr_str
        


