#from SEW import SEW
import Communication
from SEW import *
from db_login import *
from optimalisatie import runGams, loadExcelSheetsToDb
import time
import logging
from Toestel import *
#voor logging: Stel het formaat in
logging.basicConfig(format='%(asctime)s [%(levelname)s]: %(message)s', datefmt='%d/%m/%Y %H:%M:%S')

class CCU(object):
    def __init__(self, step_duration):
        # Discrete Tijdstap waarop planning is gemaakt in seconden
        self._step_duration = step_duration

        # Lijst van alle SEW initialiseren
        self._sew_lst = []

        # Geef een communicatie instantie
        self._comm = Communication(promptPortName())

    def getComm(self):
        return self._comm

        # voor multiple inheritance
        #return super(CCU, self).__init__()

    def addHouse(self, house):
        assert(isinstance(house, SEW))
        wijknummer = house.getWijknummer()
        for sew in self._sew_lst:
            if sew._wijknummer == wijknummer:
                raise ValueError("Er is al een een SEW met dit wijknummer.\n Wijknummer moet Uniek")
        self._sew_lst.append(house)

    def getHouseWith(self, wijknummer):
        # sew = SEW(0, self) #default SEW indicating that the asked SEW was not found TODO aanpassen
        for sew in self._sew_lst:
            if sew._wijknummer == wijknummer:
                return sew
        return sew

    def optimaliseer(self):
        #zorg dat alle voorwaarden om GAMS op te roepen
        #voldaan zijn, roep GAMS op en slaag op in database
        runGams()
        loadExcelSheetsToDb()

    # initialiseer sew's op basis van de database
    def initialiseerVanDB(self, wijknummer_to_connect):
        #fetch data from DB
        querySEW        = "SELECT wijknr FROM sew " \
                          "WHERE wijknr=" + str(wijknummer_to_connect) + ";" # verwijder deze lijn om alle huizen
                                                                        # in de database te importeren in de CCP
        queryToestellen = "SELECT id, wijknr, naam, verbruik " \
                          "FROM toestel;"
        queryHebs       = "SELECT id, wijknr, naam, productie " \
                          "FROM heb;"
        querySensors    = "SELECT id, wijknr, naam " \
                          "FROM sensor;"
        cur.execute(querySEW)
        sews = cur.fetchall()
        cur.execute(queryToestellen)
        toestellen = cur.fetchall()
        cur.execute(queryHebs)
        hebs = cur.fetchall()
        cur.execute(querySensors)
        sensors = cur.fetchall()

        for sew in sews:
            sew_to_add = SEW(sew['wijknr'], self)
            self.addHouse(sew_to_add)

        for toestel in toestellen:
            wijknummer = toestel['wijknr']
            id = toestel['id']
            naam = toestel['naam']
            verbruik = toestel['verbruik']
            sew = self.getHouseWith(wijknummer)

            remove = string.punctuation + string.whitespace
            gen_name  = naam.translate(None, remove).lower()
            
            if gen_naam == "ijskast" or gen_naam == "koelkast":
                toestel = Ijskast(id, sew, self._comm,verbruik)
            elif gen_naam == "ev" or gen_naam == "evoertuig":
                toestel = EV(id, sew, self._comm, verbruik)
            else:
                toestel = Toestel(id, sew, self._comm, naam, verbruik)
            sew.addAsToestel(toestel)

        for heb in hebs:
            wijknummer = heb['wijknr']
            id = heb['id']
            naam = heb['naam']
            productie = heb['productie']
            sew = self.getHouseWith(wijknummer)
            heb = HEB(id, sew, self._comm, naam, productie)
            sew.addAsHEB(heb)

        for sensor in sensors:
            wijknummer = sensor['wijknr']
            id = sensor['id']
            naam = sensor['naam']
            sew = self.getHouseWith(wijknummer)
            sensor = Sensor(id, self._comm, naam)
            sew.addAsSensor(sensor)

    def simuleer(self):
        max_minuten = 24*60
        current_minuten = 0
        tijdsstap = 0
        cur.execute("DELETE FROM klok")
        cur.execute("INSERT INTO klok (dt) "
                    "VALUES (" + str(self._step_duration) + ");")
        conn.commit()
        while(current_minuten < max_minuten):
            uur = int(current_minuten / 60)
            min = current_minuten % 60
            tijd = str(uur) + ":" + str(min)
            print tijd
            t0 = time.clock()*1000 #s
            self.updateToestellen() #TODO uncomment deze twee lijnen
            # self.updateSensoren()
            query = "UPDATE klok SET tijdsstap = " + str(tijdsstap) + ",  tijd = '" + tijd + "';"
            cur.execute(query)
            conn.commit()
            t1 = time.clock()*1000 #s
            waiting = self._step_duration - (t1 - t0)
            # print waiting
            if (waiting > 0):
                time.sleep(waiting)
            current_minuten += 15
            tijdsstap += 1
    # update toestellen van het aangesloten huis (uitbreidbaar)
    def updateToestellen(self):
        sew = self.getHouseWith(21)
        query = "SELECT id, naam, verbruik " \
                "FROM toestel " \
                "WHERE wijknr='" + str(sew.getWijknummer()) + "';"
        cur.execute(query)
        toestellen = cur.fetchall()
        for dbtoestel in toestellen:
            verbruik = dbtoestel['verbruik']
            naam = dbtoestel['naam']
            toestel = sew.getToestelWithNaam(naam)
            if (toestel.getVerbruik() != verbruik):
                toestel.setVerbruik(verbruik)
                print "updateToestellen ", toestel._naam
                toestel.sendStateToSew()
        conn.commit()

    # update sensoren van het aangesloten huis (uitbreidbaar)
    def updateSensoren(self):
        sew = self.getHouseWith(1)
        sew.updateSensorWaarden()
        sew.uploadSensorReadingsToDb()
        pass