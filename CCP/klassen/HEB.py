import Communication
from CommException import CommException

__author__ = 'Tom Gijselinck <tomgijselinck@gmail.com'

class HEB:
    def __init__(self, id, sew_instance, communication_instance, naam = 'none', energieproductie = 0):
        assert(isinstance(id, int))
        assert(isinstance(naam, str))
        assert (isinstance(energieproductie, float) or isinstance(energieproductie, int))
        self._id = id
        self._naam = naam
        self._energieproductie = energieproductie
        self._sew = sew_instance
        self._comm = communication_instance

    def getId(self):
        return self._id

    def getNaam(self):
        return self._naam

    def getEnergieproductie(self):
        return self._energieproductie

    def _setEnergieproductie(self, energieproductie):
        assert(isinstance(energieproductie, int) or isinstance(energieproductie, float))
        self._energieproductie = energieproductie

    def sendProductieToSew(self):
        """ Stuur de productie door naar de actuator op de SEW die de HEB voorstelt"""
        remove = string.punctuation + string.whitespace
        general_name  = self._naam.translate(None, remove).lower()
        if general_name == "wind":
            # energieproductie is fractie tss 0 & 1
            self._comm.setToestel(self._sew.getWijknummer(), self._id, int(self._energieproductie * 255) )

        # naargelang de naam bepalen welke waarde we moeten doorsturen
        # eerst naam
        