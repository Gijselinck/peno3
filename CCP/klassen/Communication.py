# Seriele poort communicatie, pySerial moet geinstalleerd zijn:
import serial
# om platform te bepalen voor de poort
import platform 
# om te kunnen wachten
import time

from CommException import CommException

# Costanten
DEFAULT_WAIT_TIME = 4# in seconden

# Vorm van het protocol:
# Eerst wordt er 1 byte (als char) doorgestuurd die aanduid hoeveel bytes er van het bericht
# nog moeten volgen, aan de hand daarvan wordt de rest dan uitgelezen.
# 
# lengte - SEW_id - Toestel_id - S/G (Set of Get) - Waarde - Type
#
# Er wordt eenvoudigweg een string gemaakt van de waarde
# i.p.v. als bv een int van 2 bytes om te zetten naar 2 chars. dit is eenvoudiger
# en aangezien deze communicatie via kabel verloopt is er minder ruis en geen berperking
# op lengte bericht
# Dit lost zelfs een conversieprobleem op,  nl. dat type (bv int) op arduino en in Python 
# niet dezelfde lengte hebben.

# TODO:
#  - ander waarde dan int doorsturen/ontvangen (arduino rf_wrapper ondersteunt ook array's)?

class Communication(object):
    """ Klasse die de communicatie met de Arduino van de CCU behandelt"""

    def __init__(self, port_name, comm_wait_t = DEFAULT_WAIT_TIME):
        """
        Parameters:
        - port_name         : (str) Naam van de poort (gebruik promtp_port_name() om gebruiker te vragen)
        - response_wait_t   : (float ,opt) Tijd (seconden) dat moet gewachten worden tot antwoord bij verkrijgen sensor waarden 
        """
        # timeout for read
        self._wait_t = comm_wait_t

        # Open serial port
        self._ser = serial.Serial(port_name, timeout = DEFAULT_WAIT_TIME)

        
        # wait on arduiono "Setup Done" message
        start_time = time.time()
        loop_time = time.time()
        while ( self._ser.inWaiting() < 10):
            if( abs(loop_time - start_time) > self._wait_t*5):
                raise CommException(err_val = 1)

        # Check "Setup Done" msg
        setup_msg = "Setup Done"
        rec_msg = self._ser.read(len(setup_msg))
        if setup_msg != rec_msg:
            raise CommException(err_val = 4)

        print "[Communication] ", "Setup Done"
        #return super(Communication, self).__init__(*args, **kwargs)

    def __del__(self):
        # Sluit de seriele poort zodat deze erna nog beschikbaar is
        self._ser.close()

    def setToestel( self, wijknummer, toestel_id, waarde):
        """ Stuur waarde voor een Toestel van een SEW naar de arduino aan de CCU
        
        Parameters:
        - wijknummer    : (int, positief) wijknummer van SEW waar toestel dat geconfigureerd moeten worden tot behoort
        - toestel_id    : (int, positief) id van het toestel dat geconfigureert moet worden binnen de de SEW
        - waarde        : (int) waarde waarop het toestel moet worden geconfigureerd 
        """
        # maak de ontvangstbuffer leeg voor indien er nog (foute) berichten in zouden zitten
        self._ser.flushInput()
        # print wijknummer, toestel_id #>TODO del

        # construeer data deel van het bericht
        msg = chr(wijknummer) + chr(toestel_id) + "S" + str(waarde)
        
        # Hetvolgende is nog niet geimplementeerd op arduino, we doen alles in int
        ## voeg type data vanachter toe
        #if isinstance(waarde,str):
        #    msg += "1"
        #elif isinstance(waarde,int):
        #    msg += "2"
        #elif isinstance(waarde,float):
        #    msg += "3"
        #else:
        #    msg += "0"
        
        # voeg eerste byte toe die lengt van data-deel aangeeft:
        msg = chr(len(msg)) + msg

        # schrijf het weg
        self._ser.write(msg)
        self._ser.flush()

        # bekijk of er nog een error terug komt, een 'okay' in de vorm lengte-'O'-0  
        # wordt teruggestuurd indien transmissie gelukt is.

        # lees eerste byte/char om lengte van antwoord te bekomen
        first = self._ser.read(1)
        if len(first) == 0:
            raise CommException(err_val = 1)
        length = ord(first)
        if length < 2:
            raise CommException(err_val = 3)

        # antwoord:
        response = self._ser.read(length)
        resp_type = response[0]
        if resp_type == 'E':
            raise CommException(response[1:])
        
    def getSensorWaarde(self, wijknummer, sensor_id):
        """ Verkrijg een waarde van een sensor van een SEW. Stuurt een bericht to aavraag naar arduino aan CCU,
        wacht op antwoord, en returnt dit antwoord.

        Returnt         : int, de waarde van de 

        Paramaters:
        - wijknummer    : (int, positief) wijknummer van SEW waar sensor data opgevraagd moet worden
        - toestel_id    : (int, positief) id van sensor waar data van verkregen wordt
        """
        # maak de ontvangstbuffer leeg voor indien er nog (foute) berichten in zouden zitten
        self._ser.flushInput()
             
        # vorm bericht
        msg = chr(wijknummer) + chr(sensor_id) + "G"

        # voeg eerste byte toe die lengt van data-deel aangeeft:
        msg = chr(len(msg)) + msg

        #stuur bericht
        self._ser.write(msg)
        self._ser.flush()

        # lees eerste byte/char om lengte van antwoord te bekomen
        first = self._ser.read(1)
        if len(first) == 0:
            raise CommException(err_val = 1)
        length = ord(first)
        if length < 2:
            raise CommException(err_val = 3)

        # antwoord:
        response = self._ser.read(length)

        resp_type = response[0]
        if resp_type == "O": 
            # het is gewoon een antwoord
            return int(response[1:])
        elif resp_type == "E":
            # Error van Arduino aan CCP
            raise CommException(response[1:])
        else:
            # onbekende error
            raise CommException()

       


def promptPortName():
    """ Query's the user for a port name, takes in account platform, returns the port name"""
    platform_sys = platform.system()

    if platform_sys == "Linux":

        print("System is Linux, choosing preconfigured port name")
        # Todo: testen op Linux, zien welke namen het krijgt als er meerdere zijn
        port_name = "/dev/ttyACM0"

    elif platform_sys == "Windows":
        print("System is Windows, port named COMx, wiht x a positive integer")

        x = None 
        while not isinstance(x, str) or not x.isdigit() or not int(x) > 0:
            x = raw_input("Please provide x: ")
        port_name = "COM" + x

    else:
        raise Exception("Unknown platform")

    return port_name