__author__ = 'Tom Gijselinck <tomgijselinck@gmail.com'

from db_login import *
from Sensor import *
from Toestel import *
from HEB import *
from CCU import *
from Communication import *
from CommException import CommException
import logging

class SEW(object):
    def __init__(self, wijknummer, ccu):
        assert(isinstance(wijknummer, int))
        #assert(isinstance(ccu, CCU))
        self._wijknummer = wijknummer
        self._ccu = ccu

    def getWijknummer(self):
        return self._wijknummer

    def updateSensorWaarden(self): #TODO testen met echte sensors
        comm = self._ccu.getComm()
        for sensor in self._sensors:
            try:
                sensorwaarde = comm.getSensorWaarde(self._wijknummer, sensor.getId())
                # herschaal sensorwaarde van 0-255 naar 0-100
                sensorwaarde = int( sensorwaarde * 100 / 255 )
                sensor.setSensorwaarde(sensorwaarde)
            except CommException as exp:
                # log exceptie
                logging.warning(str(exp))

    def uploadSensorReadingsToDb(self):
        for sensor in self._sensors:
            id = sensor.getId()
            waarde = sensor.getSensorwaarde()
            query = "UPDATE sensor " \
                    "SET waarde=" + str(waarde) + " " \
                    "WHERE id=" + str(id) + ";"
            cur.execute(query)

    #here
    def checkDB(self):
        pass


    # RELATIONS
    # Sensors
    def addAsSensor(self, sensor):
        assert(isinstance(sensor, Sensor))
        self._sensors.append(sensor)

    def getSensor(self, sensor):
        assert(isinstance(sensor, Sensor))
        index = self._sensors.index(sensor)
        return self._sensors[index]

    def getSensorWithNaam(self, naam):
        assert(isinstance(naam, str))
        sensor = Sensor(0)
        for sensor in self._sensors:
            if sensor.getNaam() == naam:
                return sensor
        return sensor

    # Variable referencing the sensors of this SEW
    _sensors = []

    # Toestellen
    def addAsToestel(self, toestel):
        assert(isinstance(toestel, Toestel))
        self._toestellen.append(toestel)

    def getToestel(self, toestel):
        assert(isinstance(toestel, Toestel))
        index = self._toestellen.index(toestel)
        return self._toestellen[index]

    def getToestelWithNaam(self, naam):
        assert(isinstance(naam, str))
        # toestel = Toestel(0)
        for toestel in self._toestellen:
            if toestel.getNaam() == naam:
                return toestel
        return toestel

    def getToestellen(self):
        return self._toestellen

    # Variable referencing the toestellen of this SEW
    _toestellen = []

    # HEB's
    def addAsHEB(self, heb):
        assert(isinstance(heb, HEB))
        self._HEBs.append(heb)

    def getHEB(self, heb):
        assert(isinstance(heb, HEB))
        index = self._HEBs.index(heb)
        return self._HEBs[index]

    def getHebWithNaam(self, naam):
        assert(isinstance(naam, str))
        # heb = HEB(0)
        for heb in self._HEBs:
            if heb.getNaam() == naam:
                return heb
        return heb

    # Variable referencing the Sensoren of this SEW
    _HEBs = []

    #CCU
    def getCcu(self):
        return self._ccu
