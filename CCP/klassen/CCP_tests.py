from CCU import CCU
from Toestel import Toestel
from Communication import Communication, promptPortName
from CommException import CommException
import time

# tests voor CCP klassen

# Communicatie, get test met Arduino_Bounce Programma (vraag aan Maxime)
my_comm = Communication(promptPortName(),0.2)


# schijf al een antwoord op voorhand naar Arduino (die door Arduino_bounce alles letterlijk terugstuurt)
value = 67
msg = chr(1 + len(str(value)) ) + "O" + str(value)
my_comm._ser.write(msg)
my_comm._ser.flush()
response = my_comm.getSensorWaarde(3,8)
assert response == value

# test eens een error
# doe rommer weg:
my_comm._ser.read(100);


try:
    err_val = 2
    msg = "E" + str(err_val)
    msg = chr(len(msg)) + msg
    my_comm._ser.write(msg)
    my_comm._ser.flush()
    my_comm.getSensorWaarde(3,8)
    # als er geen error is, is er sowiso iets fout:
    assert False
except CommException as e:
    assert e.err_val == err_val
# schrijf nog eens iets
my_comm._ser.flushInput();
my_comm.setToestel(3,4,8)
print "This is sent to arduino whit setToestel(): ", my_comm._ser.read(50)

#Einde tests Communication Class
del my_comm

#CCU
test_ccu = CCU(13)
print type(test_ccu)

#Toestel

test_toestel = Toestel(13,3)
print test_toestel
test_toestel = Toestel(13,3)