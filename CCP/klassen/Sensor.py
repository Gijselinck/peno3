__author__ = 'Tom Gijselinck <tomgijselinck@gmail.com>, Maxime Feyerick'
from CommException import CommException
import logging

class Sensor:
    def __init__(self, id, communication_instance, naam = 'none'):
        assert(isinstance(id, int))
        assert(isinstance(naam, str))
        self._id = id
        self._naam = naam
        self._sensorwaarde = 0
        self._comm_inst = communication_instance
        self._status = 0 # is percentage: 0-100

    def getId(self):
        return self._id

    def getNaam(self):
        return self._naam

    def getSensorwaarde(self):
        return self._sensorwaarde

    def setSensorwaarde(self, waarde):
        assert(isinstance(waarde, int) or isinstance(waarde, float))
        self._sensorwaarde = waarde

    def getStatus(self):
        return self._status

    #def readSensorFromSew(self):
    #    "Returnt de status ( getStatus() ) ook "
    #    try:
    #        sew_data = self._comm_inst.getSensorWaarde(self._sew.GetWijknummer(), self._id)
    #        # Schaal het van (0,255) naar (0,100), type int
    #        sew_data = int ( sew_data * 100 /250 )
    #    except CommException as exp:
    #        # schrijf feedback naar console, voor de rest doet het gwn door
    #        logging.warning(str(exp))
        
    #    return self.getStatus()

    def setStatus(self, status):
        assert(isinstance(status, int))
        assert(status == 0 or status == 1)
        self._status = status
