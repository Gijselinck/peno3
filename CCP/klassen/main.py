__author__ = 'Tom Gijselinck <tomgijselinck@gmail.com'

# This file initializes the SEW application

import sys
from CCU import *
from db_login import cur
import time

query = "SELECT start_optimalisatie, start_simulatie, skip_optimalisatie " \
           "FROM user_interface;"
ccu = CCU(2) #1s TODO test of dit voldoende is

#@return    Returns whether the optimalisation has been done or not
def optimized():
    cur.execute(query)
    result = cur.fetchall()
    if result[0]['start_optimalisatie']:
        ccu.optimaliseer()
        return True
    elif result[0]['skip_optimalisatie']:
        return True
    else:
        return False

#@return    Returns whether to start the simulation or not, based on DB
def startSimulation():
    cur.execute(query)
    result = cur.fetchall()
    if result[0]['start_simulatie']:
        return True
    else:
        return False

def run():
    #Initialize SEW application
    print('Starting SEW application now...')
    #Instantiate all SEW's, toestellen, sensors and HEB's
    print('Creating smart energy houses ...')
    ccu.initialiseerVanDB(21) # huis met wijknummer 1 is aangesloten met arduinos
    #Apllication fully functional
    #Now the application is waiting for further instructions from the UI
    #meaning to start (or stop) a simulation.
    print('Wating for optimalisation'),
    i=0
    #wait until optimized
    while (not optimized()):
        #waiting animation
        i = (i+1) % 25
        if (i == 0):
            print "."
            sys.stdout.flush()
        else:
            print ".",
            sys.stdout.flush()
        time.sleep(1)
    print ""
    #wait until UI gives signal to start simulation
    i=0
    print('Waiting for UI'),
    while (not startSimulation()):
        #waiting animation
        i = (i+1) % 25
        if (i == 0):
            print "."
            sys.stdout.flush()
        else:
            print ".",
            sys.stdout.flush()
        time.sleep(1)
    print ""
    print "Starting simulation ..."
    ccu.simuleer()
    print "Simulation done!"


run()