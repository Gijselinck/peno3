__author__ = 'Tom Gijselinck <tomgijselinck@gmail.com'

from SEW import *
from HEB import *
from Sensor import *

# setup
sew1 = SEW(8)
windmolen = HEB(1, 'windmolen', 3500)
zonnepaneel = HEB(2, 'zonnepaneel', 1200)
lichtsensor = Sensor(1, 'lichtsensor')
sew1.addAsSensor(lichtsensor)
sew1.addAsHEB(windmolen)
sew1.addAsHEB(zonnepaneel)

# TESTS
print('Running tests now ...')

assert(sew1.getWijknummer()==8)
sensor = sew1.getSensor(lichtsensor)
assert(sensor.getNaam()=='lichtsensor')
assert(len(sew1._HEBs)==2)
heb = sew1.getHEB(windmolen)
assert(heb.getNaam()=='windmolen')

print('Finished')