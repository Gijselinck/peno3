__author__ = 'Tom Gijselinck <tomgijselinck@gmail.com'

from Sensor import *

# setup
sensor1 = Sensor(1, 'lichtsensor')

# TESTS
print('Running tests now ...')

assert(sensor1.getId()==1)
assert(sensor1.getNaam()=='lichtsensor')
assert(sensor1.getSensorwaarde()==0)
assert(sensor1.getStatus()==0)
sensor1.setSensorwaarde(50)
assert(sensor1.getSensorwaarde()==50)
sensor1.setStatus(1)
assert(sensor1.getStatus()==1)

print('Finished')