__author__ = 'Tom Gijselinck <tomgijselinck@gmail.com'

from HEB import *

# setup
heb1 = HEB(1, 'windmolen', 2600)

# TESTS
print('Running tests now ...')

assert(heb1.getId()==1)
assert(heb1.getNaam()=='windmolen')
assert(heb1.getEnergieproductie()==2600)
heb1._setEnergieproductie(3200)
assert(heb1.getEnergieproductie()==3200)

print('Finished')