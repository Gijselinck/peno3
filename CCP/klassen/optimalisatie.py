import os, time, glob
import gams
import pymysql

# CONNECTION TO DATABASE
from db_login import *

# IMPORT FUNCTIONS
# @return   Return a dictionary that maps housenumbers to temperature lists
def verwarming():
    # fetch results
    query = "SELECT iv.verwarming_id, iv.starttijd, iv.stoptijd, iv.max_temp, iv.min_temp, toestel.wijknr " \
            "FROM instelling_verwarming AS iv, toestel " \
            "WHERE iv.verwarming_id = toestel.id;"
    cur.execute(query)
    instellingen = cur.fetchall()

    queryHuisnummers = "SELECT DISTINCT wijknr AS huisnummer " \
                       "FROM sew;"
    cur.execute(queryHuisnummers)
    huisnummers = cur.fetchall()

    #defaults
    stdmin = 5 + 273
    stdmax = 30 + 273

    #alle_temperaturen = {}
    maxtemp = {}
    mintemp = {}

    #maak standaardlijsten aan
    for huisnummer in huisnummers:
        huisnummer = huisnummer['huisnummer']
        maxtemp['huis' + str(huisnummer)] = []
        mintemp['huis' + str(huisnummer)] = []
        for i in range(0, 96):
            maxtemp['huis' + str(huisnummer)].append(stdmax)
            mintemp['huis' + str(huisnummer)].append(stdmin)

    #pas instellingen toe op de standaardlijsten
    for instelling in instellingen:
        start = int (instelling['starttijd'].total_seconds()/900)
        stop = int (instelling['stoptijd'].total_seconds()/900)
        mintemper = instelling['min_temp']
        maxtemper = instelling['max_temp']
        ihuisnummer = instelling['wijknr']

        #mintemp en maxtemp instellen op basis van de instellingen
        if (start > stop):
            for time_step in range(start, 96):
                maxtemp['huis' + str(ihuisnummer)][time_step] = maxtemper
                mintemp['huis' + str(ihuisnummer)][time_step] = mintemper
            for time_step in range(0, stop):
                maxtemp['huis' + str(ihuisnummer)][time_step] = maxtemper
                mintemp['huis' + str(ihuisnummer)][time_step] = mintemper
        else:
            for time_step in range(start, stop):
                maxtemp['huis' + str(ihuisnummer)][time_step] = maxtemper
                mintemp['huis' + str(ihuisnummer)][time_step] = mintemper
    return maxtemp, mintemp

#@return    Return three dictionaries, wasmachine, vaatwas and droogkast,
#           that map housenumbers to lists
def cat2():
    #fetch results
    queryWasmachine = "SELECT toestel_id, starttijd, stoptijd, wijknr as huisnummer " \
                      "FROM instellingView " \
                      "WHERE naam='wasmachine';"
    queryVaatwas =    "SELECT toestel_id, starttijd, stoptijd, wijknr as huisnummer " \
                      "FROM instellingView " \
                      "WHERE naam='vaatwas';"
    queryHuisnummers = "SELECT DISTINCT wijknr AS huisnummer " \
                       "FROM sew;"
    cur.execute(queryWasmachine)
    instellingenWasmachine = cur.fetchall()
    cur.execute(queryVaatwas)
    instellingenVaatwas = cur.fetchall()
    cur.execute(queryHuisnummers)
    huisnummers = cur.fetchall()

    droogkast = []
    wasmachine = {}
    vaatwas = {}

    #defaults
    lengte_cyclus_wasmachine = 3
    lengte_cyclus_vaatwas = 4

    #maak standaardlijsten aan
    for huisnummer in huisnummers:
        huisnummer = huisnummer['huisnummer']
        wasmachine['huis' + str(huisnummer)] = []
        vaatwas['huis' + str(huisnummer)] = []
        for i in range(0,96):
            wasmachine['huis' + str(huisnummer)].append(0)
            vaatwas['huis' + str(huisnummer)].append(0)

    #lijst wasmachine
    for instelling in instellingenWasmachine:
        start = int (instelling['starttijd'].total_seconds()/900)
        stop = int (instelling['stoptijd'].total_seconds()/900 - lengte_cyclus_wasmachine)
        ihuisnummer = instelling['huisnummer']
        if (start > stop):
            for step in range(start, 96):
                wasmachine['huis' + str(ihuisnummer)][step] = 1
            for step in range(0, stop):
                wasmachine['huis' + str(ihuisnummer)][step] = 1
        else:
            for step in range(int (start), int (stop)+1):
                wasmachine['huis' + str(ihuisnummer)][step] = 1

    #lijst vaatwas
    for instelling in instellingenVaatwas:
        start = int (instelling['starttijd'].total_seconds()/900)
        stop = int (instelling['stoptijd'].total_seconds()/900 - lengte_cyclus_vaatwas)
        ihuisnummer = instelling['huisnummer']
        if (start > stop):
            for step in range(start, 96):
                vaatwas['huis' + str(ihuisnummer)][step] = 1
            for step in range(0, stop):
                vaatwas['huis' + str(ihuisnummer)][step] = 1
        else:
            for step in range(start, stop):
                vaatwas['huis' + str(ihuisnummer)][step] = 1

    #lijst droogkast
    for i in range(0,96):
        droogkast.append(0)

    return wasmachine, vaatwas, droogkast


def EV():
    #fetch results
    queryEV = "SELECT toestel_id, starttijd, stoptijd, wijknr as huisnummer " \
              "FROM instellingView " \
              "WHERE naam='e-voertuig';"
    queryHuisnummers = "SELECT DISTINCT wijknr AS huisnummer " \
                       "FROM sew;"
    cur.execute(queryEV)
    instellingenEv = cur.fetchall()
    cur.execute(queryHuisnummers)
    huisnummers = cur.fetchall()

    evoertuig = {}
    ev_driving_power = {}
    echarging = {}


    #maak standaardlijsten aan
    for huisnummer in huisnummers:
        huisnummer = huisnummer['huisnummer']
        evoertuig['huis' + str(huisnummer)] = []
        ev_driving_power['huis' + str(huisnummer)] = []
        echarging['huis' + str(huisnummer)] = []
        for i in range(0,96):
            evoertuig['huis' + str(huisnummer)].append(0)
            echarging['huis' + str(huisnummer)].append(1)
            ev_driving_power['huis' + str(huisnummer)].append(0)

    #pas instellingen toe op de standaardlijsten
    for instelling in instellingenEv:
        start = int (instelling['starttijd'].total_seconds()/900)
        stop = int (instelling['stoptijd'].total_seconds()/900)
        ihuisnummer = instelling['huisnummer']
        if (start > stop):
            for step in range(start, 96):
                evoertuig['huis' + str(ihuisnummer)][step] = 1
                echarging['huis' + str(ihuisnummer)][step] = 0
                ev_driving_power['huis' + str(ihuisnummer)][step] = 480
            for step in range(0, stop):
                evoertuig['huis' + str(ihuisnummer)][step] = 1
                echarging['huis' + str(ihuisnummer)][step] = 0
                ev_driving_power['huis' + str(ihuisnummer)][step] = 480
        else:
            for step in range(start, stop):
                evoertuig['huis' + str(ihuisnummer)][step] = 1
                echarging['huis' + str(ihuisnummer)][step] = 0
                ev_driving_power['huis' + str(ihuisnummer)][step] = 480

    return evoertuig, ev_driving_power, echarging


def getTimeFrom(timestep):
    assert(isinstance(timestep, int))
    tot_min = timestep*15
    uur = int(tot_min / 60.0)
    min = tot_min % 60
    return str(uur) + ":" + str(min)


def runGams():
    # 1. Setup Gams Workspace
    # ws = gams.GamsWorkspace(working_directory=os.getcwd(), debug=gams.DebugLevel.ShowLog)
    ws = gams.GamsWorkspace(working_directory=os.getcwd(), debug=gams.DebugLevel.ShowLog)
    print os.getcwd()
    # 2. Initialize Job
    job = ws.add_job_from_file('optimalisatie.gms')

    # 3. Initialize Options
    opt = ws.add_options()

    # 4. Initialize Database
    db = ws.add_database()
    opt.defines["SupplyDataFileName"] = db.name

    # 5. Define and add Sets, Parameters,...
    set_i = db.add_set('i', 1, 'house')
    set_t = db.add_set('t', 1, 'time')

    param_Sres = db.add_parameter_dc('Sres', [set_t], 'supply local RES')
    #param_Sgrid = db.add_parameter_dc('Sgrid', [set_t], 'Supply grid')

    param_dcat1 = db.add_parameter_dc('dcat1', [set_t, set_i], 'demand category 1')

    param_dc = db.add_parameter_dc('dc', [set_t], 'demand-cycle dryer')
    param_nbcd = db.add_parameter_dc('nbcd',[set_i], 'number of dryer cycles')
    param_tstartd = db.add_parameter_dc('tstartd',[set_t,set_i], 'possible start times')

    param_wmc = db.add_parameter_dc('wmc', [set_t], 'demand-cycle washing machine')
    param_nbcwm = db.add_parameter_dc('nbcwm', [set_i], 'number of washing machine cycles')
    param_tstartwm = db.add_parameter_dc('tstartwm', [set_t, set_i], 'possible start times')

    param_dwc = db.add_parameter_dc('dwc', [set_t], 'demand-cycle dishwasher')
    param_nbcdw= db.add_parameter_dc('nbcdw', [set_i], 'number of dishwasher cycles')
    param_tstartdw = db.add_parameter_dc('tstartdw',[set_t,set_i], 'possible start times')

    #P,UA3,COP3,mcp3 kunnen eventueel als scalar?? hangt af of mn verschillende toestellen in huizen wilt of aanneemt dat ze dezelefde zijn, geldt ook Pmax van cat4
    param_P = db.add_parameter_dc('P', [set_i], 'power cat3')
    param_UA3 = db.add_parameter_dc('UA3', [set_i], 'UA cat3')
    param_COP3 = db.add_parameter_dc('COP3', [set_i], 'coefficient of performance')
    param_mCp3 = db.add_parameter_dc('mCp3', [set_i], 'heat capacity cat3')
    param_Tmin3 = db.add_parameter_dc('Tmin3', [set_t,set_i], 'minimum Temperature')
    param_Tmax3 = db.add_parameter_dc('Tmax3', [set_t,set_i], 'maximum Temperature')

    param_Pmax = db.add_parameter_dc('Pmax', [set_i], 'maximum power of appliance')
    param_Tmin4 = db.add_parameter_dc('Tmin4', [set_t,set_i], 'minimum temperature')
    param_Tmax4 = db.add_parameter_dc('Tmax4', [set_t,set_i], 'maximum temperature')

    param_SOC_wanted = db.add_parameter_dc('SOC_wanted', [set_t,set_i], 'SOC wanted by the user')
    param_Pdr = db.add_parameter_dc('Pdr', [set_t,set_i], 'driving power of EV')
    param_xdr = db.add_parameter_dc('xdr', [set_t, set_i], 'driving EV')
    param_xch = db.add_parameter_dc('xch', [set_t, set_i], 'charging EV')
    param_PmaxEV = db.add_parameter_dc('PmaxEV', [set_i], 'maximum power of charging')
    param_SOCmax = db.add_parameter_dc('SOCmax', [set_i], 'maximum SOC')
    param_SOCmin = db.add_parameter_dc('SOCmin', [set_i], 'minimum SOC')

    param_Toutside= db.add_parameter_dc('Toutside', [set_t], 'buitenhuistemperatuur')
    param_price = db.add_parameter_dc('price', [set_t], 'price of energy')

    #6. Add values

    #DATABASE SEW
    maxtemp, mintemp = verwarming()
    wasmachine, vaatwas, droogkast = cat2()
    evoertuig, ev_driving_power, echarging = EV()
    #TODO voeg hier waarden in voor meerdere huizen (onderstaande code is niet up to date!)
    for i in range(1,96+1):
        set_t.add_record(str(i))
    #     param_Tmin4.add_record(str(i)).value = mintemp[i-1]
    #     param_Tmax4.add_record(str(i)).value = maxtemp[i-1]
    #     param_Toutside.add_record(str(i)).value = tempoutside[i-1]

    #EXCELLIJSTEN
    from xlrd import open_workbook,cellname
    book = open_workbook('simulatie_data.xls')

    sheet1 = book.sheet_by_index(0)
    sheet2 = book.sheet_by_index(1)
    sheet3 = book.sheet_by_index(2)
    sheet4 = book.sheet_by_index(3)
    sheet5 = book.sheet_by_index(4)
    sheet6 = book.sheet_by_index(5)
    sheet7 = book.sheet_by_index(6)
    sheet8 = book.sheet_by_index(7)

    #twee huizen inrekenen nu door tot kolom 3: moet nog variabele worden welke kolommen inladen etc.
    for column in range(8,9):
        #enkel afhankelijk van i:
        param_P.add_record(str(sheet5.cell(0,column).value)).value= sheet5.cell(1,column).value
        param_UA3.add_record(str(sheet5.cell(0,column).value)).value=  sheet5.cell(4,column).value
        param_COP3.add_record(str(sheet5.cell(0,column).value)).value= sheet5.cell(7,column).value
        param_mCp3.add_record(str(sheet5.cell(0,column).value)).value= sheet5.cell(10,column).value

        param_PmaxEV.add_record(str(sheet5.cell(0,column).value)).value=sheet7.cell(1,column).value
        param_SOCmax.add_record(str(sheet5.cell(0,column).value)).value=sheet7.cell(4,column).value
        param_SOCmin.add_record(str(sheet5.cell(0,column).value)).value=sheet7.cell(7,column).value
    for column in range(1,2):
        set_i.add_record((str(sheet1.cell(0,column).value)))
        #enkel afhankelijk van i:
        param_nbcd.add_record(str(sheet1.cell(0,column).value)).value = sheet2.cell(7,column).value
        param_nbcwm.add_record(str(sheet1.cell(0,column).value)).value = sheet3.cell(7,column).value
        param_nbcdw.add_record(str(sheet1.cell(0,column).value)).value = sheet4.cell(8,column).value
        param_Pmax.add_record(str(sheet1.cell(0,column).value)).value= sheet6.cell(1,column).value

        #afhankelijk van t en i
        for row in range(1,96+1):
            a = str(row)
            b = str(sheet1.cell(0,column).value)
            param_dcat1.add_record((a, b)).value = sheet1.cell(row,column).value
            param_Tmin3.add_record((str(row),str(sheet1.cell(0,column).value))).value= sheet5.cell(row,column).value
            param_SOC_wanted.add_record((str(row),str(sheet1.cell(0,column).value))).value = sheet7.cell(row+1,column).value
            #uit SEW database:
            param_Tmin4.add_record((str(row),str(sheet1.cell(0,column).value))).value = mintemp['huis21'][row-1]
            param_Tmax4.add_record((str(row),str(sheet1.cell(0,column).value))).value = maxtemp['huis21'][row-1]

            param_tstartd.add_record((str(row),str(sheet1.cell(0,column).value))).value = droogkast[row-1]
            param_tstartwm.add_record((str(row),str(sheet1.cell(0,column).value))).value = wasmachine['huis21'][row-1]
            param_tstartdw.add_record((str(row),str(sheet1.cell(0,column).value))).value = vaatwas['huis21'][row-1]

            param_Pdr.add_record((str(row),str(sheet1.cell(0,column).value))).value = ev_driving_power['huis21'][row-1]
            param_xdr.add_record((str(row),str(sheet1.cell(0,column).value))).value = evoertuig['huis21'][row-1]
            param_xch.add_record((str(row),str(sheet1.cell(0,column).value))).value = echarging['huis21'][row-1]
    for column in range(4,5):
        for row in range(1,97):
            param_Tmax3.add_record((str(row),str(sheet5.cell(0,column).value))).value= sheet5.cell(row,column).value
    #enkel ta
    for row in range(1,4):
        param_dc.add_record((str(int(sheet2.cell(row,0).value)))).value = sheet2.cell(row,1).value

    for row in range(1,4):
        param_wmc.add_record((str(int(sheet3.cell(row,0).value)))).value = sheet3.cell(row,1).value

    for row in range(1,5):
        param_dwc.add_record((str(int(sheet4.cell(row,0).value)))).value = sheet4.cell(row,1).value
    #enkel t
    for row in range(1,97):
        param_price.add_record(str(row)).value = sheet8.cell(row,15).value

        param_Toutside.add_record(str(row)).value = sheet8.cell(row+1,1).value
        param_Sres.add_record(str(row)).value = sheet8.cell(row+1,6).value



    # 7. Run job
    if db.check_domains():

        db.export("test.gdx")
        job.run(gams_options=opt, databases=db)


    #8. Retrieve solution
        #prepare database
        clearDb = list()
        clearDb.append('DELETE FROM EV')
        clearDb.append('DELETE FROM ijskast')
        clearDb.append('DELETE FROM Thouse')
        clearDb.append('DELETE FROM vaatwas')
        clearDb.append('DELETE FROM verwarming')
        clearDb.append('DELETE FROM wasmachine')
        clearDb.append('DELETE FROM net_energieverbruik')
        clearDb.append('DELETE FROM energieverbruik')
        for query in clearDb:
            cur.execute(query)

        #EV
        ev = list()
        for SOC in job.out_db.get_variable('SOC'):
            ev.append((getTimeFrom(int(SOC.keys[0])), SOC.level))
        cur.executemany("INSERT INTO EV (wijknr, tijd, SOC) VALUES (21, %s, %s);", ev)

        #ijskast
        ijskast = list()
        for Pin3 in job.out_db.get_variable('Pin3'):
            ijskast.append((getTimeFrom(int(Pin3.keys[0])), Pin3.level))
        cur.executemany("INSERT INTO ijskast (wijknr, tijd, verbruik) VALUES (21, %s, %s);", ijskast)

        #Thouse
        TH = list()
        for Thouse in job.out_db.get_variable('Thouse'):
            TH.append((getTimeFrom(int(Thouse.keys[0])), Thouse.level))
        cur.executemany("INSERT INTO Thouse (wijknr, tijd, temperatuur) VALUES (21, %s, %s);", TH)

        #vaatwas
        vaat = list()
        for Pdw in job.out_db.get_variable('Pdw'):
            vaat.append((getTimeFrom(int(Pdw.keys[0])), Pdw.level))
        cur.executemany("INSERT INTO vaatwas (wijknr, tijd, verbruik) VALUES (21, %s, %s);", vaat)

        #verwarming
        H = list()
        for Pin4 in job.out_db.get_variable('Pin4'):
            H.append((getTimeFrom(int(Pin4.keys[0])), Pin4.level))
        cur.executemany("INSERT INTO verwarming (wijknr, tijd, verbruik) VALUES (21, %s, %s);", H)

        #wasmachine
        was = list()
        for Pwm in job.out_db.get_variable('Pwm'):
            was.append((getTimeFrom(int(Pwm.keys[0])), Pwm.level))
        cur.executemany("INSERT INTO wasmachine (wijknr, tijd, verbruik) VALUES (21, %s, %s);", was)

        #net energieverbruik
        netE = list()
        for Sgrid in job.out_db.get_variable('Sgrid'):
            netE.append((getTimeFrom(int(Sgrid.keys[0])), Sgrid.level))
        cur.executemany("INSERT INTO net_energieverbruik (wijknr, tijd, net_verbruik) VALUES (21, %s, %s);", netE)

        #energieverbruik
        E = list()
        for Ptot in job.out_db.get_variable('Ptot'):
            E.append((getTimeFrom(int(Ptot.keys[0])), Ptot.level))
        cur.executemany("INSERT INTO energieverbruik (wijknr, tijd, verbruik) VALUES (21, %s, %s);", E)

        conn.commit()

    else:
        #db.export('filename.gdx')
        exit('ERROR: Something wrong about the domains')

    # Clean up afterwards
    #os.rename('_gams_py_gjo0.lst', 'transport.lst')
    #for f in glob.glob('_gams_py_*'):
    #    os.remove(f)


def loadExcelSheetsToDb():
    #prepare database
    clearDb = list()
    clearDb.append('DELETE FROM zon')
    clearDb.append('DELETE FROM wind')
    clearDb.append('DELETE FROM prijs')
    clearDb.append('DELETE FROM Toutside')
    clearDb.append('DELETE FROM tv')
    clearDb.append('DELETE FROM fornuis')
    for query in clearDb:
        cur.execute(query)

    from xlrd import open_workbook,cellname
    book = open_workbook('simulatie_data.xls')

    sheet1 = book.sheet_by_index(0)
    sheet2 = book.sheet_by_index(1)
    sheet3 = book.sheet_by_index(2)
    sheet4 = book.sheet_by_index(3)
    sheet5 = book.sheet_by_index(4)
    sheet6 = book.sheet_by_index(5)
    sheet7 = book.sheet_by_index(6)
    sheet8 = book.sheet_by_index(7)

    #zon
    zon = list()
    for row in range(2, 98):
        zon.append((getTimeFrom(int(sheet8.cell(row, 4).value)), sheet8.cell(row, 6).value))
    cur.executemany("INSERT INTO zon (tijd, load_factor) VALUES (%s, %s);", zon)

    #wind
    wind = list()
    for row in range(2, 98):
        wind.append((getTimeFrom(int(sheet8.cell(row, 9).value)), sheet8.cell(row, 10).value))
    cur.executemany("INSERT INTO wind (tijd, load_factor) VALUES ( %s, %s);", wind)

    #prijs
    prijs = list()
    for row in range(1, 97):
        prijs.append((getTimeFrom(int(sheet8.cell(row, 13).value)), sheet8.cell(row, 15).value))
    cur.executemany("INSERT INTO prijs VALUES ( %s, %s);", prijs)

    #buitentemperatuur
    temp = list()
    for row in range(2, 98):
        temp.append((getTimeFrom(int(sheet8.cell(row, 0).value)), sheet8.cell(row, 1).value))
    cur.executemany("INSERT INTO Toutside VALUES ( %s, %s);", temp)

    #TV
    TV = list()
    for row in range(1, 97):
        TV.append((getTimeFrom(int(sheet1.cell(row, 0).value)), sheet1.cell(row, 6).value))
    cur.executemany("INSERT INTO tv (wijknr, tijd, verbruik) VALUES (21, %s, %s);", TV)

    #fornuis
    fornuis = list()
    for row in range(1, 97):
        fornuis.append((getTimeFrom(int(sheet1.cell(row, 0).value)), sheet1.cell(row, 7).value))
    cur.executemany("INSERT INTO fornuis (wijknr, tijd, verbruik) VALUES (21, %s, %s);", fornuis)

    conn.commit()
