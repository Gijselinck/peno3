import logging
import string
from CommException import CommException

class Toestel(object):
    """Beschrijft een toestel in een Slimme-engerigewoning"""

    def __init__(self, id, sew_instance, communication_instance, naam, verbruik = 0, ):
        """ Constructor
        Parameters:
        id : id groter dan nul """

        # alle 'private' attributes
        self._id = id
        self._verbruik = verbruik
        self._naam = naam
        self._comm_inst = communication_instance
        self._sew       = sew_instance
        if not isinstance(id,int) or id < 0:
            raise ValueError("id moet een geheel getal groter dan 0 zijn")

    # id property, read-only
    def getId(self):
        return self._id

    # verbruik proberty, aan te passen
    def getVerbruik(self):
        return self._verbruik

    def setVerbruik(self, verbruik):
        if verbruik >= 0:
            self._verbruik = verbruik
        else:
            raise ValueError("Verbruik moet positief of nul")

    def getNaam(self):
        return self._naam

    def setNaam(self, naam):
        if isinstance(naam, str):
            self._naam = naam
        else:
            raise TypeError(" Naam moet een string zijn") 

    def sendStateToSew(self):

        try:
            self._comm_inst.setToestel(self._sew.getWijknummer(), 
                                       self.getArduinoIdFromName(), 
                                       self.getArduinoAnalogValFromVerbruik() )
        except CommException as exp:
            logging.warning(str(exp))

    def getArduinoAnalogValFromVerbruik(self):
        """ Zet naargelang de naam van het toestel het verbuik in zijn toestel afhankelijke
        random eenheid om naar een (0-255) schaal voor de analoge poorten op arduino"""

         # Haal uit de naam alle punctuatie en whitespace en zet om naar kleine letters om te vergelijken
        remove = string.punctuation + string.whitespace
        general_name  = self._naam.translate(None, remove).lower()

        # Hoogste waarde van analoge poort op arduino (komt overeen met 5V)
        ARDUINO_MAX_ANALOG = 255 

        if general_name == "wasmachine":
            # bij wasmachine is de variabele verbruik een vermogen met als max 23W:
            arduino_val = int(self._verbruik*ARDUINO_MAX_ANALOG/23*2/3)
        elif general_name == "vaatwasser":
            # bij vaatwasser is de variabele verbruik een vermogen met als max 50W:
            arduino_val = int(self._verbruik*ARDUINO_MAX_ANALOG/50*2/3)
        elif general_name == "ijskast" or general_name == "koelkast":
            # bij ijskast is de variabele een vermogen met als max 200W:
            arduino_val = int(self._verbruik*ARDUINO_MAX_ANALOG/200)
        elif general_name == "verwarming":
            # bij verwarming is de variabele een vermogen met als max 200W:
            arduino_val = int(self._verbruik*ARDUINO_MAX_ANALOG/2000)
        elif (general_name == "ev" or general_name == "evoertuig"):
            # bij een electrical vehicle is het 'verbruik' de state of charge, tussen 1 en 0:
            arduino_val = int(self._verbruik*ARDUINO_MAX_ANALOG)
        else:
            # stuur gewoon de variabele door
            arduino_val = self._verbruik

        return arduino_val

    def getArduinoIdFromName(self):
        remove = string.punctuation + string.whitespace
        general_name  = self._naam.translate(None, remove).lower()
        
        if general_name == "wasmachine":
            # bij wasmachine is de variabele verbruik een vermogen met als max 23W:
            return 1
        elif general_name == "vaatwas":
            return 10
        elif general_name == "ijskast" or general_name == "koelkast":
            return 6
        elif general_name == "verwarming":
            return 8
        elif (general_name == "ev" or general_name == "evoertuig"):
            return 4
        elif general_name == "fornuis":
            return 9
        elif (general_name == "tv" or general_name == "televisie"):
            return 11
        else:
            return None
                
    def __str__(self):
        if self._naam == None:
            naam = "naamloos"
        else:
            naam = self._naam
        return "Toestel "+ naam + " met id " + str(self._id)

class Ijskast(Toestel):

    def __init__(self, id, sew_instance, communication_instance, verbruik = 0):

        super(Ijskast, self).__init__(id, sew_instance, communication_instance, "ijskast" , verbruik)
        # de blauwe ledjes staan altijd aan, deze zijn 5 en 7
        self._comm_inst.setToestel(sew_instance.getWijknummer(), 5, 1)
        self._comm_inst.setToestel(sew_instance.getWijknummer(), 7, 1)

        return 

    def __del__(self):
       zetUit(self)

    def zetUit(self):
        self._comm_inst.setToestel(sew_instance.getWijknummer(), 5, 0)
        self._comm_inst.setToestel(sew_instance.getWijknummer(), 7, 0)

class EV( Toestel ):
    def __init__(self, id, sew_instance, communication_instance, verbruik = 0):
        self._chargeState = 20
        self._isCharging = False
        self._animOn = False
        super(EV, self).__init__(id, sew_instance, communication_instance, "ev", verbruik)

    def sendStateToSew(self):
        try:
            # naargelang charge state
            if(self._isCharging):
                if( not self._animOn):
                    # doe animatie
                    self._animOn = True
                    self.startChargeAnim()
            else:
                #zet de animatie uit indien deze nog aan staat en hij niet aan het opladen is
                if(not self._isCharging and self._animOn):
                    self.stopChargeAnim()

                # bepaal welke ledjes we moeten aan/uit zetten naargelang state of charge
                # bij 24
                if self._chargeState == 24000:
                    led_nr = 2
                else:
                    led_nr = self._chargeState / 8000
                
                led_status_lst = [ int(i == led_nr) for i in range(3) ]
                for i in led_status_lst:
                    self._comm_inst.setToestel(self._sew.getWijknummer(), 
                                        100 + i, led_status_lst[i])
        except CommException as exp:
            logging.warning(str(exp))

    def startChargeAnim(self):
        try:
            for i in range(3):
                self._comm_inst.setToestel(self._sew.getWijknummer(), i, 1)
        except CommException as exp:
            logging.warning(str(exp))

    def stopChargeAnim(self):
        try:
            for i in range(3):
                self._comm_inst.setToestel(self._sew.getWijknummer(), i, 0)
        except CommException as exp:
            logging.warning(str(exp))





